<?php
include('ImageCompare.php');
/**
 * These two images are almost the same so the hammered distance will be less than 10
 * Try it with images like below:
 * 1. Two slightly different images
 * 2. Two completely different images
 * 3. Two same images (returned value 0)
 * 4. Two same image but with different size/aspect ratio (returned value ~0)
 */
$image = new ImageCompare();
echo $image->compare('C:\xampp\htdocs\attendance_jmc\image_compare_1\image_1.jpg','C:\xampp\htdocs\attendance_jmc\image_compare_1\image_3.jpg');
?>