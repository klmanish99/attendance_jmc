<?php
$servername = "localhost";
$username = "root";
//$password = "";
$password = "JmC@2018";

$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

	$start_date = '2020-03-01';
	$end_date = '2020-03-31';
	$day = array();
	$days = GetDays($start_date, $end_date);
	foreach ($days as $dkey => $dvalue) {
		$dates = explode('-', $dvalue);
		$day[$dkey]['day'] = $dates[2];
		$day[$dkey]['date'] = $dvalue;
	}

	foreach ($day as $dkey => $dvalue) {
		$pre_date = $dvalue['date'];
		$attendance_data = query("SELECT * FROM oc_attendance where punch_date = '".$pre_date."' ",$conn);

		if($attendance_data->num_rows > 0){
			foreach ($attendance_data->rows as $akey => $avalue) {
				$month = date('n', strtotime($avalue['punch_date']));
				$year = date('Y', strtotime($avalue['punch_date']));
				$new_table = 'oc_attendance_'.$year.'_'.$month;

				query("INSERT INTO `".$new_table."` SET  
					emp_id = '".$avalue['emp_id']."',
					card_id = '".$avalue['card_id']."',
					punch_date = '".$avalue['punch_date']."',
					punch_time = '".$avalue['punch_time']."',
					terminal_addr = '".$avalue['terminal_addr']."',
					status = '".$avalue['status']."',
					transaction_id = '".$avalue['transaction_id']."',
					device_id = '".$avalue['device_id']."',
					log_id = '".$avalue['log_id']."',
					download_date = '".$avalue['download_date']."',
					new_log_id = '".$avalue['new_log_id']."'
				",$conn);
			}
		}

	}


	

$conn->close();
echo 'Done';exit;
?>

