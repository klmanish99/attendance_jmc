<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
//$current_date = date('Y-m-d');

$start_date = '2017-06-26';
$end_date = '2017-08-04';//date('Y-m-d', strtotime($current_date . ' +1 day'));
$day = array();
$days = GetDays($start_date, $end_date);
foreach ($days as $dkey => $dvalue) {
	$dates = explode('-', $dvalue);
	$day[$dkey]['day'] = $dates[2];
	$day[$dkey]['date'] = $dvalue;
}
// echo '<pre>';
// print_r($day);
// exit;
//$sql = "TRUNCATE TABLE `oc_transaction`";
//query($sql, $conn);
//$sql = "TRUNCATE TABLE `oc_attendance`";
//query($sql, $conn);

//$filter_date_start = date('Y-m-d', strtotime($current_date . ' +1 day'));

$batch_id = '0';
foreach($day as $dkeys => $dvalues){
	$filter_date_start = $dvalues['date'];
	//echo $filter_date_start;exit;
	$results = getemployees($conn);
	// echo '<pre>';
	// print_r($results);
	// exit;
	foreach ($results as $rkey => $emp_data) {
		//$emp_data = getempdata($emp_data['emp_code'], $conn);
		//if(isset($emp_data['name']) && $emp_data['name'] != ''){
			$emp_name = $emp_data['name'];
			$department = $emp_data['department'];
			$unit = $emp_data['unit'];
			$group = '';//$emp_data['group'];

			$day_date = date('j', strtotime($filter_date_start));
			$month = date('n', strtotime($filter_date_start));
			$year = date('Y', strtotime($filter_date_start));

			$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$month."' ";
			//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$emp_data['emp_code']."' ";
			$shift_schedule = query($update3, $conn)->row;
			$schedule_raw = explode('_', $shift_schedule[$day_date]);
			if(!isset($schedule_raw[2])){
				$schedule_raw[2]= 1;
			}
			// echo '<pre>';
			// print_r($schedule_raw);
			// exit;
			if($schedule_raw[0] == 'S'){
				$shift_data = getshiftdata($schedule_raw[1], $conn);
				// echo '<pre>';
				// print_r($shift_data);
				// exit;
				if(isset($shift_data['shift_id'])){
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					//$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
					//$trans_exist = query($trans_exist_sql, $conn);
					//if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					//} else {
						//$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."'";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					//}
				} 
				/*
				else {
					$shift_data = getshiftdata('1', $conn);
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
					
					$day = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));

					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = query($trans_exist_sql, $conn);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
						//echo $sql.';';
						//echo '<br />';
						query($sql, $conn);
					} else {
						$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					}
				}
				*/
			} elseif ($schedule_raw[0] == 'W') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				//$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
				//$trans_exist = query($trans_exist_sql, $conn);
				//if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				//} else {
					//$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
					//echo $sql.';';
					//echo '<br />';
					//$this->db->query($sql);
				//}
			} elseif ($schedule_raw[0] == 'H') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				$shift_outtime = $shift_data['out_time'];

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				
				//$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
				//$trans_exist = query($trans_exist_sql, $conn);
				//if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				//} else {
					//$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
					//echo $sql.';';
					//echo '<br />';
					//$this->db->query($sql);
				//}
			} elseif ($schedule_raw[0] == 'HD') {
				$shift_data = getshiftdata($schedule_raw[2], $conn);
				if(!isset($shift_data['shift_id'])){
					$shift_data = getshiftdata('1', $conn);
				}
				$shift_intime = $shift_data['in_time'];
				if($shift_data['shift_id'] == '1'){
					if($emp_data['sat_status'] == '1'){
						$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
						$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
					} else {
						$shift_outtime = $shift_data['out_time'];
					}
				} else {
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];
				}
				//$shift_intime = $shift_data['in_time'];
				//$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
				//$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
				//$shift_outtime = $shift_data['out_time'];

				$act_intime = '00:00:00';
				$act_outtime = '00:00:00';
				
				//$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
				//$trans_exist = query($trans_exist_sql, $conn);
				//if($trans_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
					//echo $sql.';';
					//echo '<br />';
					query($sql, $conn);
				//} else {
					//$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$emp_data['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."' WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` = '".$filter_date_start."' ";
					//echo $sql.';';
					//echo '<br />';
					//$this->db->query($sql);
				//}
			}
			// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$emp_data['emp_code']."' ";
			// $this->db->query($update);
			// $this->log->write($update);
		//}	
		//echo 'out';exit;	
	}
}
//$sql = "UPDATE `oc_transaction` SET `day_close_status` = '1' WHERE `date` = '".$filter_date_start."'";
//query($sql, $conn);
$conn->close();

function sortByOrder($a, $b) {
	$v1 = strtotime($a['fdate']);
	$v2 = strtotime($b['fdate']);
	return $v1 - $v2; // $v2 - $v1 to reverse direction
	// if ($a['punch_date'] == $b['punch_date']) {
		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	//}
	//return $a['punch_date'] - $b['punch_date'];
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function array_sort($array, $on, $order=SORT_ASC){

	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}
function getEmployees_dat($emp_code, $conn) {
	$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
	$query = query($sql, $conn);
	return $query->row;
}
function getempdata($emp_code, $conn) {
	$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
	$query = query($sql, $conn);
	return $query->row;
}
function insert_attendance_data($data, $conn) {
	$sql = query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0' ", $conn);
}
function getemployees($conn) {
	$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name` FROM `oc_employee` WHERE `status` = '1' AND `unit` = 'VITA' ";
	$sql .= " ORDER BY `employee_id` ";		
	$query = query($sql, $conn);
	return $query->rows;
}
function getrawattendance_group_date_custom($emp_code, $date, $conn) {
	$query = query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ", $conn);
	return $query->row;
}
function getshiftdata($shift_id, $conn) {
	$shift_data = array();
	if($shift_id == '1'){
		$shift_data['shift_id'] = '1';
		$shift_data['in_time'] = '09:30:00';
		$shift_data['out_time'] = '18:30:00';
	} else {
		$shift_data['shift_id'] = '2';
		$shift_data['in_time'] = '08:00:00';
		$shift_data['out_time'] = '20:00:00';
	}
	return $shift_data;
	// $query = query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ", $conn);
	// if($query->num_rows > 0){
	// 	return $query->row;
	// } else {
	// 	return array();
	// }
}

function getrawattendance_in_time($emp_id, $punch_date, $conn) {
	$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
	$array = $query->row;
	return $array;
}

function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date, $conn) {
	$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
	$act_outtime = array();
	if($query->num_rows > 0){
		$first_punch = $query->rows['0'];
		$array = $query->rows;
		$comp_array = array();
		$hour_array = array();
		foreach ($array as $akey => $avalue) {
			$start_date = new DateTime($act_punch_date.' '.$act_intime);
			$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
			if($since_start->d == 0){
				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				$hour_array[] = $since_start->h;
				$act_array[] = $avalue;
			}
		}

		// echo '<pre>';
		// print_r($hour_array);

		// echo '<pre>';
		// print_r($comp_array);

		foreach ($hour_array as $ckey => $cvalue) {
			if($cvalue > 18){
				unset($hour_array[$ckey]);
			}
		}

		// echo '<pre>';
		// print_r($hour_array);
		
		$act_outtimes = max($hour_array);

		// echo '<pre>';
		// print_r($act_outtimes);

		foreach ($hour_array as $akey => $avalue) {
			if($avalue == $act_outtimes){
				$act_outtime = $act_array[$akey];
			}
		}
	}
	// echo '<pre>';
	// print_r($act_outtime);
	// exit;		
	return $act_outtime;
}
echo 'Done';exit;
?>