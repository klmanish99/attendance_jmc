<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once('snippet_config.php');
$CommonClass = new CommonClass();
$CommonClass->CommonWrapper();
echo 'out';exit;

Class CommonClass {
  	public $link;
  
  	public function __construct() {
  		$this->link = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

		if (mysqli_connect_error()) {
			throw new ErrorException('Error: Could not make a database link (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}

		$this->link->set_charset("utf8");
		$this->link->query("SET SQL_MODE = ''");
	    
	    // $this->dbh1 = @mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD); 
	    // mysql_select_db('sport_events_be_db_demostore', $this->dbh1);
	    // mysql_query("SET NAMES 'utf8'", $this->dbh1);
	    // mysql_query("SET CHARACTER SET utf8", $this->dbh1);
	    // mysql_query("SET CHARACTER_SET_CONNECTION=utf8", $this->dbh1);
	    // mysql_query("SET SQL_MODE = ''", $this->dbh1);
  	}

  	public function db_query($sql) {
		$query = $this->link->query($sql);

		if (!$this->link->errno){
			if (isset($query->num_rows)) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				unset($data);

				$query->close();

				return $result;
			} else{
				return true;
			}
		} else {
			throw new ErrorException('Error: ' . $this->link->error . '<br />Error No: ' . $this->link->errno . '<br />' . $sql);
			exit();
		}
	}

	public function CommonWrapper(){
		$this->sendmaildept();
		//$this->sendmailsuper();
	}

  	public function sendmaildept(){
		$dept_heads = $this->db_query("SELECT `department`, emp_code, `title`, `name`, `email`, `dept_head_list` FROM `oc_employee` WHERE `is_dept` = '1' AND `is_super` = '0' ");
		foreach($dept_heads->rows as $dkeys => $dvalues){
			$depts_list = "'" . str_replace(",", "','", $dvalues['dept_head_list']) . "'";
			$pending_leaves = $this->db_query("SELECT `emp_id` FROM `oc_leave_transaction_temp` WHERE LOWER(`dept_name`) IN (" . strtolower($depts_list) . ") AND `approval_1` = '0' GROUP BY `batch_id` ");	
			$leaves_count = $pending_leaves->num_rows;
			$leaves_count = 1;			
			//echo $leaves_count;exit;
			if($leaves_count > 0 && $dvalues['email'] != ''){
				require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
				require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
				require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

				$mail = new PHPMailer();
				//$mail->SMTPDebug = 3;
				//$mail->isMail(); // Set mailer to use SMTP
				//$mail->Host = 'localhost'; 
				$message = 'You have ' . $leaves_count . ' Leaves Pending For Approval'. "\n\n";
				$message .= 'URL - http://ess.rwitc.com/';
				$subject = 'Leave Approval Reminder';
				$from_email = 'pld.rwitc2016@gmail.com';
				//$to_email = $dvalues['email'];
				$to_email = 'fargose.aaron@gmail.com';
				$to_name = $dvalues['title'].' '.$dvalues['name'];
				$mail->IsSMTP();
				$mail->SMTPAuth = true;
				$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
				$mail->Port = '587';//$this->config->get('config_smtp_port');
				$mail->Username = 'pld.rwitc2016';//$this->config->get('config_smtp_username');
				$mail->Password = 'rwitc@2016';//$this->config->get('config_smtp_password');
				$mail->SMTPSecure = 'tls';
				$mail->SetFrom($from_email, 'PLD.RWITC');
				//$mail->From = $from_email;
				//$mail->FromName = "PLD.RWITC";				
				$mail->Subject = $subject;
				//$mail->isHTML(true);
				//$mail->MsgHTML($invoice_mail_text);
				$mail->Body = html_entity_decode($message);
				//$mail->addAttachment($filename, $bfilename);
				$mail->AddAddress($to_email, $to_name);
				if($mail->Send()) {
				  echo 'Mail Sent';
				  echo '<br />';
				  //echo '<pre>';
				  //print_r($mail->ErrorInfo);
				} else {
				  //echo "Mailer Error: " . $mail->ErrorInfo;
				  echo '<pre>';
				  print_r($mail->ErrorInfo);
				}
				exit;
			}
			//exit;
		}
	}

	public function sendmailsuper(){
		$dept_heads = $this->db_query("SELECT `department`, emp_code, `title`, `name`, `email`, `dept_head_list` FROM `oc_employee` WHERE `is_super` = '1' ");
		// echo '<pre>';
		// print_r($dept_heads);
		// exit;
		foreach($dept_heads->rows as $dkeys => $dvalues){
			$depts_list = "'" . str_replace(",", "','", $dvalues['dept_head_list']) . "'";
			$pending_leaves = $this->db_query("SELECT `emp_id` FROM `oc_leave_transaction_temp` WHERE `approval_2` = '0' AND `group` = 'OFFICIALS' GROUP BY `batch_id` ");	
			$leaves_count = $pending_leaves->num_rows;
			//echo $leaves_count;exit;
			//$dvalues['email'] = 'fargose.aaron@gmail.com';
			//$leaves_count = 2;
			if($leaves_count > 0 && $dvalues['email'] != ''){
				require_once(DIR_SYSTEM . 'library/PHPMailer/PHPMailerAutoload.php');
				require_once(DIR_SYSTEM . 'library/PHPMailer/class.phpmailer.php');
				require_once(DIR_SYSTEM . 'library/PHPMailer/class.smtp.php');

				$mail = new PHPMailer();
				//$mail->SMTPDebug = 3;
				//$mail->isMail(); // Set mailer to use SMTP
				//$mail->Host = 'localhost'; 
				$message = 'You have ' . $leaves_count . ' OFFICIALS Leaves Pending For Approval'. "\n\n";
				$message .= 'URL - http://ess.rwitc.com/';
				$subject = 'Leave Approval Reminder';
				$from_email = 'pld.rwitc2016';
				$to_email = $dvalues['email'];
				//$to_email = 'fargose.aaron@gmail.com';
				$to_name = $dvalues['title'].' '.$dvalues['name'];
				$mail->IsSMTP();
				$mail->SMTPAuth = true;
				$mail->Host = 'smtp.gmail.com';//$this->config->get('config_smtp_host');
				$mail->Port = '587';//$this->config->get('config_smtp_port');
				$mail->Username = 'pld.rwitc2016';//$this->config->get('config_smtp_username');
				$mail->Password = 'rwitc@2016';//$this->config->get('config_smtp_password');
				$mail->SMTPSecure = 'tls';
				$mail->SetFrom($from_email, 'PLD.RWITC');
				$mail->Subject = $subject;
				//$mail->isHTML(true);
				//$mail->MsgHTML($invoice_mail_text);
				$mail->Body = html_entity_decode($message);
				//$mail->addAttachment($filename, $bfilename);
				$mail->AddAddress($to_email, $to_name);
				if($mail->Send()) {
				  //echo 'Mail Sent';
				  //echo '<br />';
				  //echo '<pre>';
				  //print_r($mail->ErrorInfo);
				} else {
				  //echo "Mailer Error: " . $mail->ErrorInfo;
				  //echo '<pre>';
				  //print_r($mail->ErrorInfo);
				}
				//exit;
			}
			//exit;
		}
	}
}
?>
