<?php
$servername = "localhost";
$username = "root";
$password = "JmC@2018";
//$password = "";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function GetDays($sStartDate, $sEndDate){  
	// Firstly, format the provided dates.  
	// This function works best with YYYY-MM-DD  
	// but other date formats will work thanks  
	// to strtotime().  
	$sStartDate = date("Y-m-d", strtotime($sStartDate));  
	$sEndDate = date("Y-m-d", strtotime($sEndDate));  
	// Start the variable off with the start date  
	$aDays[] = $sStartDate;  
	// Set a 'temp' variable, sCurrentDate, with  
	// the start date - before beginning the loop  
	$sCurrentDate = $sStartDate;  
	// While the current date is less than the end date  
	while($sCurrentDate < $sEndDate){  
	// Add a day to the current date  
	$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
		// Add this new day to the aDays array  
	$aDays[] = $sCurrentDate;  
	}
	// Once the loop has finished, return the  
	// array of days.  
	return $aDays;  
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

	
	$start_date = '2020-03-01';
	$end_date = '2020-03-31';
	$day = array();
	$days = GetDays($start_date, $end_date);
	foreach ($days as $dkey => $dvalue) {
		$dates = explode('-', $dvalue);
		$day[$dkey]['day'] = $dates[2];
		$day[$dkey]['date'] = $dvalue;
	}

	foreach ($day as $dkey => $dvalue) {
		$pre_date = $dvalue['date'];
		$trans_data = query("SELECT * FROM oc_transaction where `date` = '".$pre_date."' ",$conn);

		if($trans_data->num_rows > 0){
			foreach ($trans_data->rows as $akey => $avalue) {
				$month = date('n', strtotime($avalue['date']));
				$year = date('Y', strtotime($avalue['date']));
				$new_table = 'oc_transaction_'.$year.'_'.$month;

				$sql = ("INSERT INTO `".$new_table."` SET  
					emp_id = '".$avalue['emp_id']."',
					emp_name = '".$avalue['emp_name']."',
					shift_intime = '".$avalue['shift_intime']."',
					shift_outtime = '".$avalue['shift_outtime']."',
					act_intime = '".$avalue['act_intime']."',
					act_outtime = '".$avalue['act_outtime']."',
					weekly_off = '".$avalue['weekly_off']."',
					holiday_id = '".$avalue['holiday_id']."',
					late_time = '".$avalue['late_time']."',
					early_time = '".$avalue['early_time']."',
					working_time = '".$avalue['working_time']."',
					day = '".$avalue['day']."',
					month = '".$avalue['month']."',
					year = '".$avalue['year']."',
					date = '".$avalue['date']."',
					status = '".$avalue['status']."',
					department = '".$avalue['department']."',
					unit = '".$avalue['unit']."',
					firsthalf_status = '".$avalue['firsthalf_status']."',
					secondhalf_status = '".$avalue['secondhalf_status']."',
					manual_status = '".$avalue['manual_status']."',
					day_close_status = '".$avalue['day_close_status']."',
					halfday_status = '".$avalue['halfday_status']."',
					leave_status = '".$avalue['leave_status']."',
					present_status = '".$avalue['present_status']."',
					absent_status = '".$avalue['absent_status']."',
					abnormal_status = '".$avalue['abnormal_status']."',
					date_out = '".$avalue['date_out']."',
					on_duty = '".$avalue['on_duty']."',
					compli_status = '".$avalue['compli_status']."',
					month_close_status = '".$avalue['month_close_status']."',
					device_id = '".$avalue['device_id']."',
					batch_id = '".$avalue['batch_id']."',
					late_mark = '".$avalue['late_mark']."',
					early_mark = '".$avalue['early_mark']."',
					division = '".$avalue['division']."',
					region = '".$avalue['region']."',
					department_id = '".$avalue['department_id']."',
					division_id = '".$avalue['division_id']."',
					unit_id = '".$avalue['unit_id']."',
					region_id = '".$avalue['region_id']."',
					shift_change = '".$avalue['shift_change']."',
					shift_free_change = '".$avalue['shift_free_change']."',
					act_intime_change = '".$avalue['act_intime_change']."',
					act_outtime_change = '".$avalue['act_outtime_change']."',
					company = '".$avalue['company']."',
					company_id = '".$avalue['company_id']."',
					`group` = '".$avalue['group']."'

					");

				// echo $sql;
				// echo '<br/>';

				query($sql, $conn);
			}
		}
	}

$conn->close();
echo 'Done';exit;
?>