<?php
$servername = "localhost";
$username = "root";
//$password = "";
$password = "JmC@2018";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
	$date = new DateTime();
	$date->modify("first day of previous month");
	$pre_month_date = $date->format("Y-m-d");
	//$pre_month_date = '2020-04-01';
	$pre_day = date('d', strtotime($pre_month_date));
	$pre_month = date('n', strtotime($pre_month_date));
	$pre_year = date('Y', strtotime($pre_month_date));
	$pre_atten_table = 'oc_attendance_'.$pre_year.'_'.$pre_month;
	$pre_trans_table = 'oc_transaction_'.$pre_year.'_'.$pre_month;


	$current_date = date('Y-m-d');
	//$current_date = '2020-05-01';
	$day = date('d', strtotime($current_date));
	$month = date('n', strtotime($current_date));
	$year = date('Y', strtotime($current_date));

if($day == '01') {
	$new_atten_table = 'oc_attendance_'.$year.'_'.$month;

	$sql_atten = "CREATE TABLE ".$new_atten_table." (
		  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		  `emp_id` int(11) NOT NULL,
		  `card_id` int(11) NOT NULL,
		  `punch_date` date NOT NULL,
		  `punch_time` time NOT NULL,
		  `terminal_addr` int(11) NOT NULL,
		  `status` int(11) NOT NULL,
		  `transaction_id` int(11) NOT NULL,
		  `device_id` int(11) NOT NULL,
		  `log_id` int(11) NOT NULL,
		  `new_log_id` int(11) NOT NULL,
		  `download_date` date NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";

	query($sql_atten, $conn);

	query("ALTER TABLE `".$new_atten_table."`  ADD INDEX `emp_id` (`emp_id`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_atten_table."`  ADD INDEX `punch_date` (`punch_date`) USING BTREE" , $conn);
	query("ALTER TABLE `".$new_atten_table."`  ADD INDEX `punch_time` (`punch_time`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_atten_table."` ADD INDEX `status` (`status`) USING BTREE", $conn);

	if ($result = query("SHOW TABLES LIKE '".$pre_atten_table."'",$conn)) {
	    if($result->num_rows == 1) {
	    	$inn = 1;
			$atten_tbl_autoin_no = query("SELECT id from `".$pre_atten_table."` order by id DESC LIMIT 1",$conn);
	       // echo "Table exists";
	    } else {
	    	$inn = 0;
	    }
	}
	else {
		$inn = 0;
	   // echo "Table does not exist";
	}
	
	if($inn == 1){
		if($atten_tbl_autoin_no->num_rows > 0){
			$new_atten_autoid = $atten_tbl_autoin_no->row['id'] + 200;
		}else {
			$new_atten_autoid = 1;
		}

		query("ALTER TABLE `".$new_atten_table."` AUTO_INCREMENT = ".$new_atten_autoid." ", $conn);
	}


	$new_trans_table = 'oc_transaction_'.$year.'_'.$month;


	$sql_trans = "CREATE TABLE `".$new_trans_table."`(
	  	  `transaction_id` int(11) NOT NULL  PRIMARY KEY AUTO_INCREMENT,
		  `emp_id` int(11) NOT NULL,
		  `emp_name` varchar(255) NOT NULL,
		  `shift_intime` time NOT NULL,
		  `shift_outtime` time NOT NULL,
		  `act_intime` time NOT NULL,
		  `act_outtime` time NOT NULL,
		  `weekly_off` int(11) NOT NULL,
		  `holiday_id` int(11) NOT NULL,
		  `late_time` time NOT NULL,
		  `early_time` time NOT NULL,
		  `working_time` time NOT NULL,
		  `day` int(11) NOT NULL,
		  `month` int(11) NOT NULL,
		  `year` int(11) NOT NULL,
		  `date` date NOT NULL,
		  `status` int(11) NOT NULL,
		  `department` varchar(255) NOT NULL,
		  `unit` varchar(255) NOT NULL,
		  `group` varchar(255) NOT NULL,
		  `firsthalf_status` varchar(255) NOT NULL,
		  `secondhalf_status` varchar(255) NOT NULL,
		  `manual_status` int(11) NOT NULL,
		  `day_close_status` int(11) NOT NULL,
		  `halfday_status` int(11) NOT NULL,
		  `leave_status` float NOT NULL,
		  `present_status` float NOT NULL,
		  `absent_status` float NOT NULL,
		  `abnormal_status` int(11) NOT NULL,
		  `date_out` date NOT NULL,
		  `on_duty` int(1) NOT NULL,
		  `compli_status` int(11) NOT NULL,
		  `month_close_status` int(11) NOT NULL,
		  `device_id` int(11) NOT NULL,
		  `batch_id` int(11) NOT NULL,
		  `late_mark` int(11) NOT NULL,
		  `early_mark` int(11) NOT NULL,
		  `division` varchar(255) NOT NULL,
		  `region` varchar(255) NOT NULL,
		  `department_id` int(11) NOT NULL,
		  `division_id` int(11) NOT NULL,
		  `unit_id` int(11) NOT NULL,
		  `region_id` int(11) NOT NULL,
		  `shift_change` int(11) NOT NULL,
		  `shift_free_change` int(11) NOT NULL,
		  `act_intime_change` int(11) NOT NULL,
		  `act_outtime_change` int(11) NOT NULL,
		  `company` varchar(255) NOT NULL,
		  `company_id` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1";

	query($sql_trans, $conn);


	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `emp_id` (`emp_id`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `month` (`month`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `year` (`year`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `unit` (`unit`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `department` (`department`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `group` (`group`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `firsthalf_status` (`firsthalf_status`) USING BTREE", $conn);
	query("ALTER TABLE `".$new_trans_table."`  ADD INDEX `secondhalf_status` (`secondhalf_status`) USING BTREE", $conn);


	if ($result1 = query("SHOW TABLES LIKE '".$pre_trans_table."'",$conn)) {
	    if($result1->num_rows == 1) {
	    	$inn1 = 1;
			$trans_tbl_autoin_no = query("SELECT transaction_id from `".$pre_trans_table."` order by transaction_id DESC LIMIT 1",$conn);
	       // echo "Table exists";
	    } else {
	    	$inn1 = 0;
	    }
	}
	else {
		$inn1 = 0;
	   // echo "Table does not exist";
	}
	
	if($inn1 == 1){
		if($trans_tbl_autoin_no->num_rows > 0){
			$trans_new_id = $trans_tbl_autoin_no->row['transaction_id'] + 200;
		}else {
			$trans_new_id = 1;
		}

		query("ALTER TABLE `".$new_trans_table."` AUTO_INCREMENT = ".$trans_new_id." " , $conn);
	}
	
}
$conn->close();
echo 'Done';exit;
?>