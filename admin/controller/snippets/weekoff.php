<?php    
class ControllerSnippetsWeekoff extends Controller { 
	private $error = array();

	public function index() {

		$employee_datas = $this->db->query("SELECT `emp_code`, `doj`, `shift_id`, `sat_status`, `unit` FROM `oc_employee` WHERE `unit` = 'VITA' ORDER BY `employee_id` ")->rows;
		// echo '<pre>';
		// print_r($employee_datas);
		// exit;
		$start = new DateTime('2018-02-27');
		$end   = new DateTime('2019-01-01');
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$week_array = array();
		$wednesday_array = array();
		foreach ($period as $dt){
		    if ($dt->format('N') == 3) {
		    	$sunday = $dt->format('Y-m-d');
		    	$month_name = date('M', strtotime($sunday));
	    		$year = date('Y', strtotime($sunday));
		    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
		    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
		    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
		    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
		    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
		    }
		    if ($dt->format('N') == 7){
		    	$sunday = $dt->format('Y-m-d');
		    	$month_name = date('M', strtotime($sunday));
	    		$year = date('Y', strtotime($sunday));
		    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
		    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
		    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
		    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
		    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
		    }
		}
		// echo '<pre>';
		// print_r($week_array);
		// exit;
		foreach($employee_datas as $ekey => $evalue){
			$week_string = 'W_1_'.$evalue['shift_id'];
			$shift_string = 'S_'.$evalue['shift_id'];
			foreach($week_array as $wkey => $wvalue){
				if($wvalue['day_name'] == 'Sun'){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";	
				} elseif($wvalue['day_name'] == 'Wed'){
					$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['emp_code']."' ";
				}
				$this->db->query($sql);
				//echo $sql;
				//echo '<br />';	
			}
			//echo '<br />';
			//exit;
		}
		echo 'out';exit;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function new_mysql($sql) {
		$con=mysqli_connect("localhost","root","","db_attendance_jmc_1");
		mysqli_multi_query($con,$sql);
		do {
			if($result = mysqli_store_result($con)){
				mysqli_free_result($result);
			}
		} while(mysqli_more_results($con));
		if(mysqli_error($con)) {
			die(mysqli_error($con));
		}
		// while (mysqli_next_result($link)) {
		// 	if (!mysqli_more_results()){
		// 		break;
		// 	}
		// }
		mysqli_close($con);
	}
}
?>