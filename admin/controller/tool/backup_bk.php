<?php 
class ControllerToolBackup extends Controller { 
	private $error = array();
	public function index() {		
		$this->language->load('tool/backup');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('tool/backup');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'tool/backup')) {
			if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
				$content = file_get_contents($this->request->files['import']['tmp_name']);
			} else {
				$content = false;
			}
			if ($content) {
				$this->import_data($content);
				$this->session->data['success'] = 'Data Imported Successfully';
				$this->redirect($this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->error['warning'] = $this->language->get('error_empty');
			}
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_backup'] = $this->language->get('entry_backup');

		$this->data['button_backup'] = $this->language->get('button_backup');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['restore'] = $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'tool/backup.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	public function import_data($content) {
		$this->language->load('tool/backup');
		$this->load->model('tool/backup');
		
		
		$serverName = "BIOFACE1";
		//$connectionInfo = array("Database"=>"staffdata", "UID"=>"sa", "PWD"=>"1234");
		$connectionInfo = array("Database"=>"staffdata");
		$conn1 = sqlsrv_connect($serverName, $connectionInfo);
		if($conn1) {
			 //echo "Connection established.<br />";
		}else{
			 //echo "Connection could not be established.<br />";
			 $dat['connection'] = 'Connection could not be established';
			 //echo '<pre>';
			 //print_r(sqlsrv_errors());
			 //exit;
			 //die( print_r( sqlsrv_errors(), true));
		}
		

		$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
		if($batch_ids->num_rows > 0){
			$batch_id = $batch_ids->row['batch_id'] + 1;
		} else {
			$batch_id = 1;
		}

		$exp_datas = array();
		$exp_datas = explode(PHP_EOL, $content);

		// echo '<pre>';
		// print_r($exp_datas);
		// exit;

		$this->data['employees'] = array();
		$employees = array();
		$this->load->model('catalog/employee');		
		//if(isset($exp_datas[0]) && $exp_datas[0] == ''){
			//unset($exp_datas[0]);
			//$rev_exp_datas = $exp_datas;
		//} else {
			//$rev_exp_datas = $exp_datas;			
		//}
		$employees = array();
		$employees_final = array();
		
		// echo '<pre>';
		// print_r($exp_datas);
		// exit;

		foreach($exp_datas as $data) {
			$tdata = $data;
			
			$emp_id  = substr($tdata, 0, 14);
			$in_date_raw = substr($tdata, 15, 10);
			$in_date = date('Y-m-d', strtotime($in_date_raw));
			$in_time_hour = substr($tdata, 26, 2);
			$in_time_min = substr($tdata, 29, 2);
			$in_time_sec = substr($tdata, 32, 2);
			$in_time = $in_time_hour.':'.$in_time_min.':'.$in_time_sec;

			// $emp_id  = substr($tdata, 0, 9);
   // 			$in_date_raw = substr($tdata, 10, 10);
   // 			$in_date = date('Y-m-d', strtotime($in_date_raw));
   // 			$in_time_hour = substr($tdata, 21, 2);
   // 			$in_time_min = substr($tdata, 24, 2);
   // 			$in_time_sec = substr($tdata, 27, 2);
   // 			$in_time = $in_time_hour.':'.$in_time_min.':'.$in_time_sec;


			// echo $tdata;
			// echo '<br />';
			// echo $emp_id;
			// echo '<br />';
			// echo $in_date_raw;
			// echo '<br />';
			// echo $in_date;
			// echo '<br />';
			// echo $in_time;
			// echo '<br />';
			// exit;

			if($emp_id != '') {
				$result = $this->getEmployees_dat($emp_id);
				if(isset($result['emp_code'])){
					$employees[] = array(
						'employee_id' => $result['emp_code'],
						'emp_name' => $result['name'],
						'card_id' => $result['card_number'],
						'department' => $result['department'],
						'unit' => $result['unit'],
						'group' => '',//$result['group'],
						'in_time' => $in_time,
						'device_id' => 0,
						'punch_date' => $in_date,
						'download_date' => date('Y-m-d'),
						'log_id' => 0,
						'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
					);
				} else {
					$employees[] = array(
						'employee_id' => $emp_id,
						'emp_name' => '',
						'card_id' => $emp_id,
						'department' => '',
						'unit' => '',
						'group' => '',//$result['group'],
						'in_time' => $in_time,
						'device_id' => 0,
						'punch_date' => $in_date,
						'download_date' => date('Y-m-d'),
						'log_id' => 0,
						'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
					);
				}
			}
		}

		// echo '<pre>';
		// print_r($employees);
		// exit;

		usort($employees, array($this, "sortByOrder"));
		$o_emp = array();
		foreach ($employees as $ekey => $evalue) {
			$employees_final[] = $evalue;
		}
		$filter_unit = 0;
		if(isset($employees_final) && count($employees_final) > 0){
			foreach ($employees_final as $fkey => $employee) {
				$exist = $this->getorderhistory($employee);
				if($exist == 0){
					$mystring = $employee['in_time'];
					$findme   = ':';
					$pos = strpos($mystring, $findme);
					if($pos !== false){
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
					} else {
						$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
					}
					$employee['in_time'] = $in_times;
					$this->insert_attendance_data($employee);
				}
			}
		}
		$unprocessed_dates = $this->db->query("SELECT `punch_date`, `emp_id`, `id` FROM `oc_attendance` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE e.`status` = '1' AND a.`status` = '0' AND a.`punch_date` >= '2017-06-26' AND a.`log_id` = '0' ORDER BY a.`punch_date` ASC ");
		// echo '<pre>';
		// print_r($unprocessed_dates);
		// exit;
		$current_processed_data = array();
		$data = array();
		foreach($unprocessed_dates->rows as $dkey => $dvalue){
			$current_processed_data[$dvalue['id']] = $dvalue['id'];
			
			$filter_date_start = $dvalue['punch_date'];
			$data['filter_name_id'] = $dvalue['emp_id']; 
			$results = $this->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$rvaluess = $this->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
				if($rvaluess) {
					$emp_data = $this->getempdata($rvaluess['emp_id']);
					$device_id = $rvaluess['device_id'];
					if(isset($emp_data['name']) && $emp_data['name'] != ''){
						
						$emp_name = $emp_data['name'];
						$department = $emp_data['department'];
						$unit = $emp_data['unit'];
						$group = '';//$emp_data['group'];

						$day_date = date('j', strtotime($filter_date_start));
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2] = 1;
						}
						// echo '<pre>';
						// print_r($schedule_raw);
						// exit;
						if($schedule_raw[0] == 'S'){
							$shift_data = $this->getshiftdata($schedule_raw[1]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata('1');
							}
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									/*
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
								
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}*/

									$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								}
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}
								$abnormal_status = 0;
								$first_half = 0;
								$second_half = 0;
								$late_time = '00:00:00';
								//$act_intime = '09:59:00';
								//$act_outtime = '19:45:00';
								$late_mark = 0;
								$early_mark = 0;
								$shift_early = 0;
								if($act_intime != '00:00:00'){
									if($shift_data['shift_id'] == '1'){
										$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
										//echo $shift_intime_plus_one;exit;
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											//$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 1;
										}
										//echo $late_time;exit;

										$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->invert == 1){
											$shift_early = 1;
											//$late_time = '00:00:00';
										} else {
											//if($late_min > 0){
												//$late_mark = 1;
											//} else {
												//$late_mark = 0;
											//}
										}
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
									}
									//echo 'out';exit;
									
									//if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
										//$abnormal_status = 1;
									//} else {
										if($shift_data['shift_id'] == '1'){
											/*
											if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 6){
														$second_half = 1;
													} elseif( ($since_start->h == 5 && $since_start->i >= 51)){
														$second_half = 1;
														$early_mark = 1;
													} elseif( ($since_start->h == 4 && $since_start->i >= 31) || $since_start->h > 4){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
													//if( (($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4) && $since_start->invert == 0){
													// if( $since_start->h >= 3 && $since_start->invert == 0){
													// 	$second_half = 1;
													// } else {
													// 	$second_half = 0;
													// }
												} else {
													$second_half = 0;
												}
											} else {
											*/
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														if($shift_data['shift_id'] == '1'){
															$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
															if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
															}
														} else {
															if(strtotime($act_outtime) < strtotime($shift_intime)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
															}
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
														$first_half = 1;
														$second_half = 1;
													} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											//}
										} else {
											/*
											if( (($since_start->h == 1 && $since_start->i >= 1) || $since_start->h > 1) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if(($since_start->h == 7 && $since_start->i >= 31) || $since_start->h > 7){
														$second_half = 1;
													} elseif($since_start->h >= 6){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
											*/
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														if($shift_data['shift_id'] == '1'){
															$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
															if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
															}
														} else {
															if(strtotime($act_outtime) < strtotime($shift_intime)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
															}
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
														$first_half = 1;
														$second_half = 1;
													} elseif($since_start->h >= 6){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											//}
										}
									//}
									//echo 'out';exit;
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$early_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										if($shift_data['shift_id'] == '1'){
											$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										}
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								/*
								if($first_half == 1){
									$secs = strtotime($late_time)-strtotime("00:00:00");
									$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
									$total_late_early_time_exp = explode(':', $total_late_early_time);
									if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
										$second_half = 0;
									}
								}
								// $month = 1;
								// $day = 26;
								// $year = 2018;
								if($day <= 25){
									if($month == 1){
										$c_month = 12;
										$c_year = $year - 1;
									} else {
										$c_month = $month - 1;
										$c_year = $year;
									}
									$c_month = sprintf("%02d", $c_month);
									$c_year = sprintf("%02d", $c_year);
								} else {
									$c_month = sprintf("%02d", $month);
									$c_year = sprintf("%02d", $year);
								}

								$from_date = $c_year.'-'.$c_month.'-26';
								$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
								$late_datas = query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ", $conn)->row;
								$early_datas = query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `early_mark` = '1' ", $conn)->row;
								$db_late_mark = $late_datas['total_late_mark'];
								
								// echo '<pre>';
								// print_r($from_date);
								// echo '<pre>';
								// print_r($to_date);
								// echo '<pre>';
								// print_r($late_datas);
								// echo '<pre>';
								// print_r($early_datas);
								
								if($first_half == 1 || $second_half == 1){
									$db_late_mark = $late_datas['total_late_mark'];
									$db_early_mark  = $early_datas['total_early_mark'];
									$total_late_early_mark = $db_late_mark + $db_early_mark + $late_mark + $early_mark;
									if($late_mark == 1){
										if($total_late_early_mark <= 2){
										} else {
											$second_half = 0;	
										}
									}
									if($early_mark == 1){
										if($total_late_early_mark <= 2){
										} else {
											$second_half = 0;	
										}
									}
								} else {
									if($first_half == 0 && $second_half == 0){
										$late_mark = 0;
										$early_mark = 0;
									}
								}
								*/
								/*
								if($late_datas['total_late_mark'] <= 2){
									if($late_mark == 1){
										if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
											$first_half = 0;
										} else {
											//$late_mark_var = 1;
											//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
											$db_late_mark = $late_datas['total_late_mark'] + 1;
											if($db_late_mark == 3){
												$first_half = 0;
											}
										}
									}
								} else {
									if($late_mark == 1){
										$first_half = 0;	
									}
								}
								if($early_datas['total_early_mark'] == 0){
									if($early_mark == 1){
										if($db_late_mark == 2){
											//$early_mark_var = 1;
											//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
											//$second_half = 1;
										} else {
											$second_half = 0;
										}
									}
								} else {
									if($early_mark == 1){
										$second_half = 0;	
									}	
								}
								*/

								// echo $first_half;
								// echo '<br/ >';
								// echo $second_half;
								// echo '<br/ >';
								// echo $late_mark;
								// echo '<br />';
								// echo $early_mark;
								// echo '<br />';
								// echo $late_time;
								// echo '<br/ >';
								// echo $early_time;
								// echo '<br/ >';
								// echo $total_late_early_time;
								// echo '<br/ >';
								// exit;

								$working_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										if($shift_early == 1){
											if($shift_data['shift_id'] == '1'){
												$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
												if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												}
											} else {
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										}
										//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}
								// echo $shift_early;
								// echo '<br />';
								// echo $working_time;exit;

								$abs_stat = 0;
								// if($filter_date_start != $act_in_punch_date){
								// 	$abs_stat = 1;
								// 	$act_in_punch_date = $filter_date_start;
								// }

								// if($abs_stat == 0){
								// 	if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 		$abnormal_status = 1;
								// 	}
								// }

								// if($working_time != '00:00:00'){
								// 	$work_exp = explode(':', $working_time);
								// 	if($work_exp[0] >= 18){
								// 		$abnormal_status = 1;
								// 	}
								// }

								// if($early_time != '00:00:00'){
								// 	$early_exp = explode(':', $early_time);
								// 	if($early_exp[0] >= 6){
								// 		$abnormal_status = 1;
								// 	}
								// }

								// if($late_time != '00:00:00'){
								// 	$late_exp = explode(':', $late_time);
								// 	if($late_exp[0] >= 6){
								// 		$abnormal_status = 1;
								// 	}
								// }

								// if($abnormal_status == 1){
								// 	$first_half = 0;
								// 	$second_half = 0;										
								// 	//$early_time = '00:00:00';
								// 	//$late_time = '00:00:00';
								// 	//$working_time = '00:00:00';										
								// }

								if($first_half == 1 && $second_half == 1){
									$present_status = 1;
									$absent_status = 0;
								} elseif($first_half == 1 && $second_half == 0){
									$present_status = 0.5;
									$absent_status = 0.5;
								} elseif($first_half == 0 && $second_half == 1){
									$present_status = 0.5;
									$absent_status = 0.5;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								$day = date('j', strtotime($rvaluess['punch_date']));
								$month = date('n', strtotime($rvaluess['punch_date']));
								$year = date('Y', strtotime($rvaluess['punch_date']));
								//echo 'out';exit;
								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								// echo $sql.';';
								// echo '<br />';
								// exit;
								
							}
							//echo 'out1';exit;
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = $this->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$act_in_punch_date = $filter_date_start;
							$act_out_punch_date = $filter_date_start;

							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){ 
								$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							} else {
								/*
								if($trans_exist->row['act_intime'] == '00:00:00'){
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){	
										$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}	
								} else {
									$act_intime = $trans_exist->row['act_intime'];
									$act_in_punch_date = $trans_exist->row['date'];	
								}
							
								$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
								$new_data_exist = $this->db->query($new_data_sql);
								if($new_data_exist->num_rows > 0){
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									$act_outtime = $trans_exist->row['act_outtime'];
									$act_out_punch_date = $trans_exist->row['date_out'];
								}
								*/
								$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							}
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}
							$abnormal_status = 0;
							$first_half = 0;
							$second_half = 0;
							$late_time = '00:00:00';
							//$act_intime = '09:59:00';
							//$act_outtime = '19:45:00';
							$late_mark = 0;
							$early_mark = 0;
							$shift_early = 0;
							if($act_intime != '00:00:00'){
								if($shift_data['shift_id'] == '1'){
									$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
									//echo $shift_intime_plus_one;exit;
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->h > 0){
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									} else {
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									}
									
									$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
									if($since_start->invert == 1){
										//$shift_early = 1;
										$late_time = '00:00:00';
									} else {
										$late_mark = 1;
									}
									//echo $late_time;exit;

									$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->invert == 1){
										$shift_early = 1;
										//$late_time = '00:00:00';
									} else {
										//if($late_min > 0){
											//$late_mark = 1;
										//} else {
											//$late_mark = 0;
										//}
									}
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->h > 0){
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									} else {
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									}
									$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
									if($since_start->invert == 1){
										$shift_early = 1;
										$late_time = '00:00:00';
									} else {
										$late_mark = 0;
									}
								}
								//echo 'out';exit;
								
								//if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
									//$abnormal_status = 1;
								//} else {
									if($shift_data['shift_id'] == '1'){
										/*
										if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
											$first_half = 0;
											$late_mark = 0;
											if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
												if($shift_early == 1){
													if(strtotime($act_outtime) < strtotime($shift_intime)){
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 6){
													$second_half = 1;
												} elseif( ($since_start->h == 5 && $since_start->i >= 51)){
													$second_half = 1;
													$early_mark = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 31) || $since_start->h > 4){
													$second_half = 1;
												} else {
													$second_half = 0;
												}
												//if( (($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4) && $since_start->invert == 0){
												// if( $since_start->h >= 3 && $since_start->invert == 0){
												// 	$second_half = 1;
												// } else {
												// 	$second_half = 0;
												// }
											} else {
												$second_half = 0;
											}
										} else {
										*/
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
													$first_half = 1;
													$second_half = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										//}
									} else {
										/*
										if( (($since_start->h == 1 && $since_start->i >= 1) || $since_start->h > 1) && $since_start->invert == 0){
											$first_half = 0;
											$late_mark = 0;
											if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
												if($shift_early == 1){
													if(strtotime($act_outtime) < strtotime($shift_intime)){
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if(($since_start->h == 7 && $since_start->i >= 31) || $since_start->h > 7){
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$second_half = 1;
												} else {
													$second_half = 0;
												}
											} else {
												$second_half = 0;
											}
										} else {
										*/
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										//}
									}
								//}
								//echo 'out';exit;
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$early_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$early_time = '00:00:00';
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									if($shift_data['shift_id'] == '1'){
										$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
									}
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										if($shift_data['shift_id'] == '1'){
											$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										}
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
									if($since_start->invert == 0){
										$early_time = '00:00:00';
									}
								}					
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							/*
							if($first_half == 1){
								$secs = strtotime($late_time)-strtotime("00:00:00");
								$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
								$total_late_early_time_exp = explode(':', $total_late_early_time);
								if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
									$second_half = 0;
								}
							}
							// $month = 1;
							// $day = 26;
							// $year = 2018;
							if($day <= 25){
								if($month == 1){
									$c_month = 12;
									$c_year = $year - 1;
								} else {
									$c_month = $month - 1;
									$c_year = $year;
								}
								$c_month = sprintf("%02d", $c_month);
								$c_year = sprintf("%02d", $c_year);
							} else {
								$c_month = sprintf("%02d", $month);
								$c_year = sprintf("%02d", $year);
							}

							$from_date = $c_year.'-'.$c_month.'-26';
							$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
							$late_datas = query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ", $conn)->row;
							$early_datas = query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `early_mark` = '1' ", $conn)->row;
							$db_late_mark = $late_datas['total_late_mark'];
							
							// echo '<pre>';
							// print_r($from_date);
							// echo '<pre>';
							// print_r($to_date);
							// echo '<pre>';
							// print_r($late_datas);
							// echo '<pre>';
							// print_r($early_datas);
							
							if($first_half == 1 || $second_half == 1){
								$db_late_mark = $late_datas['total_late_mark'];
								$db_early_mark  = $early_datas['total_early_mark'];
								$total_late_early_mark = $db_late_mark + $db_early_mark + $late_mark + $early_mark;
								if($late_mark == 1){
									if($total_late_early_mark <= 2){
									} else {
										$second_half = 0;	
									}
								}
								if($early_mark == 1){
									if($total_late_early_mark <= 2){
									} else {
										$second_half = 0;	
									}
								}
							} else {
								if($first_half == 0 && $second_half == 0){
									$late_mark = 0;
									$early_mark = 0;
								}
							}
							*/
							/*
							if($late_datas['total_late_mark'] <= 2){
								if($late_mark == 1){
									if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
										$first_half = 0;
									} else {
										//$late_mark_var = 1;
										//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
										$db_late_mark = $late_datas['total_late_mark'] + 1;
										if($db_late_mark == 3){
											$first_half = 0;
										}
									}
								}
							} else {
								if($late_mark == 1){
									$first_half = 0;	
								}
							}
							if($early_datas['total_early_mark'] == 0){
								if($early_mark == 1){
									if($db_late_mark == 2){
										//$early_mark_var = 1;
										//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
										//$second_half = 1;
									} else {
										$second_half = 0;
									}
								}
							} else {
								if($early_mark == 1){
									$second_half = 0;	
								}	
							}
							*/

							// echo $first_half;
							// echo '<br/ >';
							// echo $second_half;
							// echo '<br/ >';
							// echo $late_mark;
							// echo '<br />';
							// echo $early_mark;
							// echo '<br />';
							// echo $late_time;
							// echo '<br/ >';
							// echo $early_time;
							// echo '<br/ >';
							// echo $total_late_early_time;
							// echo '<br/ >';
							// exit;

							$working_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									if($shift_early == 1){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											}
										} else {
											if(strtotime($act_outtime) < strtotime($shift_intime)){
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											}
										}
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									}
									//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}
							$abs_stat = 0;
							if($first_half == 1 && $second_half == 1){
								$present_status = 1;
								$absent_status = 0;
							} elseif($first_half == 1 && $second_half == 0){
								$present_status = 0.5;
								$absent_status = 0.5;
							} elseif($first_half == 0 && $second_half == 1){
								$present_status = 0.5;
								$absent_status = 0.5;
							} else {
								$present_status = 0;
								$absent_status = 1;
							}
							$day = date('j', strtotime($rvaluess['punch_date']));
							$month = date('n', strtotime($rvaluess['punch_date']));
							$year = date('Y', strtotime($rvaluess['punch_date']));
							if($trans_exist->num_rows == 0){
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' ";
								} else {
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->getLastId();
							} else {
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
							}
							//echo $sql.';';
							//echo '<br />';
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = $this->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$act_in_punch_date = $filter_date_start;
							$act_out_punch_date = $filter_date_start;
							
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){ 
								$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							} else {
								/*
								if($trans_exist->row['act_intime'] == '00:00:00'){
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){	
										$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}	
								} else {
									$act_intime = $trans_exist->row['act_intime'];
									$act_in_punch_date = $trans_exist->row['date'];	
								}
							
								$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
								$new_data_exist = $this->db->query($new_data_sql);
								if($new_data_exist->num_rows > 0){
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									$act_outtime = $trans_exist->row['act_outtime'];
									$act_out_punch_date = $trans_exist->row['date_out'];
								}
								*/
								$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							}
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}
							$abnormal_status = 0;
							$first_half = 0;
							$second_half = 0;
							$late_time = '00:00:00';
							//$act_intime = '09:59:00';
							//$act_outtime = '19:45:00';
							$late_mark = 0;
							$early_mark = 0;
							$shift_early = 0;
							if($act_intime != '00:00:00'){
								if($shift_data['shift_id'] == '1'){
									$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
									//echo $shift_intime_plus_one;exit;
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->h > 0){
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									} else {
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									}
									
									$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
									if($since_start->invert == 1){
										//$shift_early = 1;
										$late_time = '00:00:00';
									} else {
										$late_mark = 1;
									}
									//echo $late_time;exit;

									$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->invert == 1){
										$shift_early = 1;
										//$late_time = '00:00:00';
									} else {
										//if($late_min > 0){
											//$late_mark = 1;
										//} else {
											//$late_mark = 0;
										//}
									}
								} else {
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									if($since_start->h > 0){
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									} else {
										$late_hour = $since_start->h;
										$late_min = $since_start->i;
										$late_sec = $since_start->s;
									}
									$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
									if($since_start->invert == 1){
										$shift_early = 1;
										$late_time = '00:00:00';
									} else {
										$late_mark = 0;
									}
								}
								//echo 'out';exit;
								
								//if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
									//$abnormal_status = 1;
								//} else {
									if($shift_data['shift_id'] == '1'){
										/*
										if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
											$first_half = 0;
											$late_mark = 0;
											if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
												if($shift_early == 1){
													if(strtotime($act_outtime) < strtotime($shift_intime)){
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 6){
													$second_half = 1;
												} elseif( ($since_start->h == 5 && $since_start->i >= 51)){
													$second_half = 1;
													$early_mark = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 31) || $since_start->h > 4){
													$second_half = 1;
												} else {
													$second_half = 0;
												}
												//if( (($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4) && $since_start->invert == 0){
												// if( $since_start->h >= 3 && $since_start->invert == 0){
												// 	$second_half = 1;
												// } else {
												// 	$second_half = 0;
												// }
											} else {
												$second_half = 0;
											}
										} else {
										*/
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
													$first_half = 1;
													$second_half = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										//}
									} else {
										/*
										if( (($since_start->h == 1 && $since_start->i >= 1) || $since_start->h > 1) && $since_start->invert == 0){
											$first_half = 0;
											$late_mark = 0;
											if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
												if($shift_early == 1){
													if(strtotime($act_outtime) < strtotime($shift_intime)){
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if(($since_start->h == 7 && $since_start->i >= 31) || $since_start->h > 7){
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$second_half = 1;
												} else {
													$second_half = 0;
												}
											} else {
												$second_half = 0;
											}
										} else {
										*/
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										//}
									}
								//}
								//echo 'out';exit;
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$early_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$early_time = '00:00:00';
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									if($shift_data['shift_id'] == '1'){
										$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
									}
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										if($shift_data['shift_id'] == '1'){
											$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										}
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
									if($since_start->invert == 0){
										$early_time = '00:00:00';
									}
								}					
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							/*
							if($first_half == 1){
								$secs = strtotime($late_time)-strtotime("00:00:00");
								$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
								$total_late_early_time_exp = explode(':', $total_late_early_time);
								if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
									$second_half = 0;
								}
							}
							// $month = 1;
							// $day = 26;
							// $year = 2018;
							if($day <= 25){
								if($month == 1){
									$c_month = 12;
									$c_year = $year - 1;
								} else {
									$c_month = $month - 1;
									$c_year = $year;
								}
								$c_month = sprintf("%02d", $c_month);
								$c_year = sprintf("%02d", $c_year);
							} else {
								$c_month = sprintf("%02d", $month);
								$c_year = sprintf("%02d", $year);
							}

							$from_date = $c_year.'-'.$c_month.'-26';
							$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
							$late_datas = query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ", $conn)->row;
							$early_datas = query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `early_mark` = '1' ", $conn)->row;
							$db_late_mark = $late_datas['total_late_mark'];
							
							// echo '<pre>';
							// print_r($from_date);
							// echo '<pre>';
							// print_r($to_date);
							// echo '<pre>';
							// print_r($late_datas);
							// echo '<pre>';
							// print_r($early_datas);
							
							if($first_half == 1 || $second_half == 1){
								$db_late_mark = $late_datas['total_late_mark'];
								$db_early_mark  = $early_datas['total_early_mark'];
								$total_late_early_mark = $db_late_mark + $db_early_mark + $late_mark + $early_mark;
								if($late_mark == 1){
									if($total_late_early_mark <= 2){
									} else {
										$second_half = 0;	
									}
								}
								if($early_mark == 1){
									if($total_late_early_mark <= 2){
									} else {
										$second_half = 0;	
									}
								}
							} else {
								if($first_half == 0 && $second_half == 0){
									$late_mark = 0;
									$early_mark = 0;
								}
							}
							*/
							/*
							if($late_datas['total_late_mark'] <= 2){
								if($late_mark == 1){
									if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
										$first_half = 0;
									} else {
										//$late_mark_var = 1;
										//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
										$db_late_mark = $late_datas['total_late_mark'] + 1;
										if($db_late_mark == 3){
											$first_half = 0;
										}
									}
								}
							} else {
								if($late_mark == 1){
									$first_half = 0;	
								}
							}
							if($early_datas['total_early_mark'] == 0){
								if($early_mark == 1){
									if($db_late_mark == 2){
										//$early_mark_var = 1;
										//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
										//$second_half = 1;
									} else {
										$second_half = 0;
									}
								}
							} else {
								if($early_mark == 1){
									$second_half = 0;	
								}	
							}
							*/

							// echo $first_half;
							// echo '<br/ >';
							// echo $second_half;
							// echo '<br/ >';
							// echo $late_mark;
							// echo '<br />';
							// echo $early_mark;
							// echo '<br />';
							// echo $late_time;
							// echo '<br/ >';
							// echo $early_time;
							// echo '<br/ >';
							// echo $total_late_early_time;
							// echo '<br/ >';
							// exit;

							$working_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									if($shift_early == 1){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											}
										} else {
											if(strtotime($act_outtime) < strtotime($shift_intime)){
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											}
										}
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									}
									//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}
							$abs_stat = 0;
							if($first_half == 1 && $second_half == 1){
								$present_status = 1;
								$absent_status = 0;
							} elseif($first_half == 1 && $second_half == 0){
								$present_status = 0.5;
								$absent_status = 0.5;
							} elseif($first_half == 0 && $second_half == 1){
								$present_status = 0.5;
								$absent_status = 0.5;
							} else {
								$present_status = 0;
								$absent_status = 1;
							}

							if($trans_exist->num_rows == 0){
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' ";
								} else {
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->getLastId();
							} else {
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
							}
							//echo $sql.';';
							//echo '<br />';
						} elseif($schedule_raw[0] == 'HD'){
							$shift_data = $this->getshiftdata($schedule_raw[1]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata('1');
							}
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								if($shift_data['shift_id'] == '1'){
									if($emp_data['sat_status'] == '1'){
										$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
										$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
									} else {
										$shift_outtime = $shift_data['out_time'];
									}
								} else {
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
								}
								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									/*
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
								
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
									*/
									$act_intimes = $this->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								}

								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								$abnormal_status = 0;
								$first_half = 0;
								$second_half = 0;
								$late_time = '00:00:00';
								//$act_intime = '09:59:00';
								//$act_outtime = '19:45:00';
								$late_mark = 0;
								$early_mark = 0;
								$shift_early = 0;
								if($act_intime != '00:00:00'){
									if($shift_data['shift_id'] == '1'){
										$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
										//echo $shift_intime_plus_one;exit;
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											//$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 1;
										}
										//echo $late_time;exit;

										$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->invert == 1){
											$shift_early = 1;
											//$late_time = '00:00:00';
										} else {
											//if($late_min > 0){
												//$late_mark = 1;
											//} else {
												//$late_mark = 0;
											//}
										}
									} else {
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										if($since_start->h > 0){
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										} else {
											$late_hour = $since_start->h;
											$late_min = $since_start->i;
											$late_sec = $since_start->s;
										}
										$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
										if($since_start->invert == 1){
											$shift_early = 1;
											$late_time = '00:00:00';
										} else {
											$late_mark = 0;
										}
									}
									//echo 'out';exit;
									
									//if($since_start->invert == 1 && $since_start->h >= 6){ //abnormal status condition for morning early comming
										//$abnormal_status = 1;
									//} else {
										if($shift_data['shift_id'] == '1'){
											/*
											if( (($since_start->h == 2 && $since_start->i >= 1) || $since_start->h > 2) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if($since_start->h >= 6){
														$second_half = 1;
													} elseif( ($since_start->h == 5 && $since_start->i >= 51)){
														$second_half = 1;
														$early_mark = 1;
													} elseif( ($since_start->h == 4 && $since_start->i >= 31) || $since_start->h > 4){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
													//if( (($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4) && $since_start->invert == 0){
													// if( $since_start->h >= 3 && $since_start->invert == 0){
													// 	$second_half = 1;
													// } else {
													// 	$second_half = 0;
													// }
												} else {
													$second_half = 0;
												}
											} else {
											*/
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														if($shift_data['shift_id'] == '1'){
															$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
															if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
															}
														} else {
															if(strtotime($act_outtime) < strtotime($shift_intime)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
															}
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
														$first_half = 1;
														$second_half = 1;
													} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											//}
										} else {
											/*
											if( (($since_start->h == 1 && $since_start->i >= 1) || $since_start->h > 1) && $since_start->invert == 0){
												$first_half = 0;
												$late_mark = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													if($shift_early == 1){
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if(($since_start->h == 7 && $since_start->i >= 31) || $since_start->h > 7){
														$second_half = 1;
													} elseif($since_start->h >= 6){
														$second_half = 1;
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
											*/
												if($act_outtime != '00:00:00'){
													if($shift_early == 1){
														if($shift_data['shift_id'] == '1'){
															$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
															if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
															}
														} else {
															if(strtotime($act_outtime) < strtotime($shift_intime)){
																$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
															} else {
																$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
															}
														}
													} else {
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													}
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
														$first_half = 1;
														$second_half = 1;
													} elseif($since_start->h >= 6){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											//}
										}
									//}
									//echo 'out';exit;
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$early_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										if($shift_data['shift_id'] == '1'){
											$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										}
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								/*
								if($first_half == 1){
									$secs = strtotime($late_time)-strtotime("00:00:00");
									$total_late_early_time = date("H:i:s",strtotime($early_time)+$secs);
									$total_late_early_time_exp = explode(':', $total_late_early_time);
									if( ($total_late_early_time_exp[0] == '01' && $total_late_early_time_exp[1] == '00') || $total_late_early_time_exp[0] > '01'){
										$second_half = 0;
									}
								}
								// $month = 1;
								// $day = 26;
								// $year = 2018;
								if($day <= 25){
									if($month == 1){
										$c_month = 12;
										$c_year = $year - 1;
									} else {
										$c_month = $month - 1;
										$c_year = $year;
									}
									$c_month = sprintf("%02d", $c_month);
									$c_year = sprintf("%02d", $c_year);
								} else {
									$c_month = sprintf("%02d", $month);
									$c_year = sprintf("%02d", $year);
								}

								$from_date = $c_year.'-'.$c_month.'-26';
								$to_date = date('Y-m-d', strtotime($filter_date_start . ' -1 day'));
								$late_datas = query("SELECT COUNT(*) as total_late_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `late_mark` = '1' ", $conn)->row;
								$early_datas = query("SELECT COUNT(*) as total_early_mark FROM `oc_transaction` WHERE `emp_id` = '".$emp_data['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' AND `early_mark` = '1' ", $conn)->row;
								$db_late_mark = $late_datas['total_late_mark'];
								
								// echo '<pre>';
								// print_r($from_date);
								// echo '<pre>';
								// print_r($to_date);
								// echo '<pre>';
								// print_r($late_datas);
								// echo '<pre>';
								// print_r($early_datas);
								
								if($first_half == 1 || $second_half == 1){
									$db_late_mark = $late_datas['total_late_mark'];
									$db_early_mark  = $early_datas['total_early_mark'];
									$total_late_early_mark = $db_late_mark + $db_early_mark + $late_mark + $early_mark;
									if($late_mark == 1){
										if($total_late_early_mark <= 2){
										} else {
											$second_half = 0;	
										}
									}
									if($early_mark == 1){
										if($total_late_early_mark <= 2){
										} else {
											$second_half = 0;	
										}
									}
								} else {
									if($first_half == 0 && $second_half == 0){
										$late_mark = 0;
										$early_mark = 0;
									}
								}
								*/
								/*
								if($late_datas['total_late_mark'] <= 2){
									if($late_mark == 1){
										if($late_datas['total_late_mark'] == 2 && $early_datas['total_early_mark'] == 1){
											$first_half = 0;
										} else {
											//$late_mark_var = 1;
											//$this->db->query("UPDATE `oc_early_late` SET late_mark = (late_mark + ".(int)$late_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
											$db_late_mark = $late_datas['total_late_mark'] + 1;
											if($db_late_mark == 3){
												$first_half = 0;
											}
										}
									}
								} else {
									if($late_mark == 1){
										$first_half = 0;	
									}
								}
								if($early_datas['total_early_mark'] == 0){
									if($early_mark == 1){
										if($db_late_mark == 2){
											//$early_mark_var = 1;
											//$this->db->query("UPDATE `oc_early_late` SET early_mark = (early_mark + ".(int)$early_mark_var.") WHERE `emp_code` = '".$emp_data['emp_code']."' AND `month` = '".$c_month."' AND `year` = '".$c_year."' ");
											//$second_half = 1;
										} else {
											$second_half = 0;
										}
									}
								} else {
									if($early_mark == 1){
										$second_half = 0;	
									}	
								}
								*/

								// echo $first_half;
								// echo '<br/ >';
								// echo $second_half;
								// echo '<br/ >';
								// echo $late_mark;
								// echo '<br />';
								// echo $early_mark;
								// echo '<br />';
								// echo $late_time;
								// echo '<br/ >';
								// echo $early_time;
								// echo '<br/ >';
								// echo $total_late_early_time;
								// echo '<br/ >';
								// exit;

								$working_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										if($shift_early == 1){
											if($shift_data['shift_id'] == '1'){
												$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
												if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												}
											} else {
												if(strtotime($act_outtime) < strtotime($shift_intime)){
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										}
										//$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}
								// echo $shift_early;
								// echo '<br />';
								// echo $working_time;exit;

								$abs_stat = 0;
								
								if($first_half == 1 && $second_half == 1){
									$present_status = 1;
									$absent_status = 0;
								} elseif($first_half == 1 && $second_half == 0){
									$present_status = 0.5;
									$absent_status = 0.5;
								} elseif($first_half == 0 && $second_half == 1){
									$present_status = 0.5;
									$absent_status = 0.5;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								$day = date('j', strtotime($rvaluess['punch_date']));
								$month = date('n', strtotime($rvaluess['punch_date']));
								$year = date('Y', strtotime($rvaluess['punch_date']));
								//echo 'out';exit;
								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', division = '".$emp_data['division']."', region = '".$emp_data['region']."', region_id = '".$emp_data['region_id']."', division_id = '".$emp_data['division_id']."', unit_id = '".$emp_data['unit_id']."', department_id = '".$emp_data['department_id']."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								// echo $sql.';';
								// echo '<br />';
								// exit;
								
							}
							//echo 'out1';exit;
						}
						if($act_in_punch_date == $act_out_punch_date) {
							if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
							} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
							} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
							}
						} else {
							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
							$this->db->query($update);
							
							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
							$this->db->query($update);
						}
					}
				}
			}
		}
		//echo 'Done';exit;
		 
		if($current_processed_data){
			$final_datas = array();
			$done_transaction_id = array();
			foreach($current_processed_data as $ckey => $cvalue){
				$transaction_ids = $this->db->query("SELECT `transaction_id` FROM `oc_attendance` WHERE `id` = '".$cvalue."' ");
				if($transaction_ids->num_rows > 0){
					$transaction_id = $transaction_ids->row['transaction_id'];
					if(!isset($done_transaction_id[$transaction_id])){
						$done_transaction_id[$transaction_id] = $transaction_id;
						$transaction_datass = $this->db->query("SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."' ");
						if($transaction_datass->num_rows > 0){
							$transaction_datas = $transaction_datass->rows;
							foreach($transaction_datas as $tkey => $tvalue){
								$emp_data = $this->getEmployees_dat($tvalue['emp_id']);
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeId'] = $tvalue['emp_id'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeName'] = $tvalue['emp_name'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['PunchID'] = $tvalue['emp_id'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Department'] = $tvalue['department'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Site'] = $emp_data['unit'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Region'] = $emp_data['region'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Division'] = $emp_data['division'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['AttendDate'] = date('Y-m-d', strtotime($tvalue['date']));
								
								$date_to_compare = $tvalue['date'];
								//if(strtotime($emp_data['doj']) > strtotime($date_to_compare)){
									//$status = 'X';
								//} else
								if($tvalue['leave_status'] != '0'){
									if($tvalue['leave_status'] == '0.5'){
										$status = 'PL';
									} else {
										$status = 'PL';
									}
								} elseif($tvalue['weekly_off'] != '0'){
									$working_times = explode(':', $tvalue['working_time']);
									$working_hours = $working_times[0];
									if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
									//if($working_hours >= '06'){
										$status = 'PW';
									} else {
										$status = 'W';	
									}
								} elseif($tvalue['holiday_id'] != '0'){
									$working_times = explode(':', $tvalue['working_time']);
									$working_hours = $working_times[0];
									if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
									//if($working_hours >= '06'){
										$status = 'PPH';
									} else {
										$status = 'H';
									}
								} elseif($tvalue['present_status'] == '1'){
									$status = 'P';
								} elseif($tvalue['absent_status'] == '1'){
									$status = 'A';
								} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5'){
									$status = 'HD';
								}

								$final_datas[$tvalue['date']][$tvalue['emp_id']]['Status'] = $status;
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['InTime'] = $tvalue['act_intime'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['OutTime'] = $tvalue['act_outtime'];
								$final_datas[$tvalue['date']][$tvalue['emp_id']]['TotaHour'] = $tvalue['working_time'];
							}
						}
					}
				}
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;
			foreach($final_datas as $fkeys => $fvalues){
				foreach($fvalues as $fkey => $fvalue){
					/*
					$current_date = date('Y-m-d H:i:s');
					$insert_sql = "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', '".$current_date."', '0') ";
					$stmt = sqlsrv_query($conn1, $insert_sql);
					if($stmt == false){
						// echo 'Not Executed Start';
						// echo '<br />';
						// echo "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', null, '0') ";
						// echo '<br />';
						// echo '<pre>';
						// print_r(sqlsrv_errors());
						// echo '<br />';
						// echo 'Not Executed End';
						// echo '<br />';
					}
					*/
					
					$sql = "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
					$params = array();
					$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt = sqlsrv_query($conn1, $sql, $params, $options);
					if( $stmt === false) {
						// echo 'Not Executed Start';
						// echo '<br />';
						// echo "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
						// echo '<br />';
						// echo '<pre>';
						// print_r(sqlsrv_errors());
						// echo '<br />';
						// echo 'Not Executed End';
						// echo '<br />';
					}
					$is_exist = 0;
					$id = 0;
					while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
						$is_exist = 1;
						$id = $row['Id'];
					}
					
					$current_date = date('Y-m-d H:i:s');
					if($is_exist == 1){
						$update_sql = "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '".$current_date."', ProcessStatus = '0' WHERE Id = '".$id."' ";
						$stmt = sqlsrv_query($conn1, $update_sql);
						if($stmt == false){
							//echo 'Not Executed Start';
							//echo '<br />';
							//echo "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '0000-00-00 00:00:00' WHERE Id = '".$id."' ";
							//echo '<br />';
							//echo '<pre>';
							//print_r(sqlsrv_errors());
							//echo '<br />';
							//echo 'Not Executed End';
							//echo '<br />';
						}
					} else {
						$insert_sql = "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', '".$current_date."', '0') ";
						$stmt = sqlsrv_query($conn1, $insert_sql);
						if($stmt == false){
							// echo 'Not Executed Start';
							// echo '<br />';
							// echo "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', null, '0') ";
							// echo '<br />';
							// echo '<pre>';
							// print_r(sqlsrv_errors());
							// echo '<br />';
							// echo 'Not Executed End';
							// echo '<br />';
						}
					}
				}	
			}

			// foreach($final_datas as $fkeys => $fvalues){
			// 	foreach($fvalues as $fkey => $fvalue){
			// 		$is_exists = query("SELECT `id` FROM `oc_transaction_jmc` WHERE `EmployeeId` = '".$fvalue['EmployeeId']."' AND `AttendDate` = '".$fvalue['AttendDate']."' ", $conn);
			// 		if($is_exists->num_rows > 0){
			// 			$id = $is_exists->row['id'];
			// 			$update_sql = "UPDATE `oc_transaction_jmc` SET `Status` = '".$fvalue['Status']."', `InTime` = '".$fvalue['InTime']."', `OutTime` = '".$fvalue['OutTime']."', `TotaHour` = '".$fvalue['TotaHour']."', `JmcDate` = '0000-00-00 00:00:00' WHERE `Id` = '".$id."' ";
			// 			query($update_sql, $conn);
			// 		} else {
			// 			$insert_sql = "INSERT INTO `oc_transaction_jmc` SET `EmployeeId` = '".$fvalue['EmployeeId']."', `EmployeeName` = '".$fvalue['EmployeeName']."', `PunchID` = '".$fvalue['PunchID']."', `Department` = '".$fvalue['Department']."', `Site` = '".$fvalue['Site']."', `Region` = '".$fvalue['Region']."', `Division` = '".$fvalue['Division']."', `AttendDate` = '".$fvalue['AttendDate']."', `Status` = '".$fvalue['Status']."', `InTime` = '".$fvalue['InTime']."', `OutTime` = '".$fvalue['OutTime']."', `TotaHour` = '".$fvalue['TotaHour']."' ";
			// 			query($insert_sql, $conn);
			// 		}
			// 	}	
			// }
		}
		// echo '<pre>';
		// print_r($content);
		// exit;
		
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	public function getEmployees_dat($emp_code) {
		$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getempdata($emp_code) {
		$sql = "SELECT * FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insert_attendance_data($data) {
		$sql = $this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."' ");
	}
	public function getemployees($data) {
		$sql = "SELECT * FROM `oc_employee` WHERE `status` = '1' ";
		if(!empty($data['filter_name_id'])){
			$sql .= " AND `emp_code` = '".$data['filter_name_id']."' ";
		}
		$sql .= " ORDER BY `shift_type` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getrawattendance_group_date_custom($emp_code, $date) {
		$query = $this->db->query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		return $query->row;
	}
	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC ");
		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			//echo '<pre>';
			//print_r($hour_array);

			//echo '<pre>';
			//print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 15){
					unset($hour_array[$ckey]);
				}
			}

			//echo '<pre>';
			//print_r($hour_array);
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}
			//echo '<pre>';
			//print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		//echo '<pre>';
		//print_r($act_outtime);
		//exit;		
		return $act_outtime;
	}
	public function getorderhistory($data){
		$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}
}
?>