<?php 
class ControllerToolRecalculate extends Controller { 
	private $error = array();
	public function index() {	
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-01');
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		$this->language->load('tool/recalculate');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_recalculate'] = $this->language->get('entry_recalculate');

		$this->data['button_recalculate'] = $this->language->get('button_recalculate');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;

		$this->data['token'] = $this->session->data['token'];
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);

		$this->template = 'tool/recalculate.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	

	public function recalculate_data() {
		$this->language->load('tool/recalculate');
		
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
			$filter_date_start_constant = $filter_date_start;
		} else {
			$filter_date_start = date('Y-m-01');//date('Y-m-d');
			$filter_date_start_constant = $filter_date_start;
		}

		if(isset($this->request->get['filter_date_end'])){
			$filter_date_end = $this->request->get['filter_date_end'];
			$filter_date_end_constant = $filter_date_end;
		} else {
			$filter_date_end = date('Y-m-d');//date('Y-m-d');
			$filter_date_end_constant = $filter_date_end;
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$filter_unit = html_entity_decode($this->request->get['unit']);
		} else {
			$filter_unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$filter_department = html_entity_decode($this->request->get['department']);
		} else {
			$filter_department = 0;
		}

		if (isset($this->request->get['division'])) {
			$filter_division = html_entity_decode($this->request->get['division']);
		} else {
			$filter_division = 0;
		}

		if (isset($this->request->get['region'])) {
			$filter_region = html_entity_decode($this->request->get['region']);
		} else {
			$filter_region = 0;
		}

		if (isset($this->request->get['company'])) {
			$filter_company = html_entity_decode($this->request->get['company']);
		} else {
			$filter_company = 0;
		}

		$stats_sql = "SELECT * FROM `oc_status` WHERE `process_status` = '1' ";
		$stats_data = $this->db->query($stats_sql);
		// echo '<pre>';
		// print_r($stats_data);
		// exit;
		if($stats_data->num_rows == 0){
			$sql = "UPDATE `oc_status` SET `process_status` = '1' ";
			$this->db->query($sql);
		
			$serverName = "10.200.1.35";
			$connectionInfo = array("Database"=>"SmartOffice", "UID"=>"avi", "PWD"=>"avi2123");
			//$connectionInfo = array("Database"=>"SmartOffice");
			$conn1 = sqlsrv_connect($serverName, $connectionInfo);
			if($conn1) {
				//echo "Connection established.<br />";exit;
			} else {
				// echo "Connection could not be established.<br />";
				// echo "<br />";
				$dat['connection'] = 'Connection could not be established';
				// echo '<pre>';
				// print_r(sqlsrv_errors());
				// exit;
				//die( print_r( sqlsrv_errors(), true));
			}

			
			if($filter_name_id != ''){
				$sql = "SELECT * FROM SmartOffice.dbo.Staffdevicelogsnew WHERE CAST(LogDate as DATE) >= '".$filter_date_start."' AND CAST(LogDate as DATE) <= '".$filter_date_end."' AND UserId = '".$filter_name_id."' ORDER BY DeviceLogId ";
			} else {
				$sql = "SELECT * FROM SmartOffice.dbo.Staffdevicelogsnew WHERE CAST(LogDate as DATE) >= '".$filter_date_start."' AND CAST(LogDate as DATE) <= '".$filter_date_end."' ORDER BY DeviceLogId ";
			}
			//echo $sql;exit;
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt = sqlsrv_query($conn1, $sql, $params, $options);
			if( $stmt === false) {
			   //echo'<pre>';
			   //print_r(sqlsrv_errors());
			   //exit;
			}
			$exp_datas = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				$user_id = $row['UserId'];
				$device_id = $row['DeviceId'];
				// $user_id = sprintf('%04d', $row['UserId']);
				// $device_id = sprintf('%02d', $row['DeviceId']);
				$log_id = $row['DeviceLogId'];
				$logdates = $row['LogDate'];

				if($logdates != null && strlen($user_id) < 10 && is_numeric($user_id)){	
					$logdate = $logdates->format("Y-m-d");
					$logyear = $logdates->format("Y");
					$log_month = $logdates->format("n");

					$logtimes = $row['LogDate'];
					$logtime = $logtimes->format("H:i:s");
					if($logyear >= '2020' && $logtime != '00:00:00'){
						$download_dates = $row['DownloadDate'];
						$download_date = $download_dates->format("Y-m-d");
						$exp_datas[] = array(
							'user_id' => $user_id,	
							'log_id' => $log_id,
							'download_date' => $download_date,
							'date' => $logdate,
							'time' => $logtime,
							'device_id' => $device_id,
							'logyear' => $logyear,
							'log_month' => $log_month,
						);
					}
					//break;
				}
			}
			//$exp_datas = array();
			$employees = array();
			$employees_final = array();
			foreach($exp_datas as $data) {
				$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
				$in_time = $data['time'];//substr($tdata, 6, 5);
				if($emp_id != '') {
					$result = $this->getEmployees_dat($emp_id);
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'emp_name' => $result['name'],
							'card_id' => $result['emp_code'],
							'department' => $result['department'],
							'unit' => $result['unit'],
							'logyear' => $data['logyear'],
							'log_month' => $data['log_month'],
							'group' => '',//$result['group'],
							'in_time' => $in_time,
							'device_id' => $data['device_id'],
							'punch_date' => $data['date'],
							'download_date' => $data['download_date'],
							'log_id' => $data['log_id'],
							'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
						);
					} else {
						$employees[] = array(
							'employee_id' => $emp_id,
							'emp_name' => '',
							'card_id' => $emp_id,
							'department' => '',
							'unit' => '',
							'group' => '',//$result['group'],
							'logyear' => $data['logyear'],
							'log_month' => $data['log_month'],
							'in_time' => $in_time,
							'device_id' => $data['device_id'],
							'punch_date' => $data['date'],
							'download_date' => $data['download_date'],
							'log_id' => $data['log_id'],
							'fdate' => date('Y-m-d H:i:s', strtotime($data['date'].' '.$in_time))
						);
					}
				}
			}
			usort($employees, array($this, "sortByOrder"));
			$o_emp = array();
			foreach ($employees as $ekey => $evalue) {
				$employees_final[] = $evalue;
			}

			if(isset($employees_final) && count($employees_final) > 0){
				foreach ($employees_final as $fkey => $employee) {
					$exist = $this->getorderhistory($employee);
					if($exist == 0){
						$mystring = $employee['in_time'];
						$findme   = ':';
						$pos = strpos($mystring, $findme);
						if($pos !== false){
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
						} else {
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
						}
						$employee['in_time'] = $in_times;
						$this->insert_attendance_data($employee);
					}
				}
			}
			$current_month = date('n');
			$c_year = date('Y');
			$start_month =  date('n', strtotime($filter_date_start_constant));
			$start_year =  date('Y', strtotime($filter_date_start_constant));

			$end_month =  date('n', strtotime($filter_date_end_constant));
			$end_year =  date('Y', strtotime($filter_date_end_constant));
			$date_double = 0;
			if($start_month != $end_month){
				$date_double = 1;
				$atten_tblname = 'oc_attendance_'.$start_year.'_'.$start_month;
				$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;

				$atten_tblname_new = 'oc_attendance_'.$end_year.'_'.$end_month;
				$trans_tblname_new = 'oc_transaction_'.$end_year.'_'.$end_month;
			} else {
				$atten_tblname = 'oc_attendance_'.$start_year.'_'.$start_month;
				$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;
			}

			//$is_month_closed = $this->db->query("SELECT `date` FROM `".$trans_tblname."` WHERE `date` = '".$filter_date_start."' AND `month_close_status` = '1' LIMIT 1");
			
			//if($is_month_closed->num_rows == 0 ){
				$employee_datas_sql = "SELECT `name`, `emp_code`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`, `sat_status` FROM `oc_employee` WHERE 1=1 AND (`dol` = '0000-00-00' OR `dol` > '".$filter_date_start."')";
				if (!empty($filter_name_id)) {
					$employee_datas_sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($filter_name_id)) . "'";
				}

				if (!empty($filter_company)) {
					$company_string = str_replace("multiselect-all,", "", html_entity_decode($filter_company));
					$company_string = "'" . str_replace(",", "','", $company_string) . "'";
					$employee_datas_sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}

				if (!empty($filter_region)) {
					$employee_datas_sql .= " AND `region_id` = '" . $this->db->escape(strtolower($filter_region)) . "'";
				}

				if (!empty($filter_division)) {
					$employee_datas_sql .= " AND `division_id` = '" . $this->db->escape(strtolower($filter_division)) . "'";
				}

				if (!empty($filter_department)) {
					$employee_datas_sql .= " AND `department_id` = '" . $this->db->escape(strtolower($filter_department)) . "'";
				}

				if (!empty($filter_unit)) {
					$employee_datas_sql .= " AND `unit_id` = '" . $this->db->escape(strtolower($filter_unit)) . "'";
				}

				$employee_datas_sql .= " ORDER BY `emp_code` ";
				
				//echo $employee_datas_sql;exit;
				$employee_datas = $this->db->query($employee_datas_sql);
				// echo '<pre>';
				// print_r($employee_datas);
				// exit;
				if($employee_datas->num_rows > 0){
					$days = array();
					$dayss = $this->GetDays($filter_date_start_constant, $filter_date_end_constant);
					foreach ($dayss as $dkey => $dvalue) {
						if(strtotime($dvalue) < strtotime(date('Y-m-d'))){
							$dates = explode('-', $dvalue);
							$days[$dkey]['day'] = ltrim($dates[2], '0');
							$days[$dkey]['month'] = $dates[1];
							$days[$dkey]['year'] = $dates[0];
							$days[$dkey]['date'] = $dvalue;
							$exceed = 0;
						} else {
							$exceed = 1;
						}
					}
					
					if (!empty($filter_name_id)) {
						$this->log->write('Recalculate Start For Emp Code ' . $filter_name_id . ' And Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
					} else {
						$this->log->write('Recalculate Start For Date ' . $filter_date_start_constant . ' - ' . $filter_date_end_constant);
					}
					$cnt1 = 0;
					$cnt = 0;
					$date_string = '';
					$employee_string = '';
					$punch_date_array = array();
					foreach($employee_datas->rows as $rkey => $rvalue){
						$employee_string .= $rvalue['emp_code'].',';
					}
					$employee_string = rtrim($employee_string, ',');
					$employee_string = "'" . str_replace(",", "','", $employee_string) . "'";
					$date_string = rtrim($date_string, ', ');
					$date_string = "'" . str_replace(",", "','", $date_string) . "'";
					//$reset_attendance_sql = "UPDATE `oc_attendance` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND `punch_date` >= '".$filter_date_start_constant."' AND `punch_date` <= '".$filter_date_end_constant."' ";

					if($date_double == 1){
						$reset_attendance_sql = "UPDATE `".$atten_tblname."` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND `punch_date` >= '".$filter_date_start_constant."' AND `punch_date` <= '".$filter_date_end_constant."' ";
						$reset_attendance_sql1 = "UPDATE `".$atten_tblname_new."` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND `punch_date` >= '".$filter_date_start_constant."' AND `punch_date` <= '".$filter_date_end_constant."' ";
						$reset_transaction = "UPDATE `".$trans_tblname."` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`leave_status` = '0' AND `manual_status` = '0')";
						$reset_transaction1 = "UPDATE `".$trans_tblname_new."` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`leave_status` = '0' AND `manual_status` = '0')";
						
						$this->db->query($reset_attendance_sql);
						$this->db->query($reset_attendance_sql1);
						$this->db->query($reset_transaction);
						$this->db->query($reset_transaction1);

						$this->log->write($reset_attendance_sql);
						$this->log->write($reset_transaction);
						$this->log->write($reset_transaction1);
					} else {
						$reset_attendance_sql = "UPDATE `".$atten_tblname."` SET `status` = '0', `transaction_id` = '0' WHERE `emp_id` IN (" . $employee_string . ") AND `punch_date` >= '".$filter_date_start_constant."' AND `punch_date` <= '".$filter_date_end_constant."' ";
						$reset_transaction = "UPDATE `".$trans_tblname."` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`leave_status` = '0' AND `manual_status` = '0')";
						// echo $reset_transaction;
						// echo '<br />';
						// exit;
						$this->db->query($reset_attendance_sql);
						$this->db->query($reset_transaction);

						$this->log->write($reset_attendance_sql);
						$this->log->write($reset_transaction);
					}
					// echo $reset_attendance_sql;
					// echo '<br />';
					// exit;
					
					//$reset_transaction = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `date_out` = '0000-00-00', `late_time` = '00:00:00', `early_time` = '00:00:00', `late_mark` = '0', `early_mark` = '0', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$filter_date_start_constant."' AND `date` <= '".$filter_date_end_constant."' AND `emp_id` IN (" . strtolower($employee_string) . ") AND (`leave_status` = '0' AND `manual_status` = '0')";
					
					//echo 'out';exit;
					
					$batch_id = 0;
					$current_processed_data = array();
					$current_processed_data_date = array();
					foreach($employee_datas->rows as $rkey => $rvalue){
						$current_processed_data_date = array();
						$emp_code = $rvalue['emp_code'];
						if($date_double == 1){
							$unprocessed_dates1 = $this->db->query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `".$atten_tblname."` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` >= '".$filter_date_start_constant."') AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ")->rows;
							$unprocessed_dates2 = $this->db->query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `".$atten_tblname_new."` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` >= '".$filter_date_start_constant."') AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ")->rows;
							$unprocessed_dates = array_merge($unprocessed_dates1, $unprocessed_dates2);
						} else {
							$unprocessed_dates = $this->db->query("SELECT  a.`id`, a.`emp_id`, a.`punch_date`, a.`punch_time`, a.`device_id` FROM `".$atten_tblname."` a LEFT JOIN `oc_employee` e ON(e.`emp_code` = a.`emp_id`) WHERE (e.`dol` = '0000-00-00' OR e.`dol` >= '".$filter_date_start_constant."') AND a.`status` = '0' AND a.`punch_date` >= '".$filter_date_start_constant."' AND  a.`punch_date` <= '".$filter_date_end_constant."' AND a.`emp_id` = '".$rvalue['emp_code']."' ORDER BY a.`punch_date` ASC, a.`punch_time` ASC ")->rows;
						}

						foreach($unprocessed_dates as $dkey => $dvalue){
							$filter_date_start = $dvalue['punch_date'];
							$p_month = date('n', strtotime($filter_date_start));
							$p_year = date('Y', strtotime($filter_date_start));

							$trans_tbl_name_punch = 'oc_transaction_'.$p_year.'_'.$p_month;
							$atten_tbl_name_punch = 'oc_attendance_'.$p_year.'_'.$p_month;

							//$current_processed_data[$dvalue['id']] = $dvalue['id'];
							$current_processed_data[$dvalue['id']] = $dvalue['id'].','.$dvalue['punch_date'];

							$current_processed_data_date[$dvalue['id']] = $filter_date_start;
							$data['filter_name_id'] = $dvalue['emp_id']; 
							$rvalue = $this->getempdata($dvalue['emp_id'], $dvalue['punch_date']);
							$device_id = $dvalue['device_id'];
							$is_processed_status = $this->db->query("SELECT `status` FROM `".$atten_tbl_name_punch."` WHERE `id` = '".$dvalue['id']."' ")->row['status'];
							if(isset($rvalue['name']) && $rvalue['name'] != '' && $is_processed_status == '0'){
								$this->log->write('---start----');

								$emp_name = $rvalue['name'];
								$department = $rvalue['department'];
								$unit = $rvalue['unit'];
								$group = '';

								$day_date = date('j', strtotime($filter_date_start));
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$punch_time_exp = explode(':', $dvalue['punch_time']);
								if($punch_time_exp[0] >= '00' && $punch_time_exp[0] <= '04'){
									$filter_date_start = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
									$day_date = date('j', strtotime($filter_date_start));
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));				
								}

								//echo $filter_date_start;exit;

								$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								$shift_schedule = $this->db->query($update3)->row;
								if(!isset($shift_schedule[$day_date])){
									$shift_schedule[$day_date] = 'S_1';	
								}
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if(!isset($schedule_raw[2])){
									$schedule_raw[2] = 1;
								}
								// echo '<pre>';
								// print_r($schedule_raw);
								// exit;
								$transaction_id = 0;
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `".$trans_tbl_name_punch."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									// echo $trans_exist_sql;
									// echo '<br />';
									// echo '<pre>';
									// print_r($trans_exist);
									// exit;
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start, $atten_tbl_name_punch);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										//if($trans_exist->row['act_intime'] == '00:00:00'){
											$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start, $atten_tbl_name_punch);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										//} else {
											//$act_intime = $trans_exist->row['act_intime'];
											//$act_in_punch_date = $trans_exist->row['date'];
										//}
									}
									//echo $act_intime;exit;
									
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date, $atten_tbl_name_punch);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									// echo $act_intime;
									// echo '<br />';
									// echo $act_outtime;
									// echo '<br />';
									// exit;
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
											//echo $shift_intime_plus_one;exit;
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												//$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 1;
											}
											//echo $late_time;exit;
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->invert == 1){
												$shift_early = 1;
												//$late_time = '00:00:00';
											} else {
												//if($late_min > 0){
													//$late_mark = 1;
												//} else {
													//$late_mark = 0;
												//}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 0;
											}
										}
										
										if($shift_data['shift_id'] == '1'){
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												//if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										} elseif ($shift_data['shift_id'] == '2' || $shift_data['shift_id'] == '3') {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}

										} else {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 5){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												if($shift_data['shift_id'] == '1'){
													$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												}
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									//$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name_punch."` SET `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `transaction_id` = '".$transaction_id."' ";
											//echo $sql;exit;
											$this->db->query($sql);
											$this->log->write($sql);
											//$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
										}
									}
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;

									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `".$trans_tbl_name_punch."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start, $atten_tbl_name_punch);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										//if($trans_exist->row['act_intime'] == '00:00:00'){
											$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start,$atten_tbl_name_punch);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										//} else {
											//$act_intime = $trans_exist->row['act_intime'];
											//$act_in_punch_date = $trans_exist->row['date'];
										//}
									}
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date,$atten_tbl_name_punch);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}

									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
											//echo $shift_intime_plus_one;exit;
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												//$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 1;
											}
											//echo $late_time;exit;
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->invert == 1){
												$shift_early = 1;
												//$late_time = '00:00:00';
											} else {
												//if($late_min > 0){
													//$late_mark = 1;
												//} else {
													//$late_mark = 0;
												//}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 0;
											}
										}
										
										if($shift_data['shift_id'] == '1'){
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												//if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										} elseif ($shift_data['shift_id'] == '2' ||  $shift_data['shift_id'] == '3'){

											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}

										} else {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 5){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												if($shift_data['shift_id'] == '1'){
													$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												}
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									// echo $act_intime;
									// echo '<br />';
									// echo $act_outtime;
									// echo '<br />';
									// exit;

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									//$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name_punch."` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `halfday_status` = '0', `device_id` = '".$device_id."', `batch_id` = '".$batch_id."', `late_mark` = '".$late_mark."', `early_mark` = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
											// echo $sql;
											// echo '<br />';
											//exit;
											//$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
										}
									}
									//echo $sql.';';
									//echo '<br />';
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `".$trans_tbl_name_punch."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start,$atten_tbl_name_punch);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										//if($trans_exist->row['act_intime'] == '00:00:00'){
											$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start,$atten_tbl_name_punch);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										//} else {
											//$act_intime = $trans_exist->row['act_intime'];
											//$act_in_punch_date = $trans_exist->row['date'];
										//}
									}
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date,$atten_tbl_name_punch);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
											//echo $shift_intime_plus_one;exit;
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												//$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 1;
											}
											//echo $late_time;exit;
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->invert == 1){
												$shift_early = 1;
												//$late_time = '00:00:00';
											} else {
												//if($late_min > 0){
													//$late_mark = 1;
												//} else {
													//$late_mark = 0;
												//}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 0;
											}
										}
										
										if($shift_data['shift_id'] == '1'){
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												//if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										} elseif ($shift_data['shift_id'] == '2' ||  $shift_data['shift_id'] == '3') {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										} else {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 5){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												if($shift_data['shift_id'] == '1'){
													$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												}
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									//$trans_exist_sql = "SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name_punch."` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."', `late_time` = '".$late_time."', `early_time` = '".$early_time."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
											//$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
										}
									}
									//echo $sql.';';
									//echo '<br />';
								} elseif($schedule_raw[0] == 'HD'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									
									$shift_intime = $shift_data['in_time'];
									if($shift_data['shift_id'] == '1'){
										if($rvalue['sat_status'] == '1'){
											$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
											$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
										} else {
											$shift_outtime = $shift_data['out_time'];
										}
									} else {
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
									}
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT `transaction_id`, `act_intime`, `date`, `manual_status`, `leave_status` FROM `".$trans_tbl_name_punch."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start,$atten_tbl_name_punch);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									} else {
										//if($trans_exist->row['act_intime'] == '00:00:00'){
											$act_intimes = $this->getrawattendance_in_time($rvalue['emp_code'], $filter_date_start,$atten_tbl_name_punch);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										//} else {
											//$act_intime = $trans_exist->row['act_intime'];
											//$act_in_punch_date = $trans_exist->row['date'];
										//}
									}
									$act_outtimes = $this->getrawattendance_out_time($rvalue['emp_code'], $filter_date_start, $act_intime, $act_in_punch_date,$atten_tbl_name_punch);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$abnormal_status = 0;
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									$early_time = '00:00:00';
									$working_time = '00:00:00';
									$late_mark = 0;
									$early_mark = 0;
									$shift_early = 0;
									if($act_intime != '00:00:00'){
										if($shift_data['shift_id'] == '1'){
											$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												//$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 1;
											}
											//echo $late_time;exit;
											$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->invert == 1){
												$shift_early = 1;
												//$late_time = '00:00:00';
											} else {
												//if($late_min > 0){
													//$late_mark = 1;
												//} else {
													//$late_mark = 0;
												//}
											}
										} else {
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											if($since_start->h > 0){
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											} else {
												$late_hour = $since_start->h;
												$late_min = $since_start->i;
												$late_sec = $since_start->s;
											}
											$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
											if($since_start->invert == 1){
												$shift_early = 1;
												$late_time = '00:00:00';
											} else {
												$late_mark = 0;
											}
										}
										
										if($shift_data['shift_id'] == '1'){
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($rvalue['sat_status'] == '1'){
													if($since_start->h >= 3){
														$first_half = 1;
														$second_half = 1;
													} elseif($since_start->h >= 2){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													//if( ($since_start->h == 6 && $since_start->i >= 30) || $since_start->h > 6){
													if($since_start->h >= 8){
														$first_half = 1;
														$second_half = 1;
													} elseif( ($since_start->h == 4 && $since_start->i >= 30) || $since_start->h > 4){
														$first_half = 1;
														$second_half = 0;
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										} elseif ($shift_data['shift_id'] == '2' ||  $shift_data['shift_id'] == '3'){

											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if( ($since_start->h == 8 && $since_start->i >= 30) || $since_start->h > 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 6){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}

										} else {
											if($act_outtime != '00:00:00'){
												if($shift_early == 1){
													if($shift_data['shift_id'] == '1'){
														$shift_intime_minus_one = Date('H:i:s', strtotime($shift_intime .' -30 minutes'));
														if(strtotime($act_outtime) < strtotime($shift_intime_minus_one)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_minus_one);
														}
													} else {
														if(strtotime($act_outtime) < strtotime($shift_intime)){
															$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														} else {
															$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														}
													}
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												}
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h >= 8){
													$first_half = 1;
													$second_half = 1;
												} elseif($since_start->h >= 5){
													$first_half = 1;
													$second_half = 0;
												} else {
													$first_half = 0;
													$second_half = 0;
												}
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											} else {
												$first_half = 0;
												$second_half = 0;
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											if($shift_data['shift_id'] == '1'){
												$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
											} else {
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											}
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												if($shift_data['shift_id'] == '1'){
													$shift_outtime_minus_one = Date('H:i:s', strtotime($shift_outtime .' -30 minutes'));
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime_minus_one);
												} else {
													$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												}
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = sprintf("%02d", $since_start->h).':'.sprintf("%02d", $since_start->i).':'.sprintf("%02d", $since_start->s);					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									$abs_stat = 0;
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									if($working_time == '00:00:00'){
										$late_time = '00:00:00';
										$early_time = '00:00:00';
									}

									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));
									//$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name_punch."` SET `act_intime` = '".$act_intime."', `date` = '".$filter_date_start."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', late_mark = '".$late_mark."', early_mark = '".$early_mark."' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
											//$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
										}
									}
								}
								if($act_in_punch_date == $act_out_punch_date) {
									if(($act_intime != '00:00:00') && ($act_outtime != '00:00:00')){
										$update = "UPDATE `".$atten_tbl_name_punch."` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										$this->log->write($update);
									} elseif(($act_intime != '00:00:00') && ($act_outtime == '00:00:00')) {
										$update = "UPDATE `".$atten_tbl_name_punch."` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										$this->log->write($update);
									} elseif(($act_intime == '00:00:00') && ($act_outtime != '00:00:00')) {
										$update = "UPDATE `".$atten_tbl_name_punch."` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
										$this->db->query($update);
										$this->log->write($update);
									}
								} else {
									$update = "UPDATE `".$atten_tbl_name_punch."` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
									$this->db->query($update);
									$this->log->write($update);
									
									$update = "UPDATE `".$atten_tbl_name_punch."` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
									$this->db->query($update);
									$this->log->write($update);
								}
							}
							$this->log->write('---end----');
						}

						foreach($days as $dkey => $dvalue){
							if(!in_array($dvalue['date'], $current_processed_data_date)){
								$filter_date_start = $dvalue['date'];
								
								$day_date = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$trans_tbl_name = 'oc_transaction_'.$year.'_'.$month;


								$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
								$shift_schedule = $this->db->query($update3)->row;
								$schedule_raw = explode('_', $shift_schedule[$day_date]);
								if(!isset($schedule_raw[2])){
									$schedule_raw[2]= 1;
								}
								// echo $update3;
								// echo '<br />';
								// echo '<pre>';
								// print_r($schedule_raw);
								// echo '<br />';
								//exit;
								if($schedule_raw[0] == 'S'){
									$shift_data = $this->getshiftdata($schedule_raw[1]);
									if(isset($shift_data['shift_id'])){
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
										$day = date('j', strtotime($filter_date_start));
										$month = date('n', strtotime($filter_date_start));
										$year = date('Y', strtotime($filter_date_start));
										$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `".$trans_tbl_name."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										$trans_exist = $this->db->query($trans_exist_sql);
										if($trans_exist->num_rows > 0){
											$transaction_id = $trans_exist->row['transaction_id'];
											$leave_status = $trans_exist->row['leave_status'];
											$manual_status = $trans_exist->row['manual_status'];
											if($leave_status == 0 && $manual_status == 0){
												$sql = "UPDATE `".$trans_tbl_name."` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$this->db->escape($rvalue['name'])."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' WHERE `transaction_id` = '".$transaction_id."' ";
												$this->db->query($sql);
												$this->log->write($sql);
											}
										}
									}
								} elseif ($schedule_raw[0] == 'W') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `".$trans_tbl_name."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name."` SET `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `absent_status` = '0' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'H') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `".$trans_tbl_name."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name."` SET `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `absent_status` = '0' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
										}
									}
								} elseif ($schedule_raw[0] == 'HD') {
									$shift_data = $this->getshiftdata($schedule_raw[2]);
									if(!isset($shift_data['shift_id'])){
										$shift_data = $this->getshiftdata('1');
									}
									$shift_intime = $shift_data['in_time'];
									if($shift_data['shift_id'] == '1'){
										if($rvalue['sat_status'] == '1'){
											$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
											$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
										} else {
											$shift_outtime = $shift_data['out_time'];
										}
									} else {
										$shift_intime = $shift_data['in_time'];
										$shift_outtime = $shift_data['out_time'];
									}
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$trans_exist_sql = "SELECT `transaction_id`, `leave_status`, `manual_status` FROM `".$trans_tbl_name."` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows > 0){
										$transaction_id = $trans_exist->row['transaction_id'];
										$leave_status = $trans_exist->row['leave_status'];
										$manual_status = $trans_exist->row['manual_status'];
										if($leave_status == 0 && $manual_status == 0){
											$sql = "UPDATE `".$trans_tbl_name."` SET `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `transaction_id` = '".$transaction_id."' ";
											$this->db->query($sql);
											$this->log->write($sql);
										}
									}
								}
							}
						}
					}
					// echo '<pre>';
					// print_r($current_processed_data);
					// exit;
					
					if($current_processed_data){
						$final_datas = array();
						$done_transaction_id = array();
						foreach($current_processed_data as $ckey => $cvalue){
							$valu = explode( ',', $cvalue);
							$pu_date = date('Y-m-d', strtotime($valu[1]));

							$pu_month = date('n', strtotime($pu_date));
							$pu_year = date('Y', strtotime($pu_date));

							$trantbl_name = 'oc_transaction_'.$pu_year.'_'.$pu_month;
							$attentbl_name = 'oc_attendance_'.$pu_year.'_'.$pu_month;

							$transaction_ids = $this->db->query("SELECT `transaction_id` FROM `".$attentbl_name."` WHERE `id` = '".$valu[0]."' ");
							if($transaction_ids->num_rows > 0){
								$transaction_id = $transaction_ids->row['transaction_id'];
								if(!isset($done_transaction_id[$transaction_id])){
									$done_transaction_id[$transaction_id] = $transaction_id;
									$transaction_datass = $this->db->query("SELECT * FROM `".$trantbl_name."` WHERE `transaction_id` = '".$transaction_id."' AND `company_id` = '1' ");
									if($transaction_datass->num_rows > 0){
										$transaction_datas = $transaction_datass->rows;
										foreach($transaction_datas as $tkey => $tvalue){
											$emp_data = $this->getEmployees_dat($tvalue['emp_id']);
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeId'] = $tvalue['emp_id'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['EmployeeName'] = $tvalue['emp_name'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['PunchID'] = $tvalue['emp_id'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['Department'] = $tvalue['department'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['Site'] = $emp_data['unit'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['Region'] = $emp_data['region'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['Division'] = $emp_data['division'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['AttendDate'] = date('Y-m-d', strtotime($tvalue['date']));
											
											$date_to_compare = $tvalue['date'];
											//if(strtotime($emp_data['doj']) > strtotime($date_to_compare)){
												//$status = 'X';
											//} else
											if($tvalue['leave_status'] != '0'){
												if($tvalue['leave_status'] == '0.5'){
													$status = 'PL';
												} else {
													$status = 'PL';
												}
											} elseif($tvalue['weekly_off'] != '0'){
												$working_times = explode(':', $tvalue['working_time']);
												$working_hours = $working_times[0];
												if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
												//if($working_hours >= '06'){
													$status = 'W';
												} else {
													$status = 'W';	
												}
											} elseif($tvalue['holiday_id'] != '0'){
												$working_times = explode(':', $tvalue['working_time']);
												$working_hours = $working_times[0];
												if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
												//if($working_hours >= '06'){
													$status = 'PPH';
												} else {
													$status = 'H';
												}
											} elseif($tvalue['present_status'] == '1'){
												$status = 'P';
											} elseif($tvalue['absent_status'] == '1'){
												$status = 'A';
											} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5'){
												$status = 'HD';
											}

											$final_datas[$tvalue['date']][$tvalue['emp_id']]['Status'] = $status;
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['InTime'] = $tvalue['act_intime'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['OutTime'] = $tvalue['act_outtime'];
											$final_datas[$tvalue['date']][$tvalue['emp_id']]['TotaHour'] = $tvalue['working_time'];
										}
									}
								}
							}
						}
						// echo '<pre>';
						// print_r($final_datas);
						// exit;
						foreach($final_datas as $fkeys => $fvalues){
							foreach($fvalues as $fkey => $fvalue){
								$sql = "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
								$params = array();
								$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
								$stmt = sqlsrv_query($conn1, $sql, $params, $options);
								if( $stmt === false) {
									// echo 'Not Executed Start';
									// echo '<br />';
									// echo "SELECT * FROM [dbo].[Transaction_JMC] WHERE EmployeeId = '".$fvalue['EmployeeId']."' AND AttendDate = '".$fvalue['AttendDate']."' ";
									// echo '<br />';
									// echo '<pre>';
									// print_r(sqlsrv_errors());
									// echo '<br />';
									// echo 'Not Executed End';
									// echo '<br />';
								}
								$is_exist = 0;
								$id = 0;
								while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
									$is_exist = 1;
									$id = $row['Id'];
								}

								$current_date = date('Y-m-d H:i:s');
								if($is_exist == 1){
									$update_sql = "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '".$current_date."', ProcessStatus = '0' WHERE Id = '".$id."' ";
									$stmt = sqlsrv_query($conn1, $update_sql);
									if($stmt == false){
										//echo 'Not Executed Start';
										//echo '<br />';
										//echo "UPDATE [dbo].[Transaction_JMC] SET Status = '".$fvalue['Status']."', InTime = '".$fvalue['InTime']."', OutTime = '".$fvalue['OutTime']."', TotaHour = '".$fvalue['TotaHour']."', JmcDate = '0000-00-00 00:00:00' WHERE Id = '".$id."' ";
										//echo '<br />';
										//echo '<pre>';
										//print_r(sqlsrv_errors());
										//echo '<br />';
										//echo 'Not Executed End';
										//echo '<br />';
									}
								} else {
									$insert_sql = "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$this->mssql_escape($fvalue['EmployeeName'])."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', '".$current_date."', '0') ";
									
									$this->log->write($insert_sql);

									$stmt = sqlsrv_query($conn1, $insert_sql);
									if($stmt == false){
										// echo 'Not Executed Start';
										// echo '<br />';
										// echo "INSERT INTO [dbo].[Transaction_JMC] (EmployeeId, EmployeeName, PunchID, Department, Site, Region, Division, AttendDate, Status, InTime, OutTime, TotaHour, JmcDate, ProcessStatus) VALUES ('".$fvalue['EmployeeId']."', '".$fvalue['EmployeeName']."', '".$fvalue['PunchID']."', '".$fvalue['Department']."', '".$fvalue['Site']."', '".$fvalue['Region']."', '".$fvalue['Division']."', '".$fvalue['AttendDate']."', '".$fvalue['Status']."', '".$fvalue['InTime']."', '".$fvalue['OutTime']."', '".$fvalue['TotaHour']."', null, '0') ";
										// echo '<br />';
										// echo '<pre>';
										// print_r(sqlsrv_errors());
										// echo '<br />';
										// echo 'Not Executed End';
										// echo '<br />';
										$this->log->write(print_r(sqlsrv_errors(), true));
									}
								}
							}	
						}
					}
				}
				if(!isset($_GET['daytime'])){
					if($filter_name_id != ''){
						$unprocessed = $this->getUnprocessedLeaveTillDate_2_name($filter_date_start_constant, $filter_date_end_constant,$filter_name_id);
					} else {
						$unprocessed = $this->getUnprocessedLeaveTillDate_2($filter_date_start_constant, $filter_date_end_constant);
					}
					foreach($unprocessed as $data) {
						$start_month =  date('n', strtotime($data['date']));
						$start_year =  date('Y', strtotime($data['date']));
						$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;
						$this->checkLeave($data['date'], $data['emp_id'], $trans_tblname);
					}
				}
			/*} else {
				$this->session->data['error'] = 'Month Closed For Dates';
				$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
			}*/
		} else {
			$this->session->data['error'] = 'Attendance process already running';
			$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
		}	

		$sql = "UPDATE `oc_status` SET `process_status` = '0', `update_date_time_reprocess` = '".date('Y-m-d H:i:s')."' ";
		$this->db->query($sql);	
		//echo 'out';exit;
		//echo 'out';exit;
		$this->session->data['success'] = 'You are done with process';
		$this->redirect($this->url->link('tool/recalculate', 'token=' . $this->session->data['token'], 'SSL'));
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function getEmployees_dat($emp_code) {
		$sql = "SELECT `division`, `region`, `unit`, `doj`, `emp_code`, `name`, `department`, `unit`, `region`, `division` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getUnprocessedLeaveTillDate_2_name($filter_date_start_constant,$filter_date_end_constant,$filter_name_id) {
		$sql = $this->db->query("SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM `oc_leave_transaction` lt WHERE lt.p_status = '0' AND (lt.a_status = '1')  AND lt.`date` >= '".$filter_date_start_constant."' AND lt.`date` <= '".$filter_date_end_constant."' AND lt.`emp_id` = '".$filter_name_id."'");
		if($sql->rows) {
			return $sql->rows;
		} else {
			return array();
		}
	}

	public function getUnprocessedLeaveTillDate_2($filter_date_start_constant,$filter_date_end_constant) {
		$sql = $this->db->query("SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM `oc_leave_transaction` lt WHERE lt.p_status = '0' AND (lt.a_status = '1')  AND lt.`date` >= '".$filter_date_start_constant."' AND lt.`date` <= '".$filter_date_end_constant."' ");
		if($sql->rows) {
			return $sql->rows;
		} else {
			return array();
		}
	}

	public function checkLeave($date, $emp_id, $trans_tblname) {
		$query = $this->db->query("SELECT * FROM oc_leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `p_status` = '0' AND `a_status` = '1' ");
		if($query->num_rows > 0) {
			if($query->row['type'] != ''){
				$trans_data = $this->db->query("SELECT * FROM `".$trans_tblname."` WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ")->row;
				if($query->row['type'] == 'F'){
					$this->db->query("UPDATE `".$trans_tblname."` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', `absent_status` = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");		
					$this->db->query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
					$this->db->query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
				} elseif($query->row['type'] == '1'){
					$this->db->query("UPDATE `".$trans_tblname."` SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE `".$trans_tblname."` SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['secondhalf_status'] == 'COF'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['secondhalf_status'] != '1' && $trans_data['secondhalf_status'] != '0'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['secondhalf_status'] == '1'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['secondhalf_status'] == '0'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} 
					$this->db->query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->db->query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
				} elseif($query->row['type'] == '2'){
					$this->db->query("UPDATE `".$trans_tblname."` SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE `".$trans_tblname."` SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['firsthalf_status'] == 'COF'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['firsthalf_status'] != '1' && $trans_data['firsthalf_status'] != '0'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['firsthalf_status'] == '1'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					} elseif($trans_data['firsthalf_status'] == '0'){
						$this->db->query("UPDATE `".$trans_tblname."` SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");
					}
					$this->db->query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->db->query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
				}
			} else {
				$this->db->query("UPDATE `".$trans_tblname."` SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");		
				$this->db->query("UPDATE `oc_leave_transaction` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");		
				$this->db->query("UPDATE `oc_leave_transaction_temp` SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");	
			}
		}	
	}

	public function getempdata($emp_code, $punch_date) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$punch_date."') ";
		//$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name`, `company`, `company_id` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getorderhistory($data){
		$new_table_name = 'oc_attendance_'.$data['logyear'].'_'.$data['log_month'];
		//$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$sql = "SELECT * FROM `".$new_table_name."` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function insert_attendance_data($data) {
		$new_table_name = 'oc_attendance_'.$data['logyear'].'_'.$data['log_month'];

		$this->db->query("INSERT INTO `".$new_table_name."` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `new_log_id` = '".$data['log_id']."' ");
	}	
	
	public function getrawattendance_in_time($emp_id, $punch_date, $atten_tblname) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM `".$atten_tblname."` WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date, $atten_tblname) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM `".$atten_tblname."` WHERE REPLACE(emp_id, ' ','') = '".$emp_id."' AND ((`punch_date` = '".$punch_date."' AND `punch_time` >= '05:00:00') OR (`punch_date` = '".$future_date."' AND `punch_time` <= '04:00:00')) ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			// echo '<pre>';
			// print_r($hour_array);

			// echo '<pre>';
			// print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 20){
					unset($hour_array[$ckey]);
				}
			}

			// echo '<pre>';
			// print_r($hour_array);
			
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}

			// echo '<pre>';
			// print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		// echo '<pre>';
		// print_r($act_outtime);
		// exit;		
		return $act_outtime;
	}

	public function mssql_escape($data) {
	    $data = stripslashes($data);
		$data = str_replace("'", "''", $data);
		return $data;
	}
}
?>