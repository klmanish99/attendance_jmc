<?php 
class ControllerToolEmpimport extends Controller { 
	private $error = array();
	public function index() {		
		$this->language->load('tool/empimport');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->load->model('tool/empimport');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->user->hasPermission('modify', 'tool/empimport')) {
			if (is_uploaded_file($this->request->files['import_data']['tmp_name'])) {
				$content = file_get_contents($this->request->files['import_data']['tmp_name']);
			} else {
				$content = false;
			}
			if ($content) {
				$this->import_data($content);
				$this->session->data['success'] = 'Employee Imported Successfully';
				$this->redirect($this->url->link('tool/empimport', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->error['warning'] = $this->language->get('error_empty');
			}
		}

		$this->data['error_list'] = '0'; 
		if(isset($this->session->data['final_employee_error_data']) && !empty($this->session->data['final_employee_error_data'])){
			//echo '<pre>';
			//print_r($this->session->data['final_employee_error_data']);
			//exit;
			$this->data['error_list'] = '1'; 
			$this->data['employees'] = $this->session->data['final_employee_error_data'];
			$employee_total = 0;
			foreach($this->data['employees'] as $keys => $values){
				foreach($values as $key => $value){
					$employee_total ++;
				}
			}
			unset($this->session->data['final_employee_error_data']);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_backup'] = $this->language->get('entry_backup');

		$this->data['button_backup'] = $this->language->get('button_backup');
		$this->data['button_restore'] = $this->language->get('button_restore');
		$this->data['button_revert'] = $this->language->get('button_revert');

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['restore'] = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'tool/empimport.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	public function import_data($content) {
		$valid = 0;
		// echo '<pre>';
		// print_r($this->request->files);
		// exit;
		if(isset($this->request->files['import_data']['name'])){
			$file_name = $this->request->files['import_data']['name'];
			$file_name_exp = explode('.', $file_name);
			if($file_name_exp[1] == 'csv'){
				$valid = 1;
			}
		}
		//echo $valid;exit;
		if(isset($this->request->files['import_data']['tmp_name']) && $this->request->files['import_data']['tmp_name'] != '' && $valid == 1){
			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$employement_data = array();
			$grade_data = array();
			$designtion_data = array();
			$department_data = array();
			$site_data = array();
			$division_data = array();
			$region_data = array();
			$state_data = array();
			$company_data = array();
			// $this->db->query("TRUNCATE TABLE `oc_employee`");
			// $this->db->query("TRUNCATE TABLE `oc_employement`");
			// $this->db->query("TRUNCATE TABLE `oc_grade`");
			// $this->db->query("TRUNCATE TABLE `oc_designation`");
			// $this->db->query("TRUNCATE TABLE `oc_department`");
			// $this->db->query("TRUNCATE TABLE `oc_unit`");
			// $this->db->query("TRUNCATE TABLE `oc_division`");
			// $this->db->query("TRUNCATE TABLE `oc_region`");
			// $this->db->query("TRUNCATE TABLE `oc_state`");
			// $this->db->query("TRUNCATE TABLE `oc_company`");
			// $this->db->query("TRUNCATE TABLE `oc_shift_schedule`");
			// $this->db->query("TRUNCATE TABLE `oc_shift_unit`");
			// $this->db->query("TRUNCATE TABLE `oc_leave`");
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					// echo '<pre>';
					// print_r($var);
					// exit;
					if($var[6] != ''){
						if(!isset($employement_data[$var[6]])){
							$employement_data[$var[6]] = html_entity_decode(strtolower(trim($var[6])));
						}
					}
					if($var[7] != ''){
						if(!isset($grade_data[$var[7]])){
							$grade_data[$var[7]] = html_entity_decode(strtolower(trim($var[7])));
						}
					}
					if($var[9] != ''){
						if(!isset($designtion_data[$var[9]])){
							$designtion_data[$var[9]] = html_entity_decode(strtolower(trim($var[9])));
						}
					}
					if($var[10] != ''){
						if(!isset($department_data[$var[10]])){
							$department_data[$var[10]] = html_entity_decode(strtolower(trim($var[10])));
						}
					}
					if($var[11] != ''){
						if(!isset($site_data[$var[11]])){
							$site_data[$var[11]] = html_entity_decode(strtolower(trim($var[11])));
						}
					}
					if($var[12] != ''){
						if(!isset($division_data[$var[12]])){
							$division_data[$var[12]] = html_entity_decode(strtolower(trim($var[12])));
						}
					}
					if($var[13] != ''){
						if(!isset($region_data[$var[13]])){
							$region_data[$var[13]] = html_entity_decode(strtolower(trim($var[13])));
						}
					}
					if($var[14] != ''){
						if(!isset($state_data[$var[14]])){
							$state_data[$var[14]] = html_entity_decode(strtolower(trim($var[14])));
						}
					}
					if($var[17] != ''){
						if(!isset($company_data[$var[17]])){
							$company_data[$var[17]] = html_entity_decode(strtolower(trim($var[17])));
						}
					}
				}
				$i ++;
			}
			fclose($file);
			
			$employement_data_linked = array();
			foreach($employement_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				if($dkey == 'P' || $dkey == 'PERMANENT'){
					$dkey = 'PERMANENT';
				} elseif($dkey == 'C' || $dkey == 'CONTRACT'){
					$dkey = 'CONTRACT';
				} elseif($dkey == 'T' || $dkey == 'TRAINEE'){
					$dkey = 'TRAINEE';
				} elseif($dkey == 'L' || $dkey == 'LOCAL'){
					$dkey = 'LOCAL';
				} else {
					$dkey = '';
				}
				$is_exist = $this->db->query("SELECT `employement_id` FROM `oc_employement` WHERE `employement` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_employement` SET `employement` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$employement_id = $this->db->getLastId();
					$employement_id = 0;
				} else {
					$employement_id = $is_exist->row['employement_id'];
				}	
				$employement_data_linked[$dkey] = $employement_id;
			}

			$grade_data_linked = array();
			foreach($grade_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `grade_id` FROM `oc_grade` WHERE `g_code` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_grade` SET `g_name` = '".$this->db->escape($dkey)."', `g_code` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$grade_id = $this->db->getLastId();
					$grade_id = 0;
				} else {
					$grade_id = $is_exist->row['grade_id'];
				}
				$grade_data_linked[$dvalue] = $grade_id;
			}

			$designation_data_linked = array();
			foreach($designtion_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_designation` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					//$this->db->query($sql);
					//$designation_id = $this->db->getLastId();
					$designation_id = 0;
				} else {
					$designation_id = $is_exist->row['designation_id'];
				}
				$designation_data_linked[$dvalue] = $designation_id;
			}

			$department_data_linked = array();
			foreach($department_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_department` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
					//$this->db->query($sql);
					//$department_id = $this->db->getLastId();
					$department_id = 0;
				} else {
					$department_id = $is_exist->row['department_id'];
				}
				$department_data_linked[$dvalue] = $department_id;
			}

			$site_data_linked = array();
			foreach($site_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_unit` SET `unit` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$site_id = $this->db->getLastId();
					$site_id = 0;
				} else {
					$site_id = $is_exist->row['unit_id'];
				}
				$site_data_linked[$dvalue] = $site_id;
			}

			$division_data_linked = array();
			foreach($division_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `division_id` FROM `oc_division` WHERE `division` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_division` SET `division` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$division_id = $this->db->getLastId();
					$division_id = 0;
				} else {
					$division_id = $is_exist->row['division_id'];
				}
				$division_data_linked[$dvalue] = $division_id;
			}

			$region_data_linked = array();
			foreach($region_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `region_id` FROM `oc_region` WHERE `region` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_region` SET `region` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$region_id = $this->db->getLastId();
					$region_id = 0;
				} else {
					$region_id = $is_exist->row['region_id'];
				}
				$region_data_linked[$dvalue] = $region_id;
			}

			$state_data_linked = array();
			foreach($state_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `state_id` FROM `oc_state` WHERE `state` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_state` SET `state` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$state_id = $this->db->getLastId();
					$state_id = 0;
				} else {
					$state_id = $is_exist->row['state_id'];
				}
				$state_data_linked[$dvalue] = $state_id;
			}

			$company_data_linked = array();
			foreach($company_data as $dkey => $dvalue){
				$dkey = trim(preg_replace('/\s+/',' ',$dkey));
				$is_exist = $this->db->query("SELECT `company_id` FROM `oc_company` WHERE `company` = '".$this->db->escape($dkey)."' ");
				if($is_exist->num_rows == 0){
					$sql = "INSERT INTO `oc_company` SET `company` = '".$this->db->escape($dkey)."' ";
					//$this->db->query($sql);
					//$company_id = $this->db->getLastId();
					$company_id = 0;
				} else {
					$company_id = $is_exist->row['company_id'];
				}
				$company_data_linked[$dvalue] = $company_id;
			}

			// echo '<pre>';
			// print_r($company_data_linked);
			// exit;
			//echo 'out';exit;

			$file=fopen($this->request->files['import_data']['tmp_name'],"r");
			$i=1;
			$months_array = array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
				'6' => '6',
				'7' => '7',
				'8' => '8',
				'9' => '9',
				'10' => '10',
				'11' => '11',
				'12' => '12',
			);
			$shortmonthsarray = array(
				'Jan' => '1',
				'Feb' => '2',
				'Mar' => '3',
				'Apr' => '4',
				'May' => '5',
				'Jun' => '6',
				'Jul' => '7',
				'Aug' => '8',
				'Sep' => '9',
				'Oct' => '10',
				'Nov' => '11',
				'Dec' => '12',	
			);
			$final_employee_error_data = array();
			$employee_error_data = array();
			while(($var=fgetcsv($file,1000,","))!==FALSE){
				if($i != 1) {
					$employee_error_data = array();
					$var0=addslashes($var[0]);//emp_code
					if($var0 != '' && !isset($emp_code_exist[$var0])){
						$var1 = html_entity_decode(trim($var[1]));//name
						if($var1 == ''){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Name Blank',
							);
						}
						$var22=addslashes($var[2]);//gender
						if($var22 == 'M'){
							$var2 = 'M';
						} elseif($var22 == 'F'){
							$var2 = 'F';
						} else {
							$var2 = '';
						}
						if($var2 == ''){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Gender Value is Blank / Invalid',
							);
						}

						$var33=addslashes($var[3]);//dob
						if($var33 != ''){
							$var33_exp = explode('-', $var33); // yyyy/mm/dd
							if(isset($var33_exp[2])){
								$year_string = strlen($var33_exp[0]);
								$month_string = strlen(sprintf('%02d', $var33_exp[1]));
								$day_string = strlen(sprintf('%02d', $var33_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									//echo 'in';exit;
									//if($month_string <= '12' && $day_string <= '31'){
										$var3 = Date('Y-m-d', strtotime($var33));//dob
									//} else {
										//$var3 = '0000-00-00';	
									//}
								} else {
									$var33_exp = explode('-', $var33);// dd/mm/yyyy
									if(isset($var33_exp[2])){
										$year_string = strlen($var33_exp[2]);
										$month_string = strlen(sprintf('%02d', $var33_exp[1]));
										$day_string = strlen(sprintf('%02d', $var33_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											//if($month_string <= '12' && $day_string <= '31'){
												$var3 = Date('Y-m-d', strtotime($var33));//dob
											//} else {
												//$var3 = '0000-00-00';	
											//}
										} else {
											$var3 = '0000-00-00';
										}
									} else {
										$var3 = '0000-00-00';		
									}
								}
							} else {
								$var3 = '0000-00-00';	
							}
						} else {
							$var3 = '0000-00-00';
						}
						// echo $var3;
						// echo '<br />';
						// echo 'out';exit;

						$var44=addslashes($var[4]);//doj
						if($var44 != '' && $var44 != '0000-00-00' && $var44 != '00-00-0000'){
							$var44_exp = explode('-', $var44);// yyyy/mm/dd
							if(isset($var44_exp[2])){
								$year_string = strlen($var44_exp[0]);
								$month_string = strlen(sprintf('%02d', $var44_exp[1]));
								$day_string = strlen(sprintf('%02d', $var44_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									//if($month_string <= '12' && $day_string <= '31'){
										$var4 = Date('Y-m-d', strtotime($var44));//doj
										//if(strtotime($var4) > strtotime(date('Y-m-d'))){
											//$var4 = '0000-00-00';	
										//}
									//} else {
										//$var4 = '0000-00-00';	
									//}
								} else {
									$var44_exp = explode('-', $var44);// dd/mm/yyyy
									if(isset($var44_exp[2])){
										$year_string = strlen($var44_exp[2]);
										$month_string = strlen(sprintf('%02d', $var44_exp[1]));
										$day_string = strlen(sprintf('%02d', $var44_exp[0]));
										// echo '<pre>';
										// print_r($year_string);
										// echo '<pre>';
										// print_r($month_string);
										// echo '<pre>';
										// print_r($day_string);
										// exit;
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											//if($month_string <= '12' && $day_string <= '31'){
												$var4 = Date('Y-m-d', strtotime($var44));//doj
												//if(strtotime($var4) > strtotime(date('Y-m-d'))){
													//$var4 = '0000-00-00';	
												//}
											//} else {
												//$var4 = '0000-00-00';	
											//}
										} else {
											$var4 = '0000-00-00';		
										}
									} else {
										$var4 = '0000-00-00';
									}
								}
							} else {
								$var4 = '0000-00-00';	
							}
						} else {
							$var4 = '0000-00-00';
						}
						//echo $var4;exit;

						$var55=addslashes($var[5]);//doc
						if($var55 != ''){
							$var5 = Date('Y-m-d', strtotime($var55));//doc
						} else {
							$var5 = '0000-00-00';
						}

						$var88=addslashes($var[8]);//dol
						if($var88 != ''){
							$var8 = Date('Y-m-d', strtotime($var88));//dol
						} else {
							$var8 = '0000-00-00';
						}

						/*
						$var33=addslashes($var[3]);//dob
						if($var33 != ''){
							$var3_exp = explode('-', $var33);
							$day = $var3_exp[0];
							$month = $shortmonthsarray[$var3_exp[1]];
							if($var3_exp[2] > 0 && $var3_exp[2] <= 17){
								$year = '20'.$var3_exp[2];
							} else {
								$year = '19'.$var3_exp[2];	
							}
							$var3 = $year.'-'.$month.'-'.$day;
							$var3 = Date('Y-m-d', strtotime($var3));//dob
						} else {
							$var3 = '0000-00-00';
						}
						
						$var44=addslashes($var[4]);//doj
						if($var44 != ''){
							$var4_exp = explode('-', $var44);
							$day = $var4_exp[0];
							$month = $shortmonthsarray[$var4_exp[1]];
							if($var4_exp[2] > 0 && $var4_exp[2] <= 17){
								$year = '20'.$var4_exp[2];
							} else {
								$year = '19'.$var4_exp[2];	
							}
							$var4 = $year.'-'.$month.'-'.$day;
							$var4 = Date('Y-m-d', strtotime($var4));//doj
						} else {
							$var4 = '0000-00-00';
						}

						$var55=addslashes($var[5]);//doc
						if($var55 != ''){
							$var5_exp = explode('-', $var55);
							$day = $var5_exp[0];
							$month = $shortmonthsarray[$var5_exp[1]];
							if($var5_exp[2] > 0 && $var5_exp[2] <= 17){
								$year = '20'.$var5_exp[2];
							} else {
								$year = '19'.$var5_exp[2];	
							}
							$var5 = $year.'-'.$month.'-'.$day;
							$var5 = Date('Y-m-d', strtotime($var5));//doc
						} else {
							$var5 = '0000-00-00';
						}
						
						$var88=addslashes($var[8]);//dol
						if($var88 != ''){
							$var8_exp = explode('-', $var88);
							$day = $var8_exp[0];
							$month = $shortmonthsarray[$var8_exp[1]];
							if($var8_exp[2] > 0 && $var8_exp[2] <= 17){
								$year = '20'.$var8_exp[2];
							} else {
								$year = '19'.$var8_exp[2];	
							}
							$var8 = $year.'-'.$month.'-'.$day;
							$var8 = Date('Y-m-d', strtotime($var8));//dol
						} else {
							$var8 = '0000-00-00';
						}
						*/
						if($var3 == '0000-00-00'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Date Of Birth Blank / Invalid'
							);
						}

						if($var4 == '0000-00-00'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Date Of Joining Blank / Invalid'
							);
						}
						
						$var61=html_entity_decode(addslashes($var[6]));//employement
						if($var61 == 'P' || $var61 == 'PERMANENT'){
							$var61 = 'PERMANENT';
						} elseif($var61 == 'C' || $var61 == 'CONTRACT'){
							$var61 = 'CONTRACT';
						} elseif($var61 == 'T' || $var61 == 'TRAINEE'){
							$var61 = 'TRAINEE';
						} elseif($var61 == 'L' || $var61 == 'LOCAL'){
							$var61 = 'LOCAL';
						} else {
							$var61 = $var[6];
						}

						$var6=html_entity_decode(strtolower(trim($var[6])));//employement
						if($var6 != ''){
							if($var6 == 'p' || $var6 == 'permanent'){
								$var6 = 'PERMANENT';
							} elseif($var6 == 'c' || $var6 == 'contract'){
								$var6 = 'CONTRACT';
							} elseif($var6 == 't' || $var6 == 'trainee'){
								$var6 = 'TRAINEE';
							} elseif($var6 == 'L' || $var6 == 'local'){
								$var6 = 'LOCAL';
							} else {
								$var6 = '';
							}
							$var6=$employement_data_linked[$var6];
						}
						if($var6 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employement Type '.$var61.' Does Not Exist',
							);
						}

						$var171=html_entity_decode(addslashes($var[17]));//company
						$var17=html_entity_decode(strtolower(trim($var[17])));//company
						if($var17 != ''){
							$var17=$company_data_linked[$var17];
						}
						if($var17 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Company '.$var171.' Does Not Exist',
							);
						}

						if($var17 == '1'){
							$var71=html_entity_decode(addslashes($var[7]));//grade
							$var7=html_entity_decode(strtolower(trim($var[7])));//grade
							if($var7 != ''){
								$var7=$grade_data_linked[$var7];
							}
							if($var7 == '0'){
								$employee_error_data[] = array(
									'line_number' => $i,
									'reason' => 'Grade '.$var71.' Does Not Exist',
								);
							}
						} else {
							$var71 = 'Z25';
							$var7 = '36';
						}
						
						$var91=html_entity_decode(addslashes($var[9]));//designation
						$var9=html_entity_decode(strtolower(trim($var[9])));//designation
						if($var9 != ''){
							$var9=$designation_data_linked[$var9];
						}
						if($var9 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Designation '.$var91.' Does Not Exist',
							);
						}
						$var101=html_entity_decode(addslashes($var[10]));//department
						$var10=html_entity_decode(strtolower(trim($var[10])));//department
						if($var10 != ''){
							$var10=$department_data_linked[$var10];
						}
						if($var10 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Department '.$var101.' Does Not Exist',
							);
						}
						$var111=html_entity_decode(addslashes($var[11]));//site
						$var11=html_entity_decode(strtolower(trim($var[11])));//site
						if($var11 != ''){
							$var11=$site_data_linked[$var11];
						}
						if($var11 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Site '.$var111.' Does Not Exist',
							);
						}
						$var121=html_entity_decode(addslashes($var[12]));//division
						$var12=html_entity_decode(strtolower(trim($var[12])));//division
						if($var12 != ''){
							$var12=$division_data_linked[$var12];
						}
						if($var12 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Division '.$var121.' Does Not Exist',
							);
						}
						$var131=html_entity_decode(addslashes($var[13]));//region
						$var13=html_entity_decode(strtolower(trim($var[13])));//region
						if($var13 != ''){
							$var13=$region_data_linked[$var13];
						}
						if($var13 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Region '.$var131.' Does Not Exist',
							);
						}
						$var141=html_entity_decode(addslashes($var[14]));//state
						$var14=html_entity_decode(strtolower(trim($var[14])));//state
						if($var14 != ''){
							$var14=$state_data_linked[$var14];
						}
						if($var14 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'State '.$var141.' Does Not Exist',
							);
						}
						$var16=addslashes($var[16]);//status
						if($var16 == 'Active'){
							$var16 = '1';
						} else {
							$var16 = '0';
						}
						
						$emp_code = $var0;
						$user_name = $emp_code;
						$salt = substr(md5(uniqid(rand(), true)), 0, 9);
						$password = sha1($salt . sha1($salt . sha1($emp_code)));
						
						$var151=addslashes($var[15]);//shift_id
						$var15 = '0';
						if($var151 == '09:30:00 - 18:30:00'){
							$var15 = '1';
						} elseif($var151 == '08:00:00 - 20:00:00') {
							$var15 = '2';
						} elseif($var151 == '07:00:00 - 18:00:00') {
							$var15 = '4';
						}

						if($var15 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Shift '.$var151.' Does Not Exist',
							);	
						}

						$var18=addslashes($var[18]);//weekly_off
						if($var18 == '1'){
							$sun_status = '0';
							$sat_status = '1';
						} else {
							$sun_status = '0';
							$sat_status = '0';
						}

						// echo '<pre>';
						// print_r($employee_error_data);
						// exit;

						if(empty($employee_error_data)){
							$is_exist = $this->db->query("SELECT `emp_code`, `shift_id`, `unit_id`, `sat_status`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$var0."' ");
							if($is_exist->num_rows == 0){
								$db_shift_id = 0;
								$db_unit_id = 0;
								$db_sat_status = 0;
								$db_doj = '0000-00-00';
								$insert = "INSERT INTO `oc_employee` SET 
											`emp_code` = '".$var0."', 
											`name` = '".$this->db->escape($var1)."', 
											`gender` = '".$var2."', 
											`dob` = '".$var3."', 
											`doj` = '".$var4."', 
											`doc` = '".$var5."', 
											`employement` = '".$var61."',
											`employement_id` = '".$var6."',
											`grade` = '".$var71."',
											`grade_id` = '".$var7."',
											`dol` = '".$var8."', 
											`designation` = '".$var91."', 
											`designation_id` = '".$var9."', 
											`department` = '".$var101."',
											`department_id` = '".$var10."',
											`unit` = '".$var111."',
											`unit_id` = '".$var11."',
											`division` = '".$var121."',
											`division_id` = '".$var12."',
											`region` = '".$var131."',
											`region_id` = '".$var13."',
											`state` = '".$var141."',
											`state_id` = '".$var14."',
											`status` = '".$var16."', 
											`company` = '".$var171."',
											`company_id` = '".$var17."',
											`shift_type` = 'F',
											`user_group_id` = '11',
											`username` = '".$user_name."', 
											`password` = '".$password."', 
											`shift_id` = '".$var15."', 
											`sat_status` = '".$sat_status."', 
											`sun_status` = '".$sun_status."', 
											`is_new` = '1',
											`salt` = '".$salt."' "; 
								
								$this->db->query($insert);
								// echo $insert;
								// echo '<br />';
								// exit;
							} else {
								$db_emp_data = $is_exist->row;
								$db_shift_id = $db_emp_data['shift_id'];
								$db_unit_id = $db_emp_data['unit_id'];
								$db_sat_status = $db_emp_data['sat_status'];
								$db_doj = $db_emp_data['doj'];
								$update = "UPDATE `oc_employee` SET 
											`emp_code` = '".$var0."', 
											`name` = '".$this->db->escape($var1)."', 
											`gender` = '".$var2."', 
											`dob` = '".$var3."', 
											`doj` = '".$var4."', 
											`doc` = '".$var5."', 
											`employement` = '".$var61."',
											`employement_id` = '".$var6."',
											`grade` = '".$var71."',
											`grade_id` = '".$var7."',
											`dol` = '".$var8."', 
											`designation` = '".$var91."', 
											`designation_id` = '".$var9."', 
											`department` = '".$var101."',
											`department_id` = '".$var10."',
											`unit` = '".$var111."',
											`unit_id` = '".$var11."',
											`division` = '".$var121."',
											`division_id` = '".$var12."',
											`region` = '".$var131."',
											`region_id` = '".$var13."',
											`state` = '".$var141."',
											`state_id` = '".$var14."',
											`status` = '".$var16."', 
											`shift_id` = '".$var15."', 
											`sat_status` = '".$sat_status."', 
											`sun_status` = '".$sun_status."', 
											`company` = '".$var171."',
											`company_id` = '".$var17."'
											 WHERE `emp_code` = '".$var0."' "; 
								$this->db->query($update);
								// echo $insert;
								// echo '<br />';
								// exit;
							}
							$shift_id_data = 'S_'.$var15;
							foreach ($months_array as $key => $value) {
								$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$var0."' AND `month` = '".$key."' AND `year` = '2017' ");
								if($is_exist->num_rows == 0){
									$insert1 = "INSERT INTO `oc_shift_schedule` SET 
												`emp_code` = '".$var0."',
												`1` = '".$shift_id_data."',
												`2` = '".$shift_id_data."',
												`3` = '".$shift_id_data."',
												`4` = '".$shift_id_data."',
												`5` = '".$shift_id_data."',
												`6` = '".$shift_id_data."',
												`7` = '".$shift_id_data."',
												`8` = '".$shift_id_data."',
												`9` = '".$shift_id_data."',
												`10` = '".$shift_id_data."',
												`11` = '".$shift_id_data."',
												`12` = '".$shift_id_data."', 
												`13` = '".$shift_id_data."', 
												`14` = '".$shift_id_data."', 
												`15` = '".$shift_id_data."', 
												`16` = '".$shift_id_data."', 
												`17` = '".$shift_id_data."', 
												`18` = '".$shift_id_data."', 
												`19` = '".$shift_id_data."', 
												`20` = '".$shift_id_data."', 
												`21` = '".$shift_id_data."', 
												`22` = '".$shift_id_data."', 
												`23` = '".$shift_id_data."', 
												`24` = '".$shift_id_data."', 
												`25` = '".$shift_id_data."', 
												`26` = '".$shift_id_data."', 
												`27` = '".$shift_id_data."', 
												`28` = '".$shift_id_data."', 
												`29` = '".$shift_id_data."', 
												`30` = '".$shift_id_data."', 
												`31` = '".$shift_id_data."',
												`month` = '".$key."',
												`year` = '2017',
												`status` = '1' ,
												`unit` = '".$var111."',
												`unit_id` = '".$var11."' ";
									$this->db->query($insert1);
									
									$year = '2017';
									if($key == 12){
										$mod_key = 1;
										$mod_year = $year + 1;
									} else {
										$mod_key = $key + 1;
										$mod_year = $year;
									}
									//echo $mod_key;exit;
									$start_date_string = '2017-'.$key.'-01';
									$start = new DateTime('2017-'.$key.'-01');
									//if(strtotime($start_date_string) > strtotime($var4)){
										$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
										$interval = DateInterval::createFromDateString('1 day');
										$period = new DatePeriod($start, $interval, $end);
										$week_array = array();
										foreach ($period as $dt)
										{
											$current_date = $dt->format('Y-m-d');
											if(strtotime($current_date) > strtotime($var4)){
											    if ($dt->format('N') == 3)
											    {
											    	$sunday = $dt->format('Y-m-d');
											    	$month_name = date('M', strtotime($sunday));
										    		$year = date('Y', strtotime($sunday));
											    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
											    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
											    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
											    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
											    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
											    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
											    }
											    if ($dt->format('N') == 7 || $dt->format('N') == 6)
											    {
											    	$sunday = $dt->format('Y-m-d');
											    	$month_name = date('M', strtotime($sunday));
										    		$year = date('Y', strtotime($sunday));
											    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
											    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
											    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
											    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
											    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
											    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
										    		$current_day = date('j', strtotime($sunday));
										    		if($first_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
										    		}

										    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
										    		if($second_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
										    		}

										    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
										    		if($third_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
										    		}

										    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
										    		if($fourth_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
										    		}

										    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
										    		if($fifth_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
										    		}
											        //$no++;
											    }
											}
										}
									    foreach ($week_array as $wkey => $wvalue) {
											$week_string = 'W_1_'.$var15;
											$halfday_string = 'HD_1_'.$var15;
											$shift_string = 'S_'.$var15;
											if($var15 == '1'){
												if($sat_status == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($sat_status == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		//$this->db->query($sql2);
												}
												if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}

												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}
											/*
											if($var111 == 'VITA' && $var15 == '2'){
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											} else {
											*/
											if($var15 == '2' || $var15 = '4'){
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}
											//}
										}
									//}
								}

								$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$var0."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
								if($is_exist->num_rows == 0){
									$insert1 = "INSERT INTO `oc_shift_schedule` SET 
												`emp_code` = '".$var0."',
												`1` = '".$shift_id_data."',
												`2` = '".$shift_id_data."',
												`3` = '".$shift_id_data."',
												`4` = '".$shift_id_data."',
												`5` = '".$shift_id_data."',
												`6` = '".$shift_id_data."',
												`7` = '".$shift_id_data."',
												`8` = '".$shift_id_data."',
												`9` = '".$shift_id_data."',
												`10` = '".$shift_id_data."',
												`11` = '".$shift_id_data."',
												`12` = '".$shift_id_data."', 
												`13` = '".$shift_id_data."', 
												`14` = '".$shift_id_data."', 
												`15` = '".$shift_id_data."', 
												`16` = '".$shift_id_data."', 
												`17` = '".$shift_id_data."', 
												`18` = '".$shift_id_data."', 
												`19` = '".$shift_id_data."', 
												`20` = '".$shift_id_data."', 
												`21` = '".$shift_id_data."', 
												`22` = '".$shift_id_data."', 
												`23` = '".$shift_id_data."', 
												`24` = '".$shift_id_data."', 
												`25` = '".$shift_id_data."', 
												`26` = '".$shift_id_data."', 
												`27` = '".$shift_id_data."', 
												`28` = '".$shift_id_data."', 
												`29` = '".$shift_id_data."', 
												`30` = '".$shift_id_data."', 
												`31` = '".$shift_id_data."',
												`month` = '".$key."',
												`year` = '".date('Y')."',
												`status` = '1' ,
												`unit` = '".$var111."',
												`unit_id` = '".$var11."' ";
									$this->db->query($insert1);
									
									$year = date('Y');
									if($key == 12){
										$mod_key = 1;
										$mod_year = $year + 1;
									} else {
										$mod_key = $key + 1;
										$mod_year = $year;
									}
									//echo $mod_key;exit;
									$start_date_string = date('Y').'-'.$key.'-01';
									$start = new DateTime(date('Y').'-'.$key.'-01');
									//if(strtotime($start_date_string) > strtotime($var4)){
										$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
										$interval = DateInterval::createFromDateString('1 day');
										$period = new DatePeriod($start, $interval, $end);
										$week_array = array();
										foreach ($period as $dt)
										{
											$current_date = $dt->format('Y-m-d');
											if(strtotime($current_date) > strtotime($var4)){
											    if ($dt->format('N') == 3)
											    {
											    	$sunday = $dt->format('Y-m-d');
											    	$month_name = date('M', strtotime($sunday));
										    		$year = date('Y', strtotime($sunday));
											    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
											    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
											    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
											    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
											    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
											    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
											    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
											    }
											    if ($dt->format('N') == 7 || $dt->format('N') == 6)
											    {
											    	$sunday = $dt->format('Y-m-d');
											    	$month_name = date('M', strtotime($sunday));
										    		$year = date('Y', strtotime($sunday));
											    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
											    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
											    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
											    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
											    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
											    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
										    		$current_day = date('j', strtotime($sunday));
										    		if($first_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
										    		}

										    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
										    		if($second_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
										    		}

										    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
										    		if($third_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
										    		}

										    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
										    		if($fourth_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
										    		}

										    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
										    		if($fifth_saturday == $current_day){
										    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
										    		} else {
										    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
										    		}
											        //$no++;
											    }
											}
										}
									    foreach ($week_array as $wkey => $wvalue) {
											$week_string = 'W_1_'.$var15;
											$halfday_string = 'HD_1_'.$var15;
											$shift_string = 'S_'.$var15;
											if($var15 == '1'){
												if($sat_status == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($sat_status == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		//$this->db->query($sql2);
												}
												if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}

												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}
											/*
											if($var111 == 'VITA' && $var15 == '2'){
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											} else {
											*/
											if($var15 == '2' || $var15 = '4'){
												if($wvalue['day_name'] == 'Sun'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
												if($wvalue['day_name'] == 'Wed'){
													$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
									        		//echo $sql2;
									        		//echo '<br />';
									        		$this->db->query($sql2);
												}
											}
											//}
										}
									//}
								} else {
									$shift_schedule_data = $is_exist->row;
									$shift = $shift_id_data;
									// echo '<pre>';
									// print_r($shift_schedule_data);
									// exit;
									if($db_unit_id == '0' || $db_shift_id == '0' || ($db_unit_id != $var11) || ($db_shift_id != $var15) || ($db_sat_status != $sat_status) || (strtotime($db_doj) != strtotime($var4)) ){
										$insert1 = "UPDATE `oc_shift_schedule` SET 
													`emp_code` = '".$var0."',
													`1` = '".$shift_id_data."',
													`2` = '".$shift_id_data."',
													`3` = '".$shift_id_data."',
													`4` = '".$shift_id_data."',
													`5` = '".$shift_id_data."',
													`6` = '".$shift_id_data."',
													`7` = '".$shift_id_data."',
													`8` = '".$shift_id_data."',
													`9` = '".$shift_id_data."',
													`10` = '".$shift_id_data."',
													`11` = '".$shift_id_data."',
													`12` = '".$shift_id_data."', 
													`13` = '".$shift_id_data."', 
													`14` = '".$shift_id_data."', 
													`15` = '".$shift_id_data."', 
													`16` = '".$shift_id_data."', 
													`17` = '".$shift_id_data."', 
													`18` = '".$shift_id_data."', 
													`19` = '".$shift_id_data."', 
													`20` = '".$shift_id_data."', 
													`21` = '".$shift_id_data."', 
													`22` = '".$shift_id_data."', 
													`23` = '".$shift_id_data."', 
													`24` = '".$shift_id_data."', 
													`25` = '".$shift_id_data."', 
													`26` = '".$shift_id_data."', 
													`27` = '".$shift_id_data."', 
													`28` = '".$shift_id_data."', 
													`29` = '".$shift_id_data."', 
													`30` = '".$shift_id_data."', 
													`31` = '".$shift_id_data."',
													`month` = '".$key."',
													`year` = '".date('Y')."',
													`status` = '1' ,
													`unit` = '".$var111."',
													`unit_id` = '".$var11."' 
													WHERE id = '".$shift_schedule_data['id']."' ";
										$this->db->query($insert1);
										
										$year = date('Y');
										if($key == 12){
											$mod_key = 1;
											$mod_year = $year + 1;
										} else {
											$mod_key = $key + 1;
											$mod_year = $year;
										}
										//echo $mod_key;exit;
										
										$start_date_string = date('Y').'-'.$key.'-01';
										$start = new DateTime(date('Y').'-'.$key.'-01');
										//if(strtotime($start_date_string) > strtotime($var4)){
											$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
											$interval = DateInterval::createFromDateString('1 day');
											$period = new DatePeriod($start, $interval, $end);
											$week_array = array();
											foreach ($period as $dt)
											{
											   	$current_date = $dt->format('Y-m-d');
												if(strtotime($current_date) > strtotime($var4)){
												   	if ($dt->format('N') == 3)
												    {
												    	$sunday = $dt->format('Y-m-d');
												    	$month_name = date('M', strtotime($sunday));
											    		$year = date('Y', strtotime($sunday));
												    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
												    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
												    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
												    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
												    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
												    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
												    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
												    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
												    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
												    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
												    }
												    if ($dt->format('N') == 7 || $dt->format('N') == 6)
												    {
												    	$sunday = $dt->format('Y-m-d');
												    	$month_name = date('M', strtotime($sunday));
											    		$year = date('Y', strtotime($sunday));
												    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
												    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
												    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
												    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
												    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
												    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
											    		$current_day = date('j', strtotime($sunday));
											    		if($first_saturday == $current_day){
											    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
											    		} else {
											    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
											    		}

											    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
											    		if($second_saturday == $current_day){
											    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
											    		} else {
											    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
											    		}

											    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
											    		if($third_saturday == $current_day){
											    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
											    		} else {
											    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
											    		}

											    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
											    		if($fourth_saturday == $current_day){
											    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
											    		} else {
											    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
											    		}

											    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
											    		if($fifth_saturday == $current_day){
											    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
											    		} else {
											    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
											    		}
												        //$no++;
												    }
												}
											}
										    foreach ($week_array as $wkey => $wvalue) {
												$week_string = 'W_1_'.$var15;
												$halfday_string = 'HD_1_'.$var15;
												$shift_string = 'S_'.$var15;
												if($var15 == '1'){
													if($sat_status == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
													if($sat_status == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
														//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		//$this->db->query($sql2);
													}
													if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}

													if($wvalue['day_name'] == 'Sun'){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
												}
												/*
												if($var111 == 'VITA' && $var15 == '2'){
													if($wvalue['day_name'] == 'Wed'){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
													if($wvalue['day_name'] == 'Sun'){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
												} else {
												*/
												if($var15 == '2' || $var15 = '4'){
													if($wvalue['day_name'] == 'Sun'){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
													if($sat_status == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
													if($wvalue['day_name'] == 'Wed'){
														$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$var0."' ";	
										        		//echo $sql2;
										        		//echo '<br />';
										        		$this->db->query($sql2);
													}
												}
												//}
											}
										//}
									}
									/*
									if($db_unit_id == '0' || $db_shift_id == '0' || ($db_unit_id != $var11) || ($db_shift_id != $var15)){
										foreach($shift_schedule_data as $skey => $svalue){
											if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
												$s_data_exp = explode("_", $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $var15;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "', `unit` = '".$var111."', `unit_id` = '".$var11."', `emp_code` = '".$var0."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
												} else {
													$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift) . "', `unit` = '".$var111."', `unit_id` = '".$var11."', `emp_code` = '".$var0."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
												}
												$this->db->query($sql);	
												//$this->log->write($sql);	
											}
										}
									}
									*/
								} 
							}

							if($db_unit_id == '0' || ($db_unit_id != $var11)){
								$holidayloc_sql = "SELECT `holiday_id`, `location`, `location_id` FROM `oc_holiday_loc` WHERE `location_id` = '".$var11."' ";
								$holidayloc_datas = $this->db->query($holidayloc_sql);
								if($holidayloc_datas->num_rows > 0){
									$holidayloc_data = $holidayloc_datas->rows;
									foreach($holidayloc_data as $hkey => $hvalue){
										$holiday_sql = "SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$hvalue['holiday_id']."' AND `date` >= '".$var4."' ";
										$holiday_datas = $this->db->query($holiday_sql);
										if($holiday_datas->num_rows > 0){
											$holiday_data = $holiday_datas->row;
											$date = $holiday_data['date'];
											$month = date('n', strtotime($date));
											$year = date('Y', strtotime($date));
											$day = date('j', strtotime($date));
											$holiday_string = 'H_'.$hvalue['holiday_id'].'_'.$var15;
											$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$var0."' ";
											// echo $update_shift_schedule;
											// echo '<br />';
											// //exit;
											$this->db->query($update_shift_schedule);
										}
									}
									//echo 'out';exit;
								}
							}

							$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
							if($open_years->num_rows > 0){
								$open_year = $open_years->row['year'];
							} else {
								$open_year = date('Y');
							}
							
							$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$var0."' AND `year` = '".$open_year."' ");
							if($is_exist->num_rows == 0){
								$insert2 = "INSERT INTO `oc_leave` SET 
											`emp_id` = '".$var0."', 
											`emp_name` = '".$this->db->escape($var1)."', 
											`emp_doj` = '".$var4."', 
											`year` = '".$open_year."',
											`close_status` = '0' "; 
								$this->db->query($insert2);
								// echo $insert1;
								// echo '<br />';
								// exit;
							} else {
								$leave_data = $is_exist->row;
								$insert2 = "UPDATE `oc_leave` SET 
											`emp_id` = '".$var0."', 
											`emp_name` = '".$this->db->escape($var1)."', 
											`emp_doj` = '".$var4."' 
											WHERE `leave_id` = '".$leave_data['leave_id']."' "; 
								$this->db->query($insert2);
							}

							$current_date = date('Y-m-d');
							$current_month = date('n');
							if($current_month == 1 || $current_month == 2){
								$start_date = date('Y-02-01');
							} else {
								$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2019-06-26';
							}
							$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2019-06-30'
							$day = array();
							$days = $this->GetDays($start_date, $end_date);
							foreach ($days as $dkey => $dvalue) {
								$dates = explode('-', $dvalue);
								$day[$dkey]['day'] = $dates[2];
								$day[$dkey]['date'] = $dvalue;
							}
							// echo '<pre>';
							// print_r($day);
							// exit;
							$batch_id = '0';
							$emp_code = $var0;
							foreach($day as $dkeys => $dvalues){
								$filter_date_start = $dvalues['date'];
								$results = $this->getemployee_code($emp_code);
								// echo '<pre>';
								// print_r($results);
								// exit;
								foreach ($results as $rkey => $rvalue) {
									if(isset($rvalue['name']) && $rvalue['name'] != ''){
										$emp_name = $rvalue['name'];
										$department = $rvalue['department'];
										$unit = $rvalue['unit'];
										$group = '';
										$day_date = date('j', strtotime($filter_date_start));
										$month = date('n', strtotime($filter_date_start));
										$year = date('Y', strtotime($filter_date_start));
										$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
										//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
										$shift_schedule = $this->db->query($update3)->row;
										$schedule_raw = explode('_', $shift_schedule[$day_date]);
										if(!isset($schedule_raw[2])){
											$schedule_raw[2]= 1;
										}
										// echo '<pre>';
										// print_r($schedule_raw);
										// exit;
										if($schedule_raw[0] == 'S'){
											$shift_data = $this->getshiftdata($schedule_raw[1]);
											if(isset($shift_data['shift_id'])){
												$shift_intime = $shift_data['in_time'];
												$shift_outtime = $shift_data['out_time'];
												$day = date('j', strtotime($filter_date_start));
												$month = date('n', strtotime($filter_date_start));
												$year = date('Y', strtotime($filter_date_start));
												$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
												$trans_exist = $this->db->query($trans_exist_sql);
												if($trans_exist->num_rows == 0){
													$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
													//echo $sql;exit;
													$this->db->query($sql);
												}
											} else {
												$shift_data = $this->getshiftdata('1');
												$shift_intime = $shift_data['in_time'];
												$shift_outtime = $shift_data['out_time'];
												$day = date('j', strtotime($filter_date_start));
												$month = date('n', strtotime($filter_date_start));
												$year = date('Y', strtotime($filter_date_start));
												$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
												$trans_exist = $this->db->query($trans_exist_sql);
												if($trans_exist->num_rows == 0){
													$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
													$this->db->query($sql);
												}
											}
										} elseif ($schedule_raw[0] == 'W') {
											$shift_data = $this->getshiftdata($schedule_raw[2]);
											if(!isset($shift_data['shift_id'])){
												$shift_data = $this->getshiftdata('1');
											}
											$shift_intime = $shift_data['in_time'];
											$shift_outtime = $shift_data['out_time'];
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
											$trans_exist = $this->db->query($trans_exist_sql);
											if($trans_exist->num_rows == 0){
												$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
												$this->db->query($sql);
											}
										} elseif ($schedule_raw[0] == 'H') {
											$shift_data = $this->getshiftdata($schedule_raw[2]);
											if(!isset($shift_data['shift_id'])){
												$shift_data = $this->getshiftdata('1');
											}
											$shift_intime = $shift_data['in_time'];
											$shift_outtime = $shift_data['out_time'];
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
											$trans_exist = $this->db->query($trans_exist_sql);
											if($trans_exist->num_rows == 0){
												$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
												$this->db->query($sql);
											}
										} elseif ($schedule_raw[0] == 'HD') {
											$shift_data = $this->getshiftdata($schedule_raw[2]);
											if(!isset($shift_data['shift_id'])){
												$shift_data = $this->getshiftdata('1');
											}
											$shift_intime = $shift_data['in_time'];
											if($shift_data['shift_id'] == '1'){
												if($rvalue['sat_status'] == '1'){
													$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
													$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
												} else {
													$shift_outtime = $shift_data['out_time'];
												}
											} else {
												$shift_intime = $shift_data['in_time'];
												$shift_outtime = $shift_data['out_time'];
											}
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
											$trans_exist = $this->db->query($trans_exist_sql);
											if($trans_exist->num_rows == 0){
												$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
												$this->db->query($sql);
											}
										}
									}	
								}
							}
						} else {
							$final_employee_error_data[] = $employee_error_data;	
						}
						$emp_code_exist[$var0] = $var0;
					} else {
						if(isset($emp_code_exist[$var0])){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Duplicate'
							);
						}
						if($var0 == '' || $var0 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Employee Code Empty'
							);
						}
						$final_employee_error_data[] = $employee_error_data;
					}
				}
				$i ++;	
			}
			//echo 'out';
			//exit;
			fclose($file);
			
			// echo '<pre>';
			// print_r($final_employee_error_data);
			// exit;

			$url = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			//echo $url;exit;
			if(empty($final_employee_error_data)){
				$this->session->data['success'] = 'Employees Imported Successfully';
			} else {
				$this->session->data['warning'] = 'Employees Imported with following Errors';
			}
			$this->session->data['final_employee_error_data'] = $final_employee_error_data;
			$this->response->redirect($url);
		} else {
			$url = $this->url->link('tool/empimport', 'token=' . $this->session->data['token'], true);
			//exit;
			$url = str_replace('&amp;', '&', $url);
			$this->session->data['warning'] = 'Please Select the Valid CSV File';
			$this->response->redirect($url);
		}
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
	public function getEmployees_dat($emp_code) {
		$sql = "SELECT `division`, `region`, `unit`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getempdata($emp_code) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insert_attendance_data($data) {
		$sql = $this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `device_id` = '".$data['device_id']."', `status` = '0', `download_date` = '".$data['download_date']."', `log_id` = '".$data['log_id']."' ");
	}
	public function getemployees($data) {
		$sql = "SELECT `sat_status`, `emp_code`, `division_id`, `division`, `region_id`, `region`, `unit_id`, `unit`, `department_id`, `department`, `group`, `name` FROM `oc_employee` WHERE `status` = '1' ";
		if(!empty($data['filter_name_id'])){
			$sql .= " AND `emp_code` = '".$data['filter_name_id']."' ";
		}
		$sql .= " ORDER BY `shift_type` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getrawattendance_group_date_custom($emp_code, $date) {
		$query = $this->db->query("SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		return $query->row;
	}
	public function getshiftdata($shift_id) {
		$shift_data = array();
		if($shift_id == '1'){
			$shift_data['shift_id'] = '1';
			$shift_data['in_time'] = '09:30:00';
			$shift_data['out_time'] = '18:30:00';
		} else {
			$shift_data['shift_id'] = '2';
			$shift_data['in_time'] = '08:00:00';
			$shift_data['out_time'] = '20:00:00';
		}
		return $shift_data;
		// $query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		// if($query->num_rows > 0){
		// 	return $query->row;
		// } else {
		// 	return array();
		// }
	}

	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC ");
		$array = $query->row;
		return $array;
	}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		//$query = query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC", $conn);
		//$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$query = $this->db->query("SELECT * FROM oc_attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			//echo '<pre>';
			//print_r($act_intime);

			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			//echo '<pre>';
			//print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
				//echo '<pre>';
				//print_r($since_start);

				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			//echo '<pre>';
			//print_r($hour_array);

			//echo '<pre>';
			//print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 15){
					unset($hour_array[$ckey]);
				}
			}

			//echo '<pre>';
			//print_r($hour_array);
			$act_outtimes = '0';
			if($hour_array){
				$act_outtimes = max($hour_array);
			}
			//echo '<pre>';
			//print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		//echo '<pre>';
		//print_r($act_outtime);
		//exit;		
		return $act_outtime;
	}
	public function getorderhistory($data){
		$sql = "SELECT * FROM `oc_attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function getemployee_code($emp_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "'");
		return $query->rows;
	}
}
?>