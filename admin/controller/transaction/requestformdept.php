<?php    
class ControllerTransactionRequestformdept extends Controller { 
	private $error = array();

	public function index() {

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('transaction/requestformdept');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function approve() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '1' WHERE `id` = '".$id."' ";
				$this->db->query($can_leave_sql);
				
				$is_leave = $this->db->query("SELECT `batch_id` FROM `oc_requestform` WHERE `id` = '".$id."'")->row;
				if($is_leave['batch_id'] != '0'){
					$is_processed = $this->db->query("SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$is_leave['batch_id']."' ORDER BY `id` ASC LIMIT 1")->row;
					if($is_processed['p_status'] == '0'){
						$sql_update = $this->db->query("UPDATE `oc_leave_transaction_temp` SET `approval_1` = '0'`approval_2` = '0' WHERE `batch_id` = '".$is_leave['batch_id']."' ");	
						$this->db->query("DELETE FROM `oc_leave_transaction` WHERE `linked_batch_id` = '".$is_leave['batch_id']."' ");
						
						$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '3' WHERE `id` = '".$id."' ";
						$this->db->query($can_leave_sql);
					}
				}
			}

			$this->session->data['success'] = 'Request Approved Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '1' WHERE `id` = '".$id."' ";
			$this->db->query($can_leave_sql);
			
			$is_leave = $this->db->query("SELECT `batch_id` FROM `oc_requestform` WHERE `id` = '".$id."'")->row;
			if($is_leave['batch_id'] != '0'){
				$is_processed = $this->db->query("SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$is_leave['batch_id']."' ORDER BY `id` ASC LIMIT 1")->row;
				// echo '<pre>';
				// print_r($is_processed);
				// exit;
				if($is_processed['p_status'] == '0'){
					$sql_update = $this->db->query("UPDATE `oc_leave_transaction_temp` SET `approval_1` = '0', `approval_2` = '0' WHERE `batch_id` = '".$is_leave['batch_id']."' ");	
					$this->db->query("DELETE FROM `oc_leave_transaction` WHERE `linked_batch_id` = '".$is_leave['batch_id']."' ");
					
					$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '3' WHERE `id` = '".$id."' ";
					$this->db->query($can_leave_sql);
				}
			}
			$this->session->data['success'] = 'Request Approved Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function reject() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '2' WHERE `id` = '".$id."' ";
				$this->db->query($can_leave_sql);
			}

			$this->session->data['success'] = 'Request Approved Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$can_leave_sql = "UPDATE `oc_requestform` SET `approval_1` = '2' WHERE `id` = '".$id."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'Request Rejected Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = '0';
		}

		if (isset($this->request->get['filter_dept'])) {
			$filter_dept = $this->request->get['filter_dept'];
		} elseif (isset($this->session->data['dept_name'])) {
			$filter_dept = $this->session->data['dept_name'];
		} else {
			$filter_dept = null;
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['approve'] = $this->url->link('transaction/requestformdept/approve', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['reject'] = $this->url->link('transaction/requestformdept/reject', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['requests'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_approval_1' => $filter_approval_1,
			'filter_date' => $filter_date,
			'filter_dept' => $filter_dept,
			'filter_unit' => $filter_unit,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$request_types = array(
			'1' => 'Revert Leave',
			'2' => 'Missing Punch',
			'3' => 'Shift Change',
			'4' => 'Weekly Off Change',
			'5' => 'Holiday Change',
			//'6' => 'On Duty',
		);

		
		$employee_total = $this->model_transaction_transaction->getTotalrequest($data);
		$results = $this->model_transaction_transaction->getrequests($data);
		foreach ($results as $result) {
			$action = array();
			if ($result['approval_1'] == '0') {
				$action[] = array(
					'text' => 'Approve',
					'href' => $this->url->link('transaction/requestformdept/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);
				$action[] = array(
					'text' => 'Reject',
					'href' => $this->url->link('transaction/requestformdept/reject', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);
			} elseif($result['approval_1'] == '1' || $result['approval_1'] == '3' || $result['approval_1'] == '4') {
				$action[] = array(
					'text' => '---',
					'href' => ''
				);
			} else {
				$action[] = array(
					'text' => 'Approve',
					'href' => $this->url->link('transaction/requestformdept/approve', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);
			}	
			
			$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

			if($result['approval_1'] == '0'){
				$approval_1 = 'Pending';
			} elseif($result['approval_1'] == '1' || $result['approval_1'] == '3' || $result['approval_1'] == '4') {
				$approval_1 = 'Approved';
			} elseif($result['approval_1'] == '2'){
				$approval_1 = 'Rejected';
			}

			if($result['batch_id'] != '0'){
				$leave_from = $this->model_transaction_transaction->getleave_from_ess($result['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to_ess($result['batch_id']);
				$leave_data = $this->model_transaction_transaction->getleave_data($result['batch_id']);
				$leave_data = $leave_data['leave_type'].' '.date('d-m-Y', strtotime($leave_from)).' - '.date('d-m-Y', strtotime($leave_to));
				$result['dot'] = $leave_data;
			} else {
				$leave_data = '';
			}

			$this->data['requests'][] = array(
				'id' => $result['id'],
				'dot' => $result['dot'],
				'doi' => $result['doi'],
				'name' => $emp_data['name'],
				'description' => $result['description'],
				'dept_name' => $result['dept_name'],
				'unit' => $result['unit'],
				'request_type' => $request_types[$result['request_type']],
				'leave_data'        => $leave_data,
				'approval_1' => $approval_1,
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;
		$approves = array(
			'0' => 'All',
			'1' => 'Pending',
			'2' => 'Approved',
			'3' => 'Rejected',
		);

		$this->data['approves'] = $approves;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Moving' => 'Moving' 
		);

		$this->data['unit_data'] = $unit_data;

		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['dept_data'] = $department_data;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/requestformdept', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_unit'] = $filter_unit;
		
		$this->template = 'transaction/requestformdept_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		// if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
