<?php
class ControllerCatalogLeavemaster extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/leavemaster');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leavemaster');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/leavemaster');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leavemaster');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_leavemaster->addLeavemaster($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/leavemaster');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leavemaster');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_leavemaster->editLeavemaster($this->request->get['leavemaster_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/leavemaster');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leavemaster');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $leavemaster_id) {
				$this->model_catalog_leavemaster->deleteLeavemaster($leavemaster_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function export() { //echo "string";// exit;
		$this->language->load('catalog/leavemaster');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leavemaster');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'leavemaster_id';
			$leavemaster_datas = $this->model_catalog_leavemaster->getLeavemasters($data);

			$final_datas = array();
			foreach($leavemaster_datas as $skey => $svalue){
				$final_datas[$skey]['name'] = $svalue['leavemaster'];
				$final_datas[$skey]['code'] = $svalue['leavemaster_code'];
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Leavemaster';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/leavemaster_html.tpl');
			//echo $html;exit;
			$filename = "Leavemaster";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'leavemaster';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/leavemaster/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/leavemaster/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export'] = $this->url->link('catalog/leavemaster/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	


		$this->data['leavemasters'] = array();

		$data = array(
			'filter_name'  => $filter_name,
			'filter_name_id'  => $filter_name_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$leavemaster_total = $this->model_catalog_leavemaster->getTotalLeavemasters();

		$results = $this->model_catalog_leavemaster->getLeavemasters($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/leavemaster/update', 'token=' . $this->session->data['token'] . '&leavemaster_id=' . $result['leavemaster_id'] . $url, 'SSL')
			);

			$this->data['leavemasters'][] = array(
				'leavemaster_id' => $result['leavemaster_id'],
				'leavemaster'        => $result['leavemaster'],
				'leavemaster_code'        => $result['leavemaster_code'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['leavemaster_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}	

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_leavemaster'] = $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . '&sort=leavemaster' . $url, 'SSL');

		$this->data['sort_leavemaster_code'] = $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . '&sort=leavemaster_code' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $leavemaster_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/leavemaster_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['leavemaster'])) {
			$this->data['error_leavemaster'] = $this->error['leavemaster'];
		} else {
			$this->data['error_leavemaster'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['leavemaster_id'])) {
			$this->data['action'] = $this->url->link('catalog/leavemaster/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/leavemaster/update', 'token=' . $this->session->data['token'] . '&leavemaster_id=' . $this->request->get['leavemaster_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/leavemaster', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['leavemaster_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$leavemaster_info = $this->model_catalog_leavemaster->getLeavemaster($this->request->get['leavemaster_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['leavemaster'])) {
			$this->data['leavemaster'] = $this->request->post['leavemaster'];
		} elseif (!empty($leavemaster_info)) {
			$this->data['leavemaster'] = $leavemaster_info['leavemaster'];
		} else {
			$this->data['leavemaster'] = '';
		}

		if (isset($this->request->post['leavemaster_code'])) {
			$this->data['leavemaster_code'] = $this->request->post['leavemaster_code'];
		} elseif (!empty($leavemaster_info)) {
			$this->data['leavemaster_code'] = $leavemaster_info['leavemaster_code'];
		} else {
			$this->data['leavemaster_code'] = '';
		}

		$this->template = 'catalog/leavemaster_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if(isset($this->request->get['leavemaster_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/leavemaster')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/leavemaster')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['leavemaster']) < 1) || (utf8_strlen($this->request->post['leavemaster']) > 64)) {
			$this->error['leavemaster'] = 'Plese Enter Leavemaster Name';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/leavemaster')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/leavemaster');
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_leavemaster->getLeavemasters($data);
			foreach ($results as $result) {
				$json[] = array(
					'leavemaster_id' => $result['leavemaster_id'],
					'leavemaster'            => strip_tags(html_entity_decode($result['leavemaster'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['leavemaster'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>