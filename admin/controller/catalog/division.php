<?php
class ControllerCatalogDivision extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/division');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/division');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/division');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/division');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_division->addDivision($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_region_id'])) {
				$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/division');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/division');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_division->editDivision($this->request->get['division_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_region_id'])) {
				$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/division');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/division');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $division_id) {
				$this->model_catalog_division->deleteDivision($division_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_region'])) {
				$url .= '&filter_region=' . $this->request->get['filter_region'];
			}

			if (isset($this->request->get['filter_region_id'])) {
				$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function export() { //echo "string";// exit;
		$this->language->load('catalog/division');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/division');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'division_id';
			$division_datas = $this->model_catalog_division->getDivisions($data);

			$final_datas = array();
			foreach($division_datas as $skey => $svalue){
				$final_datas[$skey]['name'] = $svalue['division'];
				$final_datas[$skey]['code'] = $svalue['division_code'];
				$final_datas[$skey]['region'] = $svalue['region_name'];
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Division';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/division_html.tpl');
			//echo $html;exit;
			$filename = "Division";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_region'])) {
			$filter_region = $this->request->get['filter_region'];
		} else {
			$filter_region = '';
		}

		if (isset($this->request->get['filter_region_id'])) {
			$filter_region_id = $this->request->get['filter_region_id'];
		} else {
			$filter_region_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'division';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_region_id'])) {
			$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/division/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/division/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export'] = $this->url->link('catalog/division/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	


		$this->data['divisions'] = array();

		$data = array(
			'filter_name'  => $filter_name,
			'filter_name_id'  => $filter_name_id,
			'filter_region'  => $filter_region,
			'filter_region_id'  => $filter_region_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$division_total = $this->model_catalog_division->getTotalDivisions();

		$results = $this->model_catalog_division->getDivisions($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/division/update', 'token=' . $this->session->data['token'] . '&division_id=' . $result['division_id'] . $url, 'SSL')
			);

			$this->data['divisions'][] = array(
				'division_id' => $result['division_id'],
				'division'        => $result['division'],
				'division_code'        => $result['division_code'],
				'region_name'        => $result['region_name'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['division_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}	

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_region_id'])) {
			$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_division'] = $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . '&sort=division' . $url, 'SSL');
		$this->data['sort_division_code'] = $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . '&sort=division_code' . $url, 'SSL');
		$this->data['sort_region_name'] = $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . '&sort=region_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_region_id'])) {
			$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $division_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_region'] = $filter_region;
		$this->data['filter_region_id'] = $filter_region_id;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/division_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['division'])) {
			$this->data['error_division'] = $this->error['division'];
		} else {
			$this->data['error_division'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_region'])) {
			$url .= '&filter_region=' . $this->request->get['filter_region'];
		}

		if (isset($this->request->get['filter_region_id'])) {
			$url .= '&filter_region_id=' . $this->request->get['filter_region_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['division_id'])) {
			$this->data['action'] = $this->url->link('catalog/division/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/division/update', 'token=' . $this->session->data['token'] . '&division_id=' . $this->request->get['division_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/division', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['division_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$division_info = $this->model_catalog_division->getDivision($this->request->get['division_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['division'])) {
			$this->data['division'] = $this->request->post['division'];
		} elseif (!empty($division_info)) {
			$this->data['division'] = $division_info['division'];
		} else {
			$this->data['division'] = '';
		}

		if (isset($this->request->post['division_code'])) {
			$this->data['division_code'] = $this->request->post['division_code'];
		} elseif (!empty($division_info)) {
			$this->data['division_code'] = $division_info['division_code'];
		} else {
			$this->data['division_code'] = '';
		}

		if (isset($this->request->post['region_id'])) {
			$this->data['region_id'] = $this->request->post['region_id'];
		} elseif (!empty($division_info)) {
			$this->data['region_id'] = $division_info['region_id'];
		} else {
			$this->data['region_id'] = '';
		}

		if (isset($this->request->post['region_name'])) {
			$this->data['region_name'] = $this->request->post['region_name'];
		} elseif (!empty($division_info)) {
			$this->data['region_name'] = $division_info['region_name'];
		} else {
			$this->data['region_name'] = '';
		}

		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			$dvalue['region'] = html_entity_decode($dvalue['region']);
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->template = 'catalog/division_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if(isset($this->request->get['division_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/division')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/division')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['division']) < 1) || (utf8_strlen($this->request->post['division']) > 64)) {
			$this->error['division'] = 'Plese Enter Division Name';
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/division')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/division');
			if(isset($this->request->get['filter_region_id']) && $this->request->get['filter_region_id'] != '' && $this->request->get['filter_region_id'] != '0'){
				$filter_region_id = $this->request->get['filter_region_id'];
			} else {
				$filter_region_id = '';
			}
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_region_id' => $filter_region_id,
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_division->getDivisions($data);
			foreach ($results as $result) {
				$json[] = array(
					'division_id' => $result['division_id'],
					'division'            => strip_tags(html_entity_decode($result['division'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['division'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>