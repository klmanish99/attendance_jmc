<?php
class ControllerReportMuster extends Controller { 
	public function index() {  
		$this->language->load('report/muster');
		$this->load->model('report/common_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			$filter_date_start = '';
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = '';//date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/muster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_month'	     	 => $filter_month,
			'filter_year'	     => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// $filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$year = $data['filter_year'];
		$month = $data['filter_month'];
		if($month == 1){
			$prev_month = 12;
			$prev_year = $year - 1;
		} else {
			$prev_month = $month - 1;
			$prev_year = $year;
		}
		$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
		$filter_date_end = sprintf("%04d-%02d-%02d", $year, $month, '25');
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $this->data['days'] = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		$act_total_count = 0;	
		$total_cnt = 0;	
		// echo '<pre>';
		// print_r($this->request->get);
		// exit;
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees_muster($data);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				// echo'<pre>';
				// print_r($transaction_datas);
				//exit;
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['division'] = $rvalue['division'];
				$final_datas[$rvalue['emp_code']]['region'] = $rvalue['region'];
				$final_datas[$rvalue['emp_code']]['unit'] = $rvalue['unit'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];

				$week_count = 0;
				$holiday_count = 0;
				$halfday_count = 0;
				$leave_count = 0;
				$present_count = 0;
				$absent_count = 0;
				$compli_count = 0;
				$od_count = 0;
				$cof_count = 0;
				$tour_count = 0;
				$exception_count = 0;
				$total_cnt = 0;
				if($transaction_datas){
					/*echo '<pre>';
					print_r($transaction_datas);
					exit*/;
					$count_t = count($transaction_datas) - 1;
					foreach($transaction_datas as $tkey => $tvalue){
						$statuss = '';
						if($tvalue['leave_status'] == '1') { 
							if($tvalue['firsthalf_status'] == 'LOP'){
								$statuss = 'A';
							} else {
								if($tvalue['firsthalf_status'] != '0'){
									$statuss = $tvalue['firsthalf_status'];
								} else {
									$statuss = 'A';
								}
							}
						} elseif($tvalue['leave_status'] == '0.5') { 
							$statuss = 'HL';
							// if($tvalue['firsthalf_status'] != '0'){
							// 	$statuss = $tvalue['firsthalf_status'];
							// } elseif($tvalue['secondhalf_status'] != '0'){
							// 	$statuss = $tvalue['secondhalf_status'];
							// } else {
							// 	$statuss = 'A';
							// }
							// if($statuss == '1'){
							// 	$statuss = 'P';
							// } elseif($statuss == ''){
							// 	$statuss = 'A';
							// }
						} elseif($tvalue['weekly_off'] != '0') { 
							if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
								$statuss = 'W';
							} else {
								$statuss = 'W';
							}
						} elseif($tvalue['holiday_id'] != '0') {
							if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
								$statuss = 'PH';
							} else {
								$statuss = 'H';
							}
						} elseif($tvalue['on_duty'] != '0') {
							$statuss = 'OD';
						} elseif($tvalue['present_status'] == '1') {
							$statuss = 'P';
						} elseif($tvalue['absent_status'] == '1') {
							if($tvalue['company_id'] <> '1'){
								if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
									$statuss = 'A';
								} else {
									$statuss = 'A';
								}
							} else {
								$statuss = 'A';
							}
						} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5') {
							$statuss = 'HD';
						}

						/*echo $tvalue['date'];
						echo '<br />';
						echo $statuss;
						echo '<br />';
*/
						if($tvalue['firsthalf_status'] == '1'){
							$firsthalf_status = 'P';
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_status = 'P';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'A';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						if($tvalue['firsthalf_status'] == 'WO') {
							if($tvalue['present_status'] == '1'){
								$firsthalf_status = 'W';
								$secondhalf_status = 'W';
							} elseif($tvalue['present_status'] == '0.5'){
								$firsthalf_status = 'W';
								$secondhalf_status = 'A';
							} else {
								$firsthalf_status = $tvalue['firsthalf_status'];
								$secondhalf_status = $tvalue['secondhalf_status'];
							}
						} elseif($tvalue['firsthalf_status'] == 'HLD') {
							if($tvalue['present_status'] == '1'){
								$firsthalf_status = 'PH';
								$seconhalf_status = 'PH';
							} elseif($tvalue['present_status'] == '0.5'){
								$firsthalf_status = 'PH';
								$seconhalf_status = 'A';
							} else {
								$firsthalf_status = $tvalue['firsthalf_status'];
								$secondhalf_status = $tvalue['secondhalf_status'];
							}
						}
						
						// if($firsthalf_status == 'COF'){
						// 	$cof_count ++;
						// } elseif($firsthalf_status == 'OD'){
						// 	$od_count ++;
						// } elseif($firsthalf_status == 'TOUR'){
						// 	$tour_count ++;
						// }

						if($tvalue['leave_status'] == 1){
							// echo '<pre>';
							// print_r($tvalue);
							// exit;
							$total_cnt = $total_cnt + 1;
							//$leave_count ++;
							if($secondhalf_status == 'OD' || $secondhalf_status == 'TOUR'){
								$od_count ++;
							} elseif($secondhalf_status == 'LOP'){
								$absent_count ++;
							} else { 
								$leave_count ++;	
							}
						} elseif($tvalue['leave_status'] == 0.5){
							if($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
								$od_count = $od_count + 0.5;
							} elseif($firsthalf_status == 'TOUR' || $secondhalf_status == 'TOUR'){
								$od_count = $od_count + 0.5;
							} else {
								$leave_count = $leave_count + 0.5;	
							}
							//$leave_count = $leave_count + 0.5;	
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;	
							}
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;	
							}
						} elseif($tvalue['weekly_off'] != 0){
							
							if($tkey == 0){
								$last_key = $tkey;
								$last_key_1 = $last_key;
							} else {
								$last_key = $tkey - 1;
								if($last_key == 0){
									$last_key_1 = $last_key;
								} else {
									$last_key_1 = $last_key - 1;
								}
							}
							if($count_t == $tkey){
								$next_key = $tkey;
								$next_key_1 = $next_key;
							} else {
								$next_key = $tkey + 1;	
								$next_key_1 = $next_key + 1;
							}

							// echo'<pre>';
							// print_r($next_key_1);

							// if($last_key == '1'){
							// 	$month_current = date('n', strtotime($transaction_datas[$key]['date']));
							// 	$year_current = date('Y', strtotime($transaction_datas[$key]['date']));
							// 	if($month_current == '1'){
							// 		$month_previous = '12';
							// 		$prev_year = $year_current - 1;
							// 	} else {
							// 		$month_previous = $month_current - 1;
							// 		$year_previous = $year_current - 1;
							// 	}
							// 	$total_days_month = cal_days_in_month('CAL_GREGORIAN', $month_previous, $year_previous);
							// 	$last_key_1 = $total_days_month;
							// } else {
							// 	$last_key_1 = $last_key - 1;
							// }
							// echo '<pre>';
							// print_r($transaction_datas[$last_key]);
							// echo '<br />';
							// echo $statuss;
							// echo '<br />';
							if(isset($transaction_datas[$last_key]['company_id']) && $transaction_datas[$last_key]['company_id'] <> '1'){
								if($transaction_datas[$last_key]['act_intime'] != '00:00:00' && $transaction_datas[$last_key]['act_outtime'] == '00:00:00'){
									$last_exception = '0';
								} else {
									$last_exception = '0';
								}
							} else {
								$last_exception = '0';
							}

							if(isset($transaction_datas[$next_key]['company_id']) && $transaction_datas[$next_key]['company_id'] <> '1'){
								if($transaction_datas[$next_key]['act_intime'] != '00:00:00' && $transaction_datas[$next_key]['act_outtime'] == '00:00:00'){
									$next_exception = '0';
								} else {
									$next_exception = '0';
								}
							} else {
								$next_exception = '0';
							}

							if(isset($transaction_datas[$last_key_1]['company_id']) && $transaction_datas[$last_key_1]['company_id'] <> '1'){
								if($transaction_datas[$last_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$last_key_1]['act_outtime'] == '00:00:00'){
									$last_exception_1 = '0';
								} else {
									$last_exception_1 = '0';
								}
							} else {
								$last_exception_1 = '0';
							}

							if(isset($transaction_datas[$next_key_1]['company_id']) && $transaction_datas[$next_key_1]['company_id'] <> '1'){
								if($transaction_datas[$next_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$next_key_1]['act_outtime'] == '00:00:00'){
									$next_exception_1 = '0';
								} else {
									$next_exception_1 = '0';
								}
							} else {
								$next_exception_1 = '0';
							}
							// echo '<pre>';
							// print_r($statuss);
							// echo '<br />';
							// echo $transaction_datas[$next_key]['absent_status'];
							// echo '<br />';

							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception == '0' && $next_exception == '0'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
								$statuss = 'A';
								// echo'innn';
								// exit;
							} else {
								//$week_count ++;
								if($transaction_datas[$next_key]['weekly_off'] != '0'){
									if(isset($transaction_datas[$last_key]['absent_status']) && $transaction_datas[$last_key]['absent_status'] == '1' && isset($transaction_datas[$next_key_1]['absent_status']) && $transaction_datas[$next_key_1]['absent_status'] == '1' && $last_exception == '0' && $next_exception_1 == '0'){
										$absent_count ++;
										$firsthalf_status = 'A';
										$secondhalf_status = 'A';
										$statuss = 'A';
										//echo'innn111';exit;
									} else {
										$week_count ++;			
									}
								} else {
									if($transaction_datas[$last_key]['weekly_off'] != '0'){
										if(isset($transaction_datas[$last_key_1]['absent_status']) && $transaction_datas[$last_key_1]['absent_status'] == '1' && isset($transaction_datas[$next_key]['absent_status']) && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception_1 == '0' && $next_exception == '0'){
											$absent_count ++;
											$firsthalf_status = 'A';
											$secondhalf_status = 'A';
											$statuss = 'A';
											// echo 'innnn222';
											// exit;
										} else {
											$week_count ++;			
										}	
									} else {
										$week_count ++;		
									}
								}
							}
							//$week_count ++;	
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['holiday_id'] != 0){
							/*
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
								$statuss = 'A';
							} else {
								$holiday_count ++;	
							}
							*/
							$holiday_count ++;	
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['on_duty'] == 1){
							$total_cnt = $total_cnt + 1;
							$od_count ++;
						} elseif($tvalue['present_status'] == 1){
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 0.5){
							$total_cnt = $total_cnt + 0.5;
							$present_count = $present_count + 0.5;
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;
							}
						} elseif($tvalue['absent_status'] == 1){
							$total_cnt = $total_cnt + 1;
							if($tvalue['company_id'] <> '1'){
								if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
									//$exception_count ++;
									$absent_count ++;
								} else {
									$absent_count ++;
								}
							} else {
								$absent_count ++;
							}
						} elseif($tvalue['absent_status'] == 0.5){
							$total_cnt = $total_cnt + 0.5;
							$absent_count = $absent_count + 0.5;
							if($tvalue['present_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
						}
						
						$performance_data['action'][$tvalue['date']] = array(
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'status' => $statuss,
							'date' => $tvalue['date'],
						);
					}
					//exit;
					// echo'<pre>';
					// print_r($performance_data);
					// exit;
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$performance_data['action'][$dvalue['date']] = array(
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'status' => 'A',
									'date' => $dvalue['date']
								);		
							}
						}
					}
					$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
					$total_present_count = $present_count + $week_count + $holiday_count + $leave_count + $od_count + $exception_count;
					$act_total_count = $present_count + $absent_count + $week_count + $holiday_count + $leave_count + $od_count + $exception_count;
					$final_datas[$rvalue['emp_code']]['week_count'] = $week_count;
					$final_datas[$rvalue['emp_code']]['holiday_count'] = $holiday_count;
					$final_datas[$rvalue['emp_code']]['od_count'] = $od_count;
					$final_datas[$rvalue['emp_code']]['halfday_count'] = $halfday_count;
					$final_datas[$rvalue['emp_code']]['compli_count'] = $compli_count;
					$final_datas[$rvalue['emp_code']]['leave_count'] = $leave_count;
					$final_datas[$rvalue['emp_code']]['present_count'] = $present_count;
					$final_datas[$rvalue['emp_code']]['absent_count'] = $absent_count;
					$final_datas[$rvalue['emp_code']]['exception_count'] = $exception_count;
					$final_datas[$rvalue['emp_code']]['total_cnt'] = $total_cnt;
					$final_datas[$rvalue['emp_code']]['total_present_count'] = $total_present_count;
					$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
			//exit;
		}
		
		//$final_datas = array();
		$this->data['final_datas'] = $final_datas;
		
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;

		$fields_data = array(
			'1' => 'Emp Id',
			'2' => 'Employee Name',
			'3' => 'Punch Id',
			'4' => 'Department',
			'5' => 'Site',
			'6' => 'Division',
			'7' => 'Region',
			'8' => 'Present Count',
			'16' => 'Exception Count',
			'9' => 'Absent Count',
			'10' => 'Holiday Count',
			'11' => 'Weekly Off Count',
			'15' => 'On Duty Count',
			'12' => 'Leave Count',
			'13' => 'Total Present Days',
			'14' => 'Total Month Days',
		);
		$this->data['fields_data'] = $fields_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['count_mismatch'] = '0';
		if($total_cnt > '0' && count($day) != $total_cnt){
			$this->data['count_mismatch'] = '1';
			$this->data['error_warning'] = 'Days Does not Match';			
		} elseif($act_total_count > '0' && $act_total_count != count($day)){
			$this->data['count_mismatch'] = '1';
			$this->data['error_warning'] = 'Total of Present + Absent + Weekly off + Holiday + Leave + Absent and Days Does not Match';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		$months = array(
			'1' => 'January',
			'2' => 'Feburary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2017' => '2017',
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',

		);
		$this->data['years'] = $years;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_fields'] = $filter_fields;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);
		$this->data['group'] = $group;

		$this->template = 'report/muster.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			$filter_date_start = '';
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';//date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_month'	     	 => $filter_month,
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// //$filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$year = $data['filter_year'];
		$month = $data['filter_month'];
		if($month == 1){
			$prev_month = 12;
			$prev_year = $year - 1;
		} else {
			$prev_month = $month - 1;
			$prev_year = $year;
		}
		$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
		$filter_date_end = sprintf("%04d-%02d-%02d", $year, $month, '25');

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $days = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		$results = $this->model_report_common_report->getemployees_muster($data);
		// echo '<pre>';
		// print_r($results);
		// exit;
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			
			$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
			$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
			$final_datas[$rvalue['emp_code']]['division'] = $rvalue['division'];
			$final_datas[$rvalue['emp_code']]['region'] = $rvalue['region'];
			$final_datas[$rvalue['emp_code']]['unit'] = $rvalue['unit'];
			$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];

			$week_count = 0;
			$holiday_count = 0;
			$halfday_count = 0;
			$leave_count = 0;
			$present_count = 0;
			$absent_count = 0;
			$compli_count = 0;
			$od_count = 0;
			$cof_count = 0;
			$tour_count = 0;
			$exception_count = 0;
			$total_cnt = 0;

			if($transaction_datas){
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				$count_t = count($transaction_datas) - 1;
				foreach($transaction_datas as $tkey => $tvalue){
					$statuss = '';
					if($tvalue['leave_status'] == '1') { 
						if($tvalue['firsthalf_status'] == 'LOP'){
							$statuss = 'A';
						} else {
							if($tvalue['firsthalf_status'] != '0'){
								$statuss = $tvalue['firsthalf_status'];
							} else {
								$statuss = 'A';
							}
						}
					} elseif($tvalue['leave_status'] == '0.5') { 
						$statuss = 'HL';
						// if($tvalue['firsthalf_status'] != '0'){
						// 	$statuss = $tvalue['firsthalf_status'];
						// } elseif($tvalue['secondhalf_status'] != '0'){
						// 	$statuss = $tvalue['secondhalf_status'];
						// } else {
						// 	$statuss = 'A';
						// }
						// if($statuss == '1'){
						// 	$statuss = 'P';
						// } elseif($statuss == ''){
						// 	$statuss = 'A';
						// }
					} elseif($tvalue['weekly_off'] != '0') { 
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
							$statuss = 'W';
						} else {
							$statuss = 'W';
						}
					} elseif($tvalue['holiday_id'] != '0') {
						if($tvalue['present_status'] == '1' || $tvalue['present_status'] == '0.5'){
							$statuss = 'PH';
						} else {
							$statuss = 'H';
						}
					} elseif($tvalue['on_duty'] != '0') {
						$statuss = 'OD';
					} elseif($tvalue['present_status'] == '1') {
						$statuss = 'P';
					} elseif($tvalue['absent_status'] == '1') {
						if($tvalue['company_id'] <> '1'){
							if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
								$statuss = 'A';
							} else {
								$statuss = 'A';
							}
						} else {
							$statuss = 'A';
						}
					} elseif($tvalue['present_status'] == '0.5' || $tvalue['absent_status'] == '0.5') {
						$statuss = 'HD';
					}

					if($tvalue['firsthalf_status'] == '1'){
						$firsthalf_status = 'P';
					} elseif($tvalue['firsthalf_status'] == '0'){
						$firsthalf_status = 'A';
					} else {
						$firsthalf_status = $tvalue['firsthalf_status'];
					}

					if($tvalue['secondhalf_status'] == '1'){
						$secondhalf_status = 'P';
					} elseif($tvalue['secondhalf_status'] == '0'){
						$secondhalf_status = 'A';
					} else {
						$secondhalf_status = $tvalue['secondhalf_status'];
					}

					if($tvalue['firsthalf_status'] == 'WO') {
						if($tvalue['present_status'] == '1'){
							$firsthalf_status = 'W';
							$secondhalf_status = 'W';
						} elseif($tvalue['present_status'] == '0.5'){
							$firsthalf_status = 'W';
							$secondhalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
							$secondhalf_status = $tvalue['secondhalf_status'];
						}
					} elseif($tvalue['firsthalf_status'] == 'HLD') {
						if($tvalue['present_status'] == '1'){
							$firsthalf_status = 'PH';
							$seconhalf_status = 'PH';
						} elseif($tvalue['present_status'] == '0.5'){
							$firsthalf_status = 'PH';
							$seconhalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
							$secondhalf_status = $tvalue['secondhalf_status'];
						}
					}
					
					// if($firsthalf_status == 'COF'){
					// 	$cof_count ++;
					// } elseif($firsthalf_status == 'OD'){
					// 	$od_count ++;
					// } elseif($firsthalf_status == 'TOUR'){
					// 	$tour_count ++;
					// }

					if($tvalue['leave_status'] == 1){
						$total_cnt = $total_cnt + 1;
						//$leave_count ++;
						if($secondhalf_status == 'OD' || $secondhalf_status == 'TOUR'){
							$od_count ++;
						} elseif($secondhalf_status == 'LOP'){
							$absent_count ++;
						} else { 
							$leave_count ++;	
						}
					} elseif($tvalue['leave_status'] == 0.5){
						if($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
							$od_count = $od_count + 0.5;
						} elseif($firsthalf_status == 'TOUR' || $secondhalf_status == 'TOUR'){
							$od_count = $od_count + 0.5;
						} else {
							$leave_count = $leave_count + 0.5;	
						}
						//$leave_count = $leave_count + 0.5;	
						$total_cnt = $total_cnt + 0.5;
						if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
							$total_cnt = $total_cnt + 0.5;
							$present_count = $present_count + 0.5;	
						}
						if($tvalue['absent_status'] == 0.5){
							$total_cnt = $total_cnt + 0.5;
							$absent_count = $absent_count + 0.5;	
						}
					} elseif($tvalue['weekly_off'] != 0){
						if($tkey == 0){
							$last_key = $tkey;
							$last_key_1 = $last_key;
						} else {
							$last_key = $tkey - 1;
							if($last_key == 0){
								$last_key_1 = $last_key;
							} else {
								$last_key_1 = $last_key - 1;
							}
						}
						if($count_t == $tkey){
							$next_key = $tkey;
							$next_key_1 = $next_key;
						} else {
							$next_key = $tkey + 1;	
							$next_key_1 = $next_key + 1;
						}
						
						if(isset($transaction_datas[$last_key]['company_id']) && $transaction_datas[$last_key]['company_id'] <> '1'){
							if($transaction_datas[$last_key]['act_intime'] != '00:00:00' && $transaction_datas[$last_key]['act_outtime'] == '00:00:00'){
								$last_exception = '0';
							} else {
								$last_exception = '0';
							}
						} else {
							$last_exception = '0';
						}

						if(isset($transaction_datas[$next_key]['company_id']) && $transaction_datas[$next_key]['company_id'] <> '1'){
							if($transaction_datas[$next_key]['act_intime'] != '00:00:00' && $transaction_datas[$next_key]['act_outtime'] == '00:00:00'){
								$next_exception = '0';
							} else {
								$next_exception = '0';
							}
						} else {
							$next_exception = '0';
						}

						if(isset($transaction_datas[$last_key_1]['company_id']) && $transaction_datas[$last_key_1]['company_id'] <> '1'){
							if($transaction_datas[$last_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$last_key_1]['act_outtime'] == '00:00:00'){
								$last_exception_1 = '0';
							} else {
								$last_exception_1 = '0';
							}
						} else {
							$last_exception_1 = '0';
						}

						if(isset($transaction_datas[$next_key_1]['company_id']) && $transaction_datas[$next_key_1]['company_id'] <> '1'){
							if($transaction_datas[$next_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$next_key_1]['act_outtime'] == '00:00:00'){
								$next_exception_1 = '0';
							} else {
								$next_exception_1 = '0';
							}
						} else {
							$next_exception_1 = '0';
						}

						if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception == '0' && $next_exception == '0'){
							$absent_count ++;
							$firsthalf_status = 'A';
							$secondhalf_status = 'A';
							$statuss = 'A';
						} else {
							//$week_count ++;
							if($transaction_datas[$next_key]['weekly_off'] != '0'){
								if(isset($transaction_datas[$last_key]['absent_status']) && $transaction_datas[$last_key]['absent_status'] == '1' && isset($transaction_datas[$next_key_1]['absent_status']) && $transaction_datas[$next_key_1]['absent_status'] == '1' && $last_exception == '0' && $next_exception_1 == '0'){
									$absent_count ++;
									$firsthalf_status = 'A';
									$secondhalf_status = 'A';
									$statuss = 'A';
								} else {
									$week_count ++;			
								}
							} else {
								if($transaction_datas[$last_key]['weekly_off'] != '0'){
									if(isset($transaction_datas[$last_key_1]['absent_status']) && $transaction_datas[$last_key_1]['absent_status'] == '1' && isset($transaction_datas[$next_key]['absent_status']) && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception_1 == '0' && $next_exception == '0'){
										$absent_count ++;
										$firsthalf_status = 'A';
										$secondhalf_status = 'A';
										$statuss = 'A';
									} else {
										$week_count ++;			
									}	
								} else {
									$week_count ++;		
								}
							}
						}
						//$week_count ++;	
						$total_cnt = $total_cnt + 1;
					} elseif($tvalue['holiday_id'] != 0){
						/*
						if($tkey == 0){
							$last_key = $tkey;
						} else {
							$last_key = $tkey - 1;
						}
						if($count_t == $tkey){
							$next_key = $tkey;
						} else {
							$next_key = $tkey + 1;	
						}
						if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
							$absent_count ++;
							$firsthalf_status = 'A';
							$secondhalf_status = 'A';
							$statuss = 'A';
						} else {
							$holiday_count ++;	
						}
						*/
						$holiday_count ++;	
						$total_cnt = $total_cnt + 1;
					} elseif($tvalue['on_duty'] == 1){
						$total_cnt = $total_cnt + 1;
						$od_count ++;
					} elseif($tvalue['present_status'] == 1){
						$total_cnt = $total_cnt + 1;
						$present_count ++;
					} elseif($tvalue['present_status'] == 0.5){
						$total_cnt = $total_cnt + 0.5;
						$present_count = $present_count + 0.5;
						if($tvalue['absent_status'] == 0.5){
							$total_cnt = $total_cnt + 0.5;
							$absent_count = $absent_count + 0.5;
						}
					} elseif($tvalue['absent_status'] == 1){
						$total_cnt = $total_cnt + 1;
						if($tvalue['company_id'] <> '1'){
							if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
								$absent_count ++;
								//$exception_count ++;
							} else {
								$absent_count ++;
							}
						} else {
							$absent_count ++;
						}
					} elseif($tvalue['absent_status'] == 0.5){
						$total_cnt = $total_cnt + 0.5;
						$absent_count = $absent_count + 0.5;
						if($tvalue['present_status'] == 0.5){
							$total_cnt = $total_cnt + 0.5;
							$present_count = $present_count + 0.5;
						}
					}
										
					$performance_data['action'][$tvalue['date']] = array(
						'firsthalf_status' => $firsthalf_status,
						'secondhalf_status' => $secondhalf_status,
						'status' => $statuss,
						'date' => $tvalue['date'],
					);
				}
				foreach ($day as $dkey => $dvalue) {
					foreach ($performance_data as $pkey => $pvalue) {
						if(isset($pvalue[$dvalue['date']]['date'])){
						} else {
							$performance_data['action'][$dvalue['date']] = array(
								'firsthalf_status' => '',
								'secondhalf_status' => '',
								'status' => 'A',
								'date' => $dvalue['date']
							);		
						}
					}
				}

				$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
				$total_present_count = $present_count + $week_count + $holiday_count + $leave_count + $od_count + $exception_count;
				$act_total_count = $present_count + $absent_count + $week_count + $holiday_count + $leave_count + $od_count + $exception_count;
				$final_datas[$rvalue['emp_code']]['week_count'] = $week_count;
				$final_datas[$rvalue['emp_code']]['holiday_count'] = $holiday_count;
				$final_datas[$rvalue['emp_code']]['od_count'] = $od_count;
				$final_datas[$rvalue['emp_code']]['halfday_count'] = $halfday_count;
				$final_datas[$rvalue['emp_code']]['compli_count'] = $compli_count;
				$final_datas[$rvalue['emp_code']]['leave_count'] = $leave_count;
				$final_datas[$rvalue['emp_code']]['present_count'] = $present_count;
				$final_datas[$rvalue['emp_code']]['absent_count'] = $absent_count;
				$final_datas[$rvalue['emp_code']]['exception_count'] = $exception_count;
				$final_datas[$rvalue['emp_code']]['total_cnt'] = $total_cnt;
				$final_datas[$rvalue['emp_code']]['total_present_count'] = $total_present_count;
				$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
			} else {
				unset($final_datas[$rvalue['emp_code']]);
			}
		}
		//$final_datass = array_chunk($final_datas, 9);
		if($final_datas){
			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			
			$year = $filter_year;
			$month = $filter_month;
			if($month == 1){
				$prev_month = 12;
				$prev_year = $year - 1;
			} else {
				$prev_month = $month - 1;
				$prev_year = $year;
			}
			$filter_date_start = $prev_year.'-'.$prev_month.'-26';
			$filter_date_end = $year.'-'.$month.'-25';
			
			$template->data['month_of'] = date('F-Y', strtotime($filter_date_start));
			$template->data['filter_date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['filter_date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['days'] = $days;
			$template->data['filter_fields'] = $filter_fields;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['title'] = 'Monthly Muster Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/muster_html.tpl');
			// echo $html;exit;
			/*
			$filename = "Muster_Report.xls";
			$dpath = DIR_DOWNLOAD.$filename;

			@unlink($dpath);
			$handle = fopen($dpath, "w+");
			fwrite($handle, $html);
			fclose($handle);
			//echo 'sudo chmod -R 400 '.$dpath;exit;
			exec('sudo chmod -R 400 '.$dpath);
			
			$file = $dpath;
			$mask = '';

			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));

					readfile($file, 'rb');
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
			*/
			$filename = "Monthly_Muster_Report";			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>