<?php
class ControllerReportWorking extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$this->language->load('report/working');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = '0';
		}

		if (isset($this->request->get['grade'])) {
			$grade = html_entity_decode($this->request->get['grade']);
		} else {
			$grade = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/working', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group,
			'grade'			 	 	 => $grade,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$this->data['days'] = $day;

		$final_datas = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$working_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['division'] = $rvalue['division'];
				$final_datas[$rvalue['emp_code']]['region'] = $rvalue['region'];
				$final_datas[$rvalue['emp_code']]['unit'] = $rvalue['unit'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['grade'] = $rvalue['grade'];
				$transaction_inn = '0';
				$time_arr = array();
				$total_working_hours = '00:00:00';
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['working_time'] != '00:00:00'){
							$working_time = date('h:i', strtotime($tvalue['working_time']));
							$time_arr[] = $working_time;
						} else {
							$working_time = '00:00';
						}
						$working_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
							'working_time' => $working_time,
							'date' => $tvalue['date'],
						);

					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
				// echo '<pre>';
				// print_r($working_data);
				// exit;
				if($working_data){
					foreach ($day as $dkey => $dvalue) {
						foreach ($working_data as $pkey => $pvalue) {
							if(isset($pvalue['action'][$dvalue['date']]['date'])){
							} else {
								$working_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
									'working_time' => '00:00',
									'date' => $dvalue['date'],
								);			
							}
						}
					}
					$time = 0;
					$hours = 0;
					$minutes = 0;
					foreach ($time_arr as $time_val) {
						$times = explode(':', $time_val);
						$hours += $times[0];
						$minutes += $times[1];
					}
					$min_min = 0;
					$min_hours = 0;
					if($minutes > 0){
						$min_hours = floor($minutes / 60);
						$min_min = ($minutes % 60);
						$min_min = sprintf('%02d', $min_min);
					}
					$total_working_hours = ($hours + $min_hours).':'.$min_min;
				}
				$final_datas[$rvalue['emp_code']]['total_working_hours'] = $total_working_hours;
				// echo '<pre>';
				// print_r($working_data);
				// exit;
				foreach ($working_data as $pkey => $pvalue) {
					$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
					$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
				}
			}
			//exit;
		}

		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
  // 		print_r($this->data['final_datas']);
  // 		exit;
		$type_data = array(
			'1' => 'Default',
			'2' => 'Working',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			'6' => 'Manual'
		);
		$this->data['type_data'] = $type_data;


		$fields_data = array(
			'1' => 'Emp Id',
			'2' => 'Punch Id',
			'3' => 'Employee Name',
			'4' => 'Division',
			'5' => 'Region',
			'6' => 'Site',
			'7' => 'Department',
			'8' => 'Grade',
			'9' => 'Total Working Hours',
		);
		$this->data['fields_data'] = $fields_data;

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		//$grade_data['0'] = 'All';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];	
		}
		$this->data['grade_data'] = $grade_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		// echo '<pre>';
		// print_r($filter_fields);
		// exit;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_fields'] = $filter_fields;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['grade'] = explode(',', $grade);
		$this->data['company'] = explode(',', $company);
		$this->data['group'] = $group;

		$this->template = 'report/working.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/working');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['grade'])) {
			$grade = html_entity_decode($this->request->get['grade']);
		} else {
			$grade = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'group'					 => $group,
			'grade'			 	 	 => $grade,
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		$this->data['days'] = $day;
		$final_datas = array();
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$working_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
			$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
			$final_datas[$rvalue['emp_code']]['division'] = $rvalue['division'];
			$final_datas[$rvalue['emp_code']]['region'] = $rvalue['region'];
			$final_datas[$rvalue['emp_code']]['unit'] = $rvalue['unit'];
			$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
			$final_datas[$rvalue['emp_code']]['grade'] = $rvalue['grade'];
			$transaction_inn = '0';
			$time_arr = array();
			$total_working_hours = '00:00:00';
			if($transaction_datas){
				foreach($transaction_datas as $tkey => $tvalue){
					if($tvalue['working_time'] != '00:00:00'){
						$working_time = date('h:i', strtotime($tvalue['working_time']));
						$time_arr[] = $working_time;
					} else {
						$working_time = '00:00';
					}
					$working_data[$rvalue['emp_code']]['action'][$tvalue['date']] = array(
						'working_time' => $working_time,
						'date' => $tvalue['date'],
					);

				}
			} else {
				unset($final_datas[$rvalue['emp_code']]);
			}
			// echo '<pre>';
			// print_r($working_data);
			// exit;
			if($working_data){
				foreach ($day as $dkey => $dvalue) {
					foreach ($working_data as $pkey => $pvalue) {
						if(isset($pvalue['action'][$dvalue['date']]['date'])){
						} else {
							$working_data[$rvalue['emp_code']]['action'][$dvalue['date']] = array(
								'working_time' => '00:00',
								'date' => $dvalue['date'],
							);			
						}
					}
				}
				$time = 0;
				$hours = 0;
				$minutes = 0;
				foreach ($time_arr as $time_val) {
					$times = explode(':', $time_val);
					$hours += $times[0];
					$minutes += $times[1];
				}
				$min_min = 0;
				$min_hours = 0;
				if($minutes > 0){
					$min_hours = floor($minutes / 60);
					$min_min = ($minutes % 60);
					$min_min = sprintf('%02d', $min_min);
				}
				$total_working_hours = ($hours + $min_hours).':'.$min_min;
			}
			$final_datas[$rvalue['emp_code']]['total_working_hours'] = $total_working_hours;
			// echo '<pre>';
			// print_r($working_data);
			// exit;
			foreach ($working_data as $pkey => $pvalue) {
				$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
				$final_datas[$rvalue['emp_code']]['tran_data'] = $per_sort_data;
			}
		}
		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}
		$url .= '&once=1';
		//$final_datas = array_chunk($final_datas, 3);
		if($final_datas){
			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			if (isset($this->request->get['grade'])) {
				$grade_count = explode(',', $this->request->get['grade']);
				if(COUNT($grade_count) == 1){
					$grade_names = $this->db->query("SELECT `g_name` FROM `oc_grade` WHERE `grade_id` = '".$this->request->get['grade']."' ");
					if($grade_names->num_rows > 0){
						$grade_name = $grade_names->row['g_name'];
					} else {
						$grade_name = 'All';
					}
				} else {
					$grade_name = 'All';
				}
				$filter_grade = html_entity_decode($grade_name);
			} else {
				$filter_grade = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['month_of'] = date('F-Y', strtotime($filter_date_end));
			$template->data['date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['days'] = $day;
			$template->data['filter_fields'] = $filter_fields;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['filter_grade'] = $filter_grade;
			$template->data['title'] = 'Working Hours Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/working_html.tpl');
			//echo $html;exit;
			$filename = "Working_Report";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename.".html");
			// echo $html;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/working', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_name'])){
				$filter_department = $this->session->data['dept_name'];
			} else {
				$filter_department = null;
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>