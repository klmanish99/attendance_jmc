<?php
class ControllerReportLta extends Controller { 
	public function index() {  
		$this->language->load('report/lta');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/lta', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_year'	         => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$months = array(
			'1' => 'JAN',
			'2' => 'FEB',
			'3' => 'MAR',
			'4' => 'APR',
			'5' => 'MAY',
			'6' => 'JUN',
			'7' => 'JUL',
			'8' => 'AUG',
			'9' => 'SEP',
			'10' => 'OCT',
			'11' => 'NOV',
			'12' => 'DEC'
		);
		$this->data['months'] = $months;
		$final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$total_pl_cnt = 0;
				$total_cl_cnt = 0;
				$total_sl_cnt = 0;
				$total_pre_cnt = 0;
				$total_abs_cnt = 0;
				$total_hld_cnt = 0;
				$total_hd_cnt = 0;
				$total_wo_cnt = 0;
				$total_cof_cnt = 0;
				$total_od_cnt = 0;
				foreach($months as $mkey => $mvalue){
					$pl_cnt = 0;
					$cl_cnt = 0;
					$sl_cnt = 0;
					$pre_cnt = 0;
					$abs_cnt = 0;
					$hld_cnt = 0;
					$hd_cnt = 0;
					$wo_cnt = 0;
					$cof_cnt = 0;
					$od_cnt = 0;

					$cl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'CL');
					foreach($cl_count as $ckey => $tvalue){
						if($tvalue['leave_status'] == 1){
							if($tvalue['firsthalf_status'] == 'CL'){
								$cl_cnt ++;
							}
						} elseif($tvalue['leave_status'] == 0.5){
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								}
							}
						}
					}
					$pl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'PL');
					foreach($pl_count as $ckey => $tvalue){
						if($tvalue['leave_status'] == 1){
							if($tvalue['firsthalf_status'] == 'PL'){
								$pl_cnt ++;
							}
						} elseif($tvalue['leave_status'] == 0.5){
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								}
							}
						}
					}
					$sl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'SL');
					foreach($sl_count as $ckey => $tvalue){
						if($tvalue['leave_status'] == 1){
							if($tvalue['firsthalf_status'] == 'SL'){
								$sl_cnt ++;
							}
						} elseif($tvalue['leave_status'] == 0.5){
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							}
						}
					}
					$pre_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, '1');
					foreach($pre_count as $ckey => $tvalue){
						if($tvalue['present_status'] == 1){
							$pre_cnt ++;
						} elseif($tvalue['present_status'] == 0.5){
							$pre_cnt = $pre_cnt + 0.5;
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							}
						}
					}
					$abs_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, '0');
					foreach($abs_count as $ckey => $tvalue){
						if($tvalue['absent_status'] == 1){
							$abs_cnt ++;
						} elseif($tvalue['absent_status'] == 0.5){
							$abs_cnt = $abs_cnt + 0.5;
						}
					}
					$hld_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'HLD');
					$hd_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'HD');
					$wo_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'WO');
					$cof_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'COF');
					$od_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'OD');
					
					$total_pl_cnt = $total_pl_cnt + $pl_cnt;
					$total_sl_cnt = $total_sl_cnt + $sl_cnt;
					$total_cl_cnt = $total_cl_cnt + $cl_cnt;
					$total_pre_cnt = $total_pre_cnt + $pre_cnt;
					$total_abs_cnt = $total_abs_cnt + $abs_cnt;
					$total_hld_cnt = $total_hld_cnt + $hld_cnt;
					$total_hd_cnt = $total_hd_cnt + $hd_cnt;
					$total_wo_cnt = $total_wo_cnt + $wo_cnt;
					$total_cof_cnt = $total_cof_cnt + $cof_cnt;
					$total_od_cnt = $total_od_cnt + $od_cnt;

					$total = $pl_cnt + $sl_cnt + $cl_cnt + $pre_cnt + $abs_cnt + $hld_cnt + $hd_cnt + $wo_cnt + $cof_cnt + $od_cnt;

					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['pl_cnt'] = $pl_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['cl_cnt'] = $cl_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['sl_cnt'] = $sl_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['pre_cnt'] = $pre_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['abs_cnt'] = $abs_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['hld_cnt'] = $hld_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['hd_cnt'] = $hd_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['wo_cnt'] = $wo_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['cof_cnt'] = $cof_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['od_cnt'] = $od_cnt;
					$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['total_cnt'] = $total;
				}

				$total_total = $total_pl_cnt + $total_cl_cnt + $total_sl_cnt + $total_pre_cnt + $total_abs_cnt + $total_hld_cnt + $total_hd_cnt + $total_wo_cnt + $total_cof_cnt + $total_od_cnt;
				
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['g_pl_cnt'] = $total_pl_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_sl_cnt'] = $total_sl_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_cl_cnt'] = $total_cl_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_pre_cnt'] = $total_pre_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_abs_cnt'] = $total_abs_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_hld_cnt'] = $total_hld_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_hd_cnt'] = $total_hd_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_wo_cnt'] = $total_wo_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_cof_cnt'] = $total_cof_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_od_cnt'] = $total_od_cnt;
				$final_datas[$rvalue['emp_code']]['basic_data']['g_total'] = $total_total;
			}
			//exit;
		}
		//$final_datas = array();
		$this->data['final_datas'] = $final_datas;
		
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Moving' => 'Moving' 
		);

		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		$this->template = 'report/lta.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function date_compare($a, $b){
	    $t1 = strtotime($a['date']);
	    $t2 = strtotime($b['date']);
	    return $t1 - $t2;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/lta');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$months = array(
			'1' => 'JAN',
			'2' => 'FEB',
			'3' => 'MAR',
			'4' => 'APR',
			'5' => 'MAY',
			'6' => 'JUN',
			'7' => 'JUL',
			'8' => 'AUG',
			'9' => 'SEP',
			'10' => 'OCT',
			'11' => 'NOV',
			'12' => 'DEC'
		);

		
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$total_pl_cnt = 0;
			$total_cl_cnt = 0;
			$total_sl_cnt = 0;
			$total_pre_cnt = 0;
			$total_abs_cnt = 0;
			$total_hld_cnt = 0;
			$total_hd_cnt = 0;
			$total_wo_cnt = 0;
			$total_cof_cnt = 0;
			$total_od_cnt = 0;
			foreach($months as $mkey => $mvalue){
				$pl_cnt = 0;
				$cl_cnt = 0;
				$sl_cnt = 0;
				$pre_cnt = 0;
				$abs_cnt = 0;
				$hld_cnt = 0;
				$hd_cnt = 0;
				$wo_cnt = 0;
				$cof_cnt = 0;
				$od_cnt = 0;

				$cl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'CL');
				foreach($cl_count as $ckey => $tvalue){
					if($tvalue['leave_status'] == 1){
						if($tvalue['firsthalf_status'] == 'CL'){
							$cl_cnt ++;
						}
					} elseif($tvalue['leave_status'] == 0.5){
						if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
							if($tvalue['firsthalf_status'] == 'CL'){
								$cl_cnt = $cl_cnt + 0.5;
							}
						} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
							if($tvalue['secondhalf_status'] == 'CL'){
								$cl_cnt = $cl_cnt + 0.5;
							}
						}
					}
				}
				$pl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'PL');
				foreach($pl_count as $ckey => $tvalue){
					if($tvalue['leave_status'] == 1){
						if($tvalue['firsthalf_status'] == 'PL'){
							$pl_cnt ++;
						}
					} elseif($tvalue['leave_status'] == 0.5){
						if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
							if($tvalue['firsthalf_status'] == 'PL'){
								$pl_cnt = $pl_cnt + 0.5;
							}
						} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
							if($tvalue['secondhalf_status'] == 'PL'){
								$pl_cnt = $pl_cnt + 0.5;
							}
						}
					}
				}
				$sl_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, 'SL');
				foreach($sl_count as $ckey => $tvalue){
					if($tvalue['leave_status'] == 1){
						if($tvalue['firsthalf_status'] == 'SL'){
							$sl_cnt ++;
						}
					} elseif($tvalue['leave_status'] == 0.5){
						if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
							if($tvalue['firsthalf_status'] == 'SL'){
								$sl_cnt = $sl_cnt + 0.5;
							}
						} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
							if($tvalue['secondhalf_status'] == 'SL'){
								$sl_cnt = $sl_cnt + 0.5;
							}
						}
					}
				}
				$pre_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, '1');
				foreach($pre_count as $ckey => $tvalue){
					if($tvalue['present_status'] == 1){
						$pre_cnt ++;
					} elseif($tvalue['present_status'] == 0.5){
						$pre_cnt = $pre_cnt + 0.5;
						if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
							if($tvalue['firsthalf_status'] == 'SL'){
								$sl_cnt = $sl_cnt + 0.5;
							}
						} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
							if($tvalue['secondhalf_status'] == 'SL'){
								$sl_cnt = $sl_cnt + 0.5;
							}
						}
					}
				}
				$abs_count = $this->model_report_common_report->getleav_data($rvalue['emp_code'], $mkey, $filter_year, '0');
				foreach($abs_count as $ckey => $tvalue){
					if($tvalue['absent_status'] == 1){
						$abs_cnt ++;
					} elseif($tvalue['absent_status'] == 0.5){
						$abs_cnt = $abs_cnt + 0.5;
					}
				}
				$hld_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'HLD');
				$hd_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'HD');
				$wo_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'WO');
				$cof_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'COF');
				$od_cnt = $this->model_report_common_report->getsum_data($rvalue['emp_code'], $mkey, $filter_year, 'OD');
				
				$total_pl_cnt = $total_pl_cnt + $pl_cnt;
				$total_sl_cnt = $total_sl_cnt + $sl_cnt;
				$total_cl_cnt = $total_cl_cnt + $cl_cnt;
				$total_pre_cnt = $total_pre_cnt + $pre_cnt;
				$total_abs_cnt = $total_abs_cnt + $abs_cnt;
				$total_hld_cnt = $total_hld_cnt + $hld_cnt;
				$total_hd_cnt = $total_hd_cnt + $hd_cnt;
				$total_wo_cnt = $total_wo_cnt + $wo_cnt;
				$total_cof_cnt = $total_cof_cnt + $cof_cnt;
				$total_od_cnt = $total_od_cnt + $od_cnt;

				$total = $pl_cnt + $sl_cnt + $cl_cnt + $pre_cnt + $abs_cnt + $hld_cnt + $hd_cnt + $wo_cnt + $cof_cnt + $od_cnt;

				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['pl_cnt'] = $pl_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['cl_cnt'] = $cl_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['sl_cnt'] = $sl_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['pre_cnt'] = $pre_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['abs_cnt'] = $abs_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['hld_cnt'] = $hld_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['hd_cnt'] = $hd_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['wo_cnt'] = $wo_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['cof_cnt'] = $cof_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['od_cnt'] = $od_cnt;
				$final_datas[$rvalue['emp_code']][$mkey]['trans_data']['total_cnt'] = $total;
			}

			$total_total = $total_pl_cnt + $total_cl_cnt + $total_sl_cnt + $total_pre_cnt + $total_abs_cnt + $total_hld_cnt + $total_hd_cnt + $total_wo_cnt + $total_cof_cnt + $total_od_cnt;
			
			$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
			$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
			$final_datas[$rvalue['emp_code']]['basic_data']['g_pl_cnt'] = $total_pl_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_sl_cnt'] = $total_sl_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_cl_cnt'] = $total_cl_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_pre_cnt'] = $total_pre_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_abs_cnt'] = $total_abs_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_hld_cnt'] = $total_hld_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_hd_cnt'] = $total_hd_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_wo_cnt'] = $total_wo_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_cof_cnt'] = $total_cof_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_od_cnt'] = $total_od_cnt;
			$final_datas[$rvalue['emp_code']]['basic_data']['g_total'] = $total_total;
		}

		//$final_datas = array();
		//$final_datass = array_chunk($final_datas, 3);

		// echo '<pre>';
		// print_r($final_datass);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Yearly Leave Summary';
			$template->data['months'] = $months;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/lta_html.tpl');
			//echo $html;exit;
			$filename = "Yearly_Leave_Summary";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/lta', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>