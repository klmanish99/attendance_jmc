<?php
class ControllerReportToday extends Controller { 
	public function index() {  
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '0';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['grade'])) {
			$grade = html_entity_decode($this->request->get['grade']);
		} else {
			$grade = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = $this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'grade'			 	 	 => $grade,
			'company'			 	 => $company,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$results = $this->model_report_attendance->getTodaysAttendance_new($data);
		/*
		foreach($results as $rkey => $rvalue){
			$data['filter_name_id'] = $rvalue['emp_id'];
			$emp_datas = $this->model_report_attendance->getemployees_1($rvalue['emp_id']);
			$results[$rkey]['grade'] = $emp_datas['grade'];
		}
		*/
		$this->data['results'] = $results;
		
		// $locations = $this->db->query("SELECT * FROM `oc_unit` ")->rows;
		// $unit_data['0'] = 'All'; 
		// foreach($locations as $lkey => $lvalue){
		// 	$unit_data[$lvalue['unit']] = $lvalue['unit'];
		// }
		// $this->data['unit_data'] = $unit_data;

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		//$grade_data['0'] = 'All';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];	
		}
		$this->data['grade_data'] = $grade_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['site_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;	


		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);
		$this->data['grade'] = explode(',', $grade);
		$this->data['group'] = $group;

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'report/today.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function export(){
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		$filter_date_start = date('Y-m-d');
		//$filter_date_start = '2016-04-09';
		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['grade'])) {
			$grade = html_entity_decode($this->request->get['grade']);
		} else {
			$grade = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'grade'			 	 	 => $grade,
			'company'			 	 => $company,
			'group'					 => $group 
		);

		$final_datas = array();
		$final_datas = $this->model_report_attendance->getTodaysAttendance_new($data);
		
		$url = '';
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['grade'])) {
			$url .= '&grade=' . $this->request->get['grade'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		$url .= '&once=1';

		$final_datas = array_chunk($final_datas, 15000);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		if($department){
			$department = $department;
		} else {
			$department = 'All';
		}
		
		if($final_datas){
			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = 'All';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = 'All';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = 'All';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = 'All';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			if (isset($this->request->get['grade'])) {
				$grade_count = explode(',', $this->request->get['grade']);
				if(COUNT($grade_count) == 1){
					$grade_names = $this->db->query("SELECT `g_name` FROM `oc_grade` WHERE `grade_id` = '".$this->request->get['grade']."' ");
					if($grade_names->num_rows > 0){
						$grade_name = $grade_names->row['g_name'];
					} else {
						$grade_name = 'All';
					}
				} else {
					$grade_name = 'All';
				}
				$filter_grade = html_entity_decode($grade_name);
			} else {
				$filter_grade = 'All';
			}
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			//$tdays = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['filter_grade'] = $filter_grade;
			$template->data['title'] = 'Todays Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/today_html.tpl');
			//echo $html;exit;
			//$filename = "Todays_Attendance_".$department.".html";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			$filename = "Todays_Attendance_".$department;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>
