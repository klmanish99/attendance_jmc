<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/backup.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <form action="<?php echo $restore; ?>" method="post" enctype="multipart/form-data" id="restore">
          <input type="file" name="import_data" id="import_data" accept="csv" />
          <a onclick="$('#restore').submit();" class="button"><?php echo 'Upload and Import Employee'; ?></a>
        </form>
      </div>
    </div>
    <div class="content">
      <?php if($error_list == '1'){ ?>
        <table class="list">
          <thead>
            <tr>
              <td>
                <a><?php echo "Line Number"; ?></a>
              </td>
              <td>
                <a><?php echo "Reason"; ?></a>
              </td>
            </tr>
          </thead>
          <tbody>
            <?php if($employees) { ?>
              <?php foreach ($employees as $values) { ?>
                <?php
                  $total_count = count($values) - 1;
                ?>
                <?php foreach($values as $key => $value){ ?>
                  <?php if($total_count == $key){ ?>
                    <tr>
                      <td class="left"><?php echo $value['line_number']; ?></td>
                      <td class="left"><?php echo $value['reason']; ?></td>
                    </tr>
                  <?php } else { ?>
                    <tr>
                      <td class="left"><?php echo $value['line_number']; ?></td>
                      <td class="left"><?php echo $value['reason']; ?></td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      <?php }  ?>
    </div>
  </div>
</div>
<?php echo $footer; ?>