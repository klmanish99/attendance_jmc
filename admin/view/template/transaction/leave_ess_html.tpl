<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : ' . date('d-m-Y H:i:s'); ?></p> <br />
    <?php if($is_dept == '1'){ ?>
      <span style="font-size: 13px;">Department</span> : <b style="font-size: 14px;"><?php echo $dept_name; ?></b>
    <?php } ?>
  </h1>
  <table class="list product">
    <thead>
      <tr>
        <td class="left" style="">
          <?php echo 'Emp Name'; ?>
        </td>
        <td class="left">
          <?php echo 'Emp Id'; ?>
        </td>
        <?php if($is_super == '1'){ ?>
        <td class="left">
          <?php echo 'Department'; ?>
        </td>
        <?php } ?>
        <td class="left">
          <?php echo 'Group'; ?>
        </td>
        <td class="left">
          <?php echo 'Dte of input'; ?>
        </td>
        <td class="left">
          <?php echo 'Lve Tpe'; ?>
        </td>
        <td>
          <a><?php echo "Days"; ?></a>
        </td>
        <td>
          <a><?php echo "Encash"; ?></a>
        </td>
        <td style="width:55px;">
          <a><?php echo "From"; ?></a>
        </td>
        <td style="width:55px;">
          <a><?php echo "To"; ?></a>
        </td>
        <td>
          <a><?php echo "Leave Reason"; ?></a>
        </td>
        <td>
          <a><?php echo "Dept.Appr"; ?></a>
        </td>
        <td>
          <a><?php echo "Dept Appr by"; ?></a>
        </td>
        <td>
          <a><?php echo "Secy. Appr"; ?></a>
        </td>
        <td>
          <a><?php echo "Admin Reject Status"; ?></a>
        </td>
        <td>
          <a><?php echo "Process"; ?></a>
        </td>
      </tr>
    </thead>
    <tbody>
      <?php if ($leaves) { ?>
        <?php foreach ($leaves as $employee) { ?>
          <tr>
            <td class="left"><?php echo $employee['name']; ?></td>
            <td class="left"><?php echo $employee['emp_id']; ?></td>
            <?php if($is_super == '1'){ ?>
              <td class="left"><?php echo $employee['dept']; ?></td>
            <?php } ?>
            <td class="left"><?php echo $employee['group']; ?></td>
            <td class="left"><?php echo $employee['dot']; ?></td>
            <td class="left"><?php echo $employee['leave_type']; ?></td>
            <td class="left"><?php echo $employee['total_leave_days']; ?></td>
            <td class="left"><?php echo $employee['encash']; ?></td>
            <td class="left"><?php echo $employee['leave_from']; ?></td>
            <td class="left"><?php echo $employee['leave_to']; ?></td>
            <td class="left"><?php echo $employee['leave_reason']; ?></td>
            <td class="left"><?php echo $employee['approval_1']; ?></td>
            <td class="left"><?php echo $employee['approval_1_by']; ?></td>
            <td class="left"><?php echo $employee['approval_2']; ?></td>
            <td class="left"><?php echo $employee['reject_reason']; ?></td>
            <td class="left"><?php echo $employee['proc_stat']; ?></td>
          </tr>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>