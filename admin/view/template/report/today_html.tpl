<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php $i = 1; ?>
<?php foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
		<?php echo $filter_company; ?><br />
		<?php echo $title; ?><br />
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
			<?php echo 'Date : '. $date_start; ?><br />
		</span>
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
			<?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Grade : '. $filter_grade; ?><br />
		</span>
		<span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
	</h1>
	<table class="product" style="width:100% !important;">
		<tbody>
			<tr class="">
				<td class="center"><?php echo 'Sr.No'; ?></td>
				<td class="center"><?php echo 'Emp Code'; ?></td>
				<td class="center"><?php echo 'Name'; ?></td>
				<td class="center"><?php echo 'Region'; ?></td>
				<td class="center"><?php echo 'Division'; ?></td>
				<td class="center"><?php echo 'Site'; ?></td>
				<td class="center"><?php echo 'Department'; ?></td>
				<td class="center"><?php echo 'Grade'; ?></td>
				<td class="center"><?php echo 'Shift Assigned'; ?></td>
				<td class="center"><?php echo 'Shift Attended'; ?></td>
				<td class="center"><?php echo 'Late time'; ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="center">In</td>
				<td class="center">In</td>
				<td>&nbsp;</td>
			</tr>
			<?php if($final_datas) { ?>
				<?php foreach($final_datas as $final_data) { ?>
					<tr>
						<td class="left" style="font-size:11px;"><?php echo $i; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['emp_id']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['emp_name']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['region']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['division']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['unit']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['dept']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['grade']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['shift_intime']; ?></td>
						<td class="left" style="font-size:11px;"><?php echo $final_data['punch_time']; ?></td>
						<td class="left" style="font-size:11px;">
							<?php if($final_data['late_time'] != '00:00:00') { ?>
								<span style="background-color:red;">
									<?php echo $final_data['late_time']; ?>
								</span>
							<?php } else { ?>
								<?php echo $final_data['late_time']; ?>
							<?php } ?>
						</td>
					</tr>
					<?php $i++; ?>
				<?php } ?>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php } ?>
</body>
</html>