<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
	$part = explode('/', $this->request->get['route']);

	if (isset($part[0])) {
		$route .= $part[0];
	}

	if (isset($part[1])) {
		$route .= '/' . $part[1];
	}
}
?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/report.png" alt="" /> <?php echo "Today's Attendance Report"; ?></h1>
		</div>
		<div class="content sales-report">
			<table class="form">
				<tr>
					<td style="width:10%;"><?php echo "Name / Emp Code"; ?>
						<?php if(isset($this->session->data['emp_code'])) { ?>
							<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
						<?php } else { ?>
							<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
						<?php } ?>
						<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
					</td>
					<td style="width:6%">Company
						<select multiple name="company[]" id="input-company" style="display:inline;">
							<?php foreach($company_data as $key => $ud) { ?>
								<?php if (in_array($key, $company)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Region
						<select name="region" id="region">
							<?php foreach($region_data as $key => $ud) { ?>
								<?php if($key == $region) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Division
						<select name="division" id="division">
							<?php foreach($division_data as $key => $ud) { ?>
								<?php if($key == $division) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Location
						<select name="unit" id="unit">
							<?php foreach($site_data as $key => $ud) { ?>
								<?php if($key == $unit) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Department
						<select name="department" id="department">
							<?php foreach($department_data as $key => $dd) { ?>
								<?php if($key == $department) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Grade
						<select multiple name="grade[]" id="grade" style="width:100%;display:inline;">
							<?php foreach($grade_data as $key => $dd) { ?>
								<?php if (in_array($key, $grade)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="text-align: right;">
						<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
						<?php if($this->user->hasPermission('modify', $route)){ ?>
							<a style="padding: 13px 25px;" href="<?php echo $generate_today; ?>" class="button"><?php echo "Refresh"; ?></a>
						<?php } ?>
						<?php if($this->user->hasPermission('modify', $route)){ ?>
							<a style="padding: 13px 25px;" href="<?php echo $export; ?>" class="button"><?php echo "Export"; ?></a>
						<?php } ?>
					</td>
				</tr>
			</table>
			<div style="width: 1459px;overflow-x: scroll;overflow-y: hidden;">
				<table class="list" style="width: 100%;">
					<thead>
						<tr>
							<td class="center"><?php echo 'Sr.No'; ?></td>
							<td class="center"><?php echo 'Emp Code'; ?></td>
							<td class="center"><?php echo 'Name'; ?></td>
							<td class="center"><?php echo 'Region'; ?></td>
							<td class="center"><?php echo 'Division'; ?></td>
							<td class="center"><?php echo 'Site'; ?></td>
							<td class="center"><?php echo 'Department'; ?></td>
							<td class="center"><?php echo 'Grade'; ?></td>
							<td class="center"><?php echo 'Shift Assigned'; ?></td>
							<td class="center"><?php echo 'Shift Attended'; ?></td>
							<td class="center"><?php echo 'Late time'; ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class="center">In</td>
							<?php /* ?>
							<td class="center">Out</td>
							<?php */ ?>
							<td class="center">In</td>
							<?php /* ?>
							<td class="center">Out</td>
							<?php */ ?>
							<td>&nbsp;</td>
							
						</tr>
					</thead>
					<tbody>
						<?php if($results) { ?>
							<?php $i = 1; ?>
							<?php foreach($results as $result) { ?>
								<tr>
									<td>
										<?php echo $i; ?>
									</td>
									<td>
										<?php echo $result['emp_id']; ?>
									</td>
									<td>
										<?php echo $result['emp_name']; ?>
									</td>
									<td>
										<?php echo $result['region']; ?>
									</td>
									<td>
										<?php echo $result['division']; ?>
									</td>
									<td>
										<?php echo $result['unit']; ?>
									</td>
									<td>
										<?php echo $result['dept']; ?>
									</td>
									<td>
										<?php echo $result['grade']; ?>
									</td>
									<td>
										<?php /* if($result['shift_intime'] != '00:00:00') { ?>
											<?php echo $result['shift_intime']; ?>
										<?php } elseif($result['weekly_off'] != '0') { ?>
											<?php echo 'Weekly Off'; ?>
										<?php } elseif($result['holiday_id'] != '0') { ?>
											<?php echo 'Holiday'; ?>
										<?php } else { ?>
											<?php echo $result['shift_intime']; ?>
										<?php } */ ?>
										<?php echo $result['shift_intime']; ?>
									</td>
									<td>
										<?php echo $result['punch_time']; ?>
									</td>
									<td>
										<?php if($result['late_time'] != '00:00:00') { ?>
											<span style="background-color:red;">
												<?php echo $result['late_time']; ?>
											</span>
										<?php } else { ?>
											<?php echo $result['late_time']; ?>
										<?php } ?>
									</td>
								</tr>
								<?php $i++; ?>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="6"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/today&token=<?php echo $token; ?>';
	
	var filter_name = $('#filter_name').val();
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
		var filter_name_id = $('#filter_name_id').val();
		if (filter_name_id) {
			url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
		} else {
			alert('Please Enter Correct Employee Name');
			return false;
		}
	}

	var unit = $('#unit').val();
	if (unit && unit != '0') {
		url += '&unit=' + encodeURIComponent(unit);
	}

	var department = $('#department').val();
	if (department && department != '0') {
		url += '&department=' + encodeURIComponent(department);
	}

	var division = $('#division').val();
	if (division && division != '0') {
		url += '&division=' + encodeURIComponent(division);
	}

	var region = $('#region').val();
	if (region && region != '0') {
		url += '&region=' + encodeURIComponent(region);
	}

	grade_selected = $('#grade').val();
	field_ids = '';
	if(grade_selected){
		max_length = grade_selected.length - 1;
		for(i=0; i<grade_selected.length; i++){
			if(max_length == i){  
				field_ids += grade_selected[i];
			} else {
				field_ids += grade_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&grade=' + encodeURIComponent(field_ids);
		}
	}
	
	company_selected = $('#input-company').val();
	field_ids = '';
	if(company_selected){
		max_length = company_selected.length - 1;
		for(i=0; i<company_selected.length; i++){
			if(max_length == i){  
				field_ids += company_selected[i];
			} else {
				field_ids += company_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&company=' + encodeURIComponent(field_ids);
		}
	} 
	
	location = url;
	return false;
}

$(document).ready(function() {
	$('#input-company, #grade').multiselect({
		includeSelectAllOption: true,
		enableFiltering: true,
		maxHeight: 235,
	});
});

$('input[name=\'filter_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/daily/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.emp_code
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
		$('input[name=\'filter_name_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('#region').on('change', function() {
	region = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
		dataType: 'json',
		success: function(json) {   
			$('#division').find('option').remove();
			if(json['division_datas']){
				$.each(json['division_datas'], function (i, item) {
					$('#division').append($('<option>', { 
							value: item.division_id,
							text : item.division 
					}));
				});
			}
			$('#unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#unit').append($('<option>', { 
							value: item.unit_id,
							text : item.unit 
					}));
				});
			}
		}
	});
});

$('#division').on('change', function() {
	division = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
		dataType: 'json',
		success: function(json) {   
			$('#unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#unit').append($('<option>', { 
							value: item.unit_id,
							text : item.unit 
					}));
				});
			}
		}
	});
});

//--></script> 
<?php echo $footer; ?>