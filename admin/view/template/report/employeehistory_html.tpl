<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $filter_company; ?><br />
    <?php echo $title; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Period : '. $date_start . ' - ' . $date_end; ?><br />
    </span>
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  </h1>
  <table class="product" style="width:100% !important;">
    <?php $count = 0; ?>
    <?php if($final_datas) { ?>
    <thead>
      <tr>
        <td style="padding: 0px 9px;font-size: 11px">Employee Id</td>
        <td style="padding: 0px 9px;font-size: 11px">Employee Name</td>
        <td style="padding: 0px 9px;font-size: 11px">DOJ</td>
        <td style="padding: 0px 9px;font-size: 11px">REGION</td>
        <td style="padding: 0px 9px;font-size: 11px">DIVISION</td>
        <td style="padding: 0px 9px;font-size: 11px">SITE</td>
        <td style="padding: 0px 9px;font-size: 11px">DESIGNATION</td>
        <td style="padding: 0px 9px;font-size: 11px">GRADE</td>
        <td style="padding: 0px 9px;font-size: 11px">DEPARTMENT</td>
        <td style="padding: 0px 9px;font-size: 11px">EMPLOYMENT</td>
        <td style="padding: 0px 9px;font-size: 11px">COMPANY</td>
        <td style="padding: 0px 9px;font-size: 11px">TRANSFER DATE</td>
        <td style="padding: 0px 9px;font-size: 11px">REMARKS</td>
      </tr>
    </thead>
    <?php } ?>
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_dat) { ?>
          <tr>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['emp_id'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['emp_name'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['doj'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['region'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['division'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['unit'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['designation'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['grade'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['department'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['employement'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['company'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['date'] ?></td>
            <td style="padding: 0px 9px;font-size: 10px"><?php echo $final_dat['remarks'] ?></td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan = "13"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div></body></html>