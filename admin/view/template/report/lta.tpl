<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="15" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:10%">Year
            <select name="filter_year">
              <?php for($i=2015;$i<=2020;$i++) { ?>
                <?php if($filter_year == $i) { ?>
                  <option value='<?php echo $i; ?>' selected="selected"><?php echo $i; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $i; ?>'><?php echo $i; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:10%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:8%">Department
            <select name="department" style="width:100%;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key === $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>

          <td style="width:10%;display: none;">Group
            <select name="group">
              <?php foreach($group_data as $key => $gd) { ?>
                <?php if($key == $group) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
    
          <td style="text-align: right;">
            <a style="padding: 5px 5px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 5px 5px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          <?php if($final_datas) { ?>
            <tr>
              <td style="font-weight:bold;font-size:12px;">
                Status
              </td>
              <?php foreach($months as $mkey => $mvalue) { ?>
              <td style="font-weight:bold;font-size:12px;">
                <?php echo $mvalue; ?>
              </td>
              <?php } ?>
              <td style="font-weight:bold;font-size:12px;">
                TOTAL
              </td>
            </tr>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr style="font-weight:bold;font-size:12px;">
                <td colspan = "13">
                  <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  CL
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['cl_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_cl_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  PL
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['pl_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_pl_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  SL
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['sl_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_sl_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  PRE
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['pre_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_pre_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  ABS
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['abs_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_abs_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  HLD
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['hld_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_hld_cnt']; ?>
                </td>
              </tr>
              <tr style="display:none;">
                <td style="padding: 0px 9px;font-size:11px;">
                  HD
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['hd_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_hd_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  WO
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['wo_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_wo_cnt']; ?>
                </td>
              </tr>
              <tr style="display:none;">
                <td style="padding: 0px 9px;font-size:11px;">
                  COF
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['cof_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_cof_cnt']; ?>
                </td>
              </tr>
              <tr style="display:none;">
                <td style="padding: 0px 9px;font-size:11px;">
                  OD
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['od_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_od_cnt']; ?>
                </td>
              </tr>
              <tr>
                <td style="padding: 0px 9px;font-size:11px;">
                  TOTAL
                </td>
                <?php foreach($months as $mkey => $mvalue) { ?>
                  <?php foreach($final_data[$mkey] as $fkey => $fvalue) { ?>
                    <td style="padding: 0px 9px;font-size:11px;">
                      <?php echo $fvalue['total_cnt']; ?>
                    </td>
                  <?php } ?>
                <?php } ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $final_data['basic_data']['g_total']; ?>
                </td>
              </tr>
              <tr style="border-bottom:2px solid black;">
                <td colspan = "14" >
                </td>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/lta&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
  
  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/lta/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
  
  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  location = url;
  return false;
}



//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?>