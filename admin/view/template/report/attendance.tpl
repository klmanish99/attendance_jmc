<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:10%;"><?php echo "Date"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" />
          </td>
          <td style="width:8%">Division
            <select name="division" id="division">
              <?php foreach($division_data as $key => $ud) { ?>
                <?php if($key == $division) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:10%">Region
            <select name="region" id="region">
              <?php foreach($region_data as $key => $ud) { ?>
                <?php if($key == $region) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:10%">Site
            <select name="unit" id="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:13%">Department
            <select name="department" id="department">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key == $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:8%">Status
            <select name="status" id="status">
              <?php foreach($statuses as $skey => $svalue) { ?>
                <?php if($skey == $status) { ?>
                  <option value='<?php echo $skey; ?>' selected="selected"><?php echo $svalue; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $skey; ?>'><?php echo $svalue; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:6%">Fields
            <select multiple name="filter_fields[]" id="input-filter_fields" style="display:inline;">
              <?php foreach($fields_data as $tkey => $tvalue) { ?>
                <?php if (in_array($tkey, $filter_fields)) { ?>
                  <option value='<?php echo $tkey; ?>' selected="selected"><?php echo $tvalue; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $tkey; ?>'><?php echo $tvalue; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;display: none;" href="<?php echo $generate; ?>" id="filter_refresh" class="button"><?php echo 'Refresh'; ?></a>
            <?php if($this->user->hasPermission('modify', $route)){ ?>   
              <a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
            <?php } ?>
          </td>
        </tr>
      </table>
      <table class="list">
        <thead>
          <tr>
            <td class="center"><?php echo 'Sr.No'; ?></td>
            <td class="center"><?php echo 'Emp Code'; ?></td>
            <td class="center"><?php echo 'Name'; ?></td>
            <td class="center" colspan='2'><?php echo 'Shift Assigned'; ?></td>
            <td class="center" colspan='4'><?php echo 'Shift Attended'; ?></td>
            <td class="center"><?php echo 'Working Hours'; ?></td>
            <td class="center"><?php echo 'Late time'; ?></td>
            <td class="center"><?php echo 'Early time'; ?></td>
            <?php if($this->user->getId() == 111111111){ ?>
              <td class="center"><?php echo 'Action'; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="center">In</td>
            <td class="center">Out</td>
            <td class="center" style="width:8%;">In Date</td>
            <td class="center">In Time</td>
            <td class="center" style="width:8%;">Out Date</td>
            <td class="center">Out TIme</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            <?php if($this->user->getId() == 111111111){ ?>
              <td>&nbsp;</td>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php if($results) { ?>
            <?php $i = 1; ?>
            <?php foreach($results as $result) { ?>
              <tr>
                <td>
                  <?php echo $i; ?>
                </td>
                <td>
                  <?php echo $result['emp_id']; ?>
                </td>
                <td>
                  <?php echo $result['emp_name']; ?>
                </td>
                <td>
                  <?php if($result['leave_status'] == 1) { ?>
                    <?php echo $result['firsthalf_status']; ?>
                  <?php } elseif($result['weekly_off'] != '0') { ?>
                    <?php echo 'Weekly Off'; ?>
                  <?php } elseif($result['holiday_id'] != '0') { ?>
                    <?php echo 'Holiday'; ?>
                  <?php } else { ?>
                    <?php echo $result['shift_intime']; ?>
                  <?php } ?>
                </td>
                <td>
                  <?php if($result['leave_status'] == 1) { ?>
                    <?php echo $result['secondhalf_status']; ?>
                  <?php } elseif($result['weekly_off'] != '0') { ?>
                    <?php echo 'Weekly Off'; ?>
                  <?php } elseif($result['holiday_id'] != '0') { ?>
                    <?php echo 'Holiday'; ?>
                  <?php else { ?>
                    <?php echo $result['shift_outtime']; ?>
                  <?php } ?>
                </td>
                <td>
                  <?php echo $result['date']; ?>
                </td>
                <td>
                  <?php echo $result['act_intime']; ?>
                </td>
                <td>
                  <?php echo $result['date_out']; ?>
                </td>
                <td>
                  <?php echo $result['act_outtime']; ?>
                </td>
                <td>
                  <?php echo $result['date']; ?>
                </td>
                <td>
                  <?php echo $result['act_intime']; ?>
                </td>
                <td>
                  <?php echo $result['date_out']; ?>
                </td>
                <td>
                  <?php echo $result['act_outtime']; ?>
                </td>
                <td>
                  <?php if($result['working_time'] == '08:00:00') { ?>
                    <span style="background-color:#13F113;">
                      <?php echo $result['working_time']; ?>
                    </span>
                  <?php } else { ?>
                      <?php echo $result['working_time']; ?>
                  <?php } ?> 
                </td>
                <td>
                  <?php if($result['late_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                      <?php echo $result['late_time']; ?>
                    </span>
                  <?php } else { ?>
                    <?php echo $result['late_time']; ?>
                  <?php } ?>
                </td>
                <td>
                  <?php if($result['early_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                      <?php echo $result['early_time']; ?>
                    </span>
                  <?php } else { ?>
                    <?php echo $result['early_time']; ?>
                  <?php } ?>
                </td>
                <?php if($this->user->getId() == 1111111111){ ?>
                  <td>
                    <a href="<?php echo $result['remove_href']; ?>">Delete</a>
                  </td>
                <?php } ?>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="13"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/attendance&token=<?php echo $token; ?>';
  
  var filter_date_start = $('#date-start').val();
  //alert(filter_date_start);
  //return false;
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var unit = $('#unit').val();
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('#department').val();
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var division = $('#division').val();
  if (division) {
    url += '&division=' + encodeURIComponent(division);
  }

  var region = $('#region').val();
  if (region) {
    url += '&region=' + encodeURIComponent(region);
  }

  var group = $('#group').val();
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var status = $('#status').val();
  if (status) {
    url += '&status=' + encodeURIComponent(status);
  }  

  filter_fields_selected = $('#input-filter_fields').val();
  field_ids = '';
  if(filter_fields_selected){
    max_length = filter_fields_selected.length - 1;
    for(i=0; i<filter_fields_selected.length; i++){
      if(max_length == i){  
        field_ids += filter_fields_selected[i];
      } else {
        field_ids += filter_fields_selected[i]+',';
      }
    }
    if (field_ids) {
      url += '&filter_fields=' + encodeURIComponent(field_ids);
    }
  }
  
  location = url;
  return false;
}

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})()

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#input-filter_fields').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  /*
  $('#date-start').datetimepicker({
    pickTime: false,
    format: 'YYYY-MM-DD'
  });
  */
  $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<?php echo $footer; ?>