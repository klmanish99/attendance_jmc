<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:1%">Year
            <select name="filter_year" id="filter_year" style="width: 70px;">
              <?php foreach($years as $key => $ud) { ?>
                <?php if($key == $filter_year) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:10%;"><?php echo "Name / Emp Code"; ?>
            <?php if(isset($this->session->data['emp_code'])) { ?>
              <input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
            <?php } else { ?>
              <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
            <?php } ?>
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:6%">Company
            <select multiple name="company[]" id="input-company" style="display:inline;">
              <?php foreach($company_data as $key => $ud) { ?>
                <?php if (in_array($key, $company)) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:6%">Region
            <select name="region" id="region">
              <?php foreach($region_data as $key => $ud) { ?>
                <?php if($key == $region) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:6%">Division
            <select name="division" id="division">
              <?php foreach($division_data as $key => $ud) { ?>
                <?php if($key == $division) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php if($user_dept == '0' || $is_dept == '1') { ?>
          <td style="width:6%">Site
            <select name="unit" id="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <?php if($user_dept == '0') { ?>
          <td style="width:6%">Department
            <select name="department" id="department" style="width:100%;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key == $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <div style="width: 1459px;overflow-x: scroll;overflow-y: hidden;">
        <table class="list" style="width: 80%;">
          <?php if($final_datas) { ?>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr>
                <td class="left" style="width:20%;font-weight:bold;font-size:12px;">Employee Id : <?php echo $final_data['basic_data']['emp_code']; ?> &nbsp;-&nbsp; <?php echo $final_data['basic_data']['name']; ?></td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">Opening Balance</td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">PL Credit</td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">PL Availed</td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;text-align: right;">Balance</td>
              </tr>
              <?php /* ?>
              <tr style="font-weight:bold;font-size:11px;">
                <td colspan = "5">
                  <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
                </td>
              </tr>
              <?php */ ?>
              <tr>
                <td class="left">
                  Opening Balance
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
                  <?php echo $final_data['basic_data']['pl_open']; ?>
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
                <td>
                  &nbsp;
                </td>
              </tr>
              <?php 
                $total_pl_credit = 0;
                $total_pl_availed = 0;
              ?>
              <?php if(isset($final_data['tran_data']['action'])) { ?>
                <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
                  <tr>
                    <td style="padding: 0px 9px;font-size:11px;text-align: left;">
                      <?php echo $tvalue['month_name']; ?>
                    </td>
                    <td style="padding: 0px 9px;font-size:11px;">
                      &nbsp;
                    </td>
                    <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                      <?php echo $tvalue['credit_pl']; ?>
                    </td>
                    <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                      <?php echo $tvalue['taken_pl']; ?>
                    </td>
                    <td style="padding: 0px 9px;font-size:11px;text-align: right;">
                      <?php echo $tvalue['balance_pl']; ?>
                    </td>
                  </tr>
                  <?php 
                    $total_pl_credit = $total_pl_credit + $tvalue['credit_pl'];
                    $total_pl_availed = $total_pl_availed + $tvalue['taken_pl'];
                  ?>
                <?php } ?>
              <?php } ?>
              <tr>
                <td style="padding: 0px 9px; font-weight:bold;font-size:12px;text-align: left;">
                  <?php echo 'Total'; ?>
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
                  <?php echo $final_data['basic_data']['pl_open']; ?>
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
                  <?php echo $total_pl_credit; ?>
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
                  <?php echo $total_pl_availed; ?>
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;text-align: right;">
                  <?php echo $tvalue['balance_pl']; ?>
                </td>
              </tr>
              <tr style="border-bottom:2px solid black;">
                <td colspan = "5">
                </td>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/leave_credit&token=<?php echo $token; ?>';
  
  var filter_name = $('#filter_name').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('#filter_name_id').val();
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_year = $('#filter_year').val();
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var unit = $('#unit').val();
  if (unit && unit != '0') {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('#department').val();
  if (department && department != '0') {
    url += '&department=' + encodeURIComponent(department);
  }

  var division = $('#division').val();
  if (division && division != '0') {
    url += '&division=' + encodeURIComponent(division);
  }

  var region = $('#region').val();
  if (region && region != '0') {
    url += '&region=' + encodeURIComponent(region);
  }

  company_selected = $('#input-company').val();
  field_ids = '';
  if(company_selected){
    max_length = company_selected.length - 1;
    for(i=0; i<company_selected.length; i++){
      if(max_length == i){  
        field_ids += company_selected[i];
      } else {
        field_ids += company_selected[i]+',';
      }
    }
    if (field_ids) {
      url += '&company=' + encodeURIComponent(field_ids);
    }
  }
  
  url += '&once=1';
  
  //alert(url);
  //return false;

  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/leave_credit/export&token=<?php echo $token; ?>';
  
  var filter_name = $('#filter_name').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('#filter_name_id').val();
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_year = $('#filter_year').val();
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var unit = $('#unit').val();
  if (unit && unit != '0') {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('#department').val();
  if (department && department != '0') {
    url += '&department=' + encodeURIComponent(department);
  }

  var division = $('#division').val();
  if (division && division != '0') {
    url += '&division=' + encodeURIComponent(division);
  }

  var region = $('#region').val();
  if (region && region != '0') {
    url += '&region=' + encodeURIComponent(region);
  }

  company_selected = $('#input-company').val();
  field_ids = '';
  if(company_selected){
    max_length = company_selected.length - 1;
    for(i=0; i<company_selected.length; i++){
      if(max_length == i){  
        field_ids += company_selected[i];
      } else {
        field_ids += company_selected[i]+',';
      }
    }
    if (field_ids) {
      url += '&company=' + encodeURIComponent(field_ids);
    }
  }

  location = url;
  return false;
}

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#input-filter_fields, #input-company').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight: 235,
  });
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=report/leave_credit/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
    dataType: 'json',
    success: function(json) {   
      $('#division').find('option').remove();
      if(json['division_datas']){
        $.each(json['division_datas'], function (i, item) {
          $('#division').append($('<option>', { 
              value: item.division_id,
              text : item.division 
          }));
        });
      }
      $('#unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
    dataType: 'json',
    success: function(json) {   
      $('#unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});
//--></script>
<?php echo $footer; ?>