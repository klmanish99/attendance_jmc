<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : '. Date('d-m-Y'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr class="">
        <td>
          Sr. No
        </td>
        <td>
          Employee Code
        </td>
        <td>
          Employee Name
        </td>
        <td>
          Gender
        </td>
        <td>
          Date Of Birth
        </td>
        <td>
          Date Of Joining
        </td>
        <td>
          Date Of Confirmation
        </td>
        <td>
          Employement Type
        </td>
        <td>
          Grade
        </td>
        <td>
          Date Of Left
        </td>
        <td>
          Designation
        </td>
        <td>
          Department
        </td>
        <td>
          Site
        </td>
        <td>
          Division
        </td>
        <td>
          Region
        </td>
        <td>
          State
        </td>
        <td>
          Shift
        </td>
        <td>
          Status
        </td>
        <td>
          Company
        </td>
      </tr>
      <?php $i = 1; ?>
      <?php if($final_datas) { ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="font-size:11px;"><?php echo $i; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['code']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['name']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['gender']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['dob']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['doj']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['doc']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['employement']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['grade']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['dol']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['designation']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['department']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['unit']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['division']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['region']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['state']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['shift']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['status']; ?></td>
            <td class="left" style="font-size:11px;"><?php echo $final_data['company']; ?></td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>