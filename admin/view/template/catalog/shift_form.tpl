<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['shift_id'])) ){ ?>
          <a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Name'; ?></td>
              <td><input type="text" name="name" value="<?php echo $name; ?>" size="100" />
                <?php if ($error_name) { ?>
                <span class="error"><?php echo $error_name; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Code'; ?></td>
              <td><input type="text" name="shift_code" value="<?php echo $shift_code; ?>" size="100" />
                <?php if ($error_shift_code) { ?>
                <span class="error"><?php echo $error_shift_code; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'In Time'; ?></td>
              <td><input type="text" name="in_time" value="<?php echo $in_time; ?>" size="10" class="time" />
                <span class="help"><?php echo 'hh:mm:ss'; ?></span>
                <?php if ($error_in_time) { ?>
                <span class="error"><?php echo $error_in_time; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Out Time'; ?></td>
              <td><input type="text" name="out_time" value="<?php echo $out_time; ?>" size="10" class="time" />
                <span class="help"><?php echo 'hh:mm:ss'; ?></span>
                <?php if ($error_in_time) { ?>
                <span class="error"><?php echo $error_in_time; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;">
               <a onclick="$('.checkboxclass').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$('.checkboxclass').attr('checked', false);"><?php echo 'UnSelect All'; ?></a>
              </td>
            </tr>
            <?php if ($unit_data) { ?>
              <?php foreach ($unit_data as $ukey => $unit) { ?>
                <tr>
                  <td><?php echo "Site( ".$unit.")"; ?></td>
                  <td>
                    <div class="scrollbox">
                      <?php $class = 'even'; ?>
                      <?php foreach ($department_data as $dkey => $dvalue) { ?>
                      <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                      <div class="<?php echo $class; ?>">
                        <?php /* if (in_array($dkey, $shift_datas[$ukey])) { */ ?>
                        <?php if (array_key_exists($dkey, $shift_datas[$ukey])) { ?>
                        <input class="checkboxclass" type="checkbox" name="shift_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" checked="checked" />
                        <?php echo $dvalue; ?>
                        <?php } else { ?>
                        <input class="checkboxclass" type="checkbox" name="shift_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" />
                        <?php echo $dvalue; ?>
                        <?php } ?>
                      </div>
                      <?php } ?>
                    </div>
                    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'UnSelect All'; ?></a>
                  </td>            
                </tr>
              <?php } ?>
            <?php } ?>
            <tr style="display:none;">
              <td><?php echo 'Weekly Off 1'; ?></td>
              <td>
                <select name="weekly_off_1">
                <?php foreach($weeks as $wkey => $wvalue) { ?>
                  <?php if ($wkey == $weekly_off_1) { ?>
                  <option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $wkey; ?>"><?php echo $wvalue; ?></option>
                  <?php } ?>
                <?php } ?>
                </select>
              </td>
            </tr>
            <tr style="display:none;">
              <td><?php echo 'Weekly Off 2'; ?></td>
              <td>
                <select name="weekly_off_2">
                <?php foreach($weeks as $wkey => $wvalue) { ?>
                  <?php if ($wkey == $weekly_off_2) { ?>
                  <option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $wkey; ?>"><?php echo $wvalue; ?></option>
                  <?php } ?>
                <?php } ?>
                </select>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Long Lunch'; ?></td>
              <td>
                <select name="lunch">
                  <?php if ($lunch == 1) { ?>
                    <option value="1" selected="selected"><?php echo 'Yes'; ?></option>
                    <option value="0"><?php echo 'No'; ?></option>
                  <?php } else { ?>
                    <option value="1"><?php echo 'Yes'; ?></option>
                    <option value="0" selected="selected"><?php echo 'No'; ?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<?php echo $footer; ?>