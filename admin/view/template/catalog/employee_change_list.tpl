<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
      </div>
    </div>
    <div class="content">
      <table class="list">
        <thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="left"><?php if ($sort == 'name') { ?>
              <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
              <?php } ?></td>
            <td class="left"><?php if ($sort == 'emp_code') { ?>
              <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Emp Code'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_code; ?>"><?php echo 'Emp Code'; ?></a>
              <?php } ?></td>
            <td class="left"><?php if ($sort == 'region') { ?>
              <a href="<?php echo $sort_region; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Region'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_region; ?>"><?php echo 'Region'; ?></a>
              <?php } ?></td>
            <td class="left"><?php if ($sort == 'division') { ?>
              <a href="<?php echo $sort_division; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Division'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_division; ?>"><?php echo 'Division'; ?></a>
              <?php } ?></td>
            <td class="left"><?php if ($sort == 'unit') { ?>
              <a href="<?php echo $sort_unit; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Location'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_unit; ?>"><?php echo 'Location'; ?></a>
              <?php } ?></td>  
            <td class="left"><?php if ($sort == 'department') { ?>
              <a href="<?php echo $sort_department; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Department'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_department; ?>"><?php echo 'Department'; ?></a>
              <?php } ?></td>
            <td class="left"><?php if ($sort == 'company') { ?>
              <a href="<?php echo $sort_company; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Company'; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_company; ?>"><?php echo 'Company'; ?></a>
              <?php } ?></td>
            <td>
              <a><?php echo "Shift"; ?></a>
            </td>
            <td style="display: none;">
              <a><?php echo "Department Head"; ?></a>
            </td>
            <td class="right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr class="filter">
            <td>&nbsp;</td>
            <td>
              <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:210px;" />
            </td>
            <td>
              <input type="text" id="filter_code" name="filter_code" value="<?php echo $filter_code; ?>"  style="width:210px;" />
            </td>
            <td>
              <select name="filter_region" id="filter_region">
                <?php foreach($region_data as $key => $ud) { ?>
                  <?php if($key == $filter_region) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td>
              <select name="filter_division" id="filter_division">
                <?php foreach($division_data as $key => $ud) { ?>
                  <?php if($key == $filter_division) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td>
              <select name="filter_unit" id="filter_unit">
                <?php foreach($site_data as $key => $ud) { ?>
                  <?php if($key == $filter_unit) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td>
              <select name="filter_department" id="filter_department">
                <?php foreach($department_data as $key => $ud) { ?>
                  <?php if($key == $filter_department) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td>
              <select name="filter_company" id="filter_company">
                <?php foreach($company_data as $key => $ud) { ?>
                  <?php if($key == $filter_company) { ?>
                    <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                  <?php } else { ?>
                    <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td>&nbsp;</td>
            <td align="right"><a onclick="filter();" class="button"><?php echo 'Filter'; ?></a></td>
          </tr>
          <form action="" method="post" enctype="multipart/form-data" id="form">
            <?php if ($employees) { ?>
            <?php foreach ($employees as $employee) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $employee['name']; ?></td>
              <td class="left"><?php echo $employee['code']; ?></td>
              <td class="left"><?php echo $employee['region']; ?></td>
              <td class="left"><?php echo $employee['division']; ?></td>
              <td class="left"><?php echo $employee['location']; ?></td>
              <td class="left"><?php echo $employee['department']; ?></td>
              <td class="left"><?php echo $employee['company']; ?></td>
              <td class="left"><?php echo $employee['shift']; ?></td>
              <td class="right">
                <?php //if($this->user->hasPermission('modify', $route)){ ?>
                  <?php foreach ($employee['action'] as $action) { ?>
                  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                <?php //} ?>
              </td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </form>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/employee_change&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_code = $('input[name=\'filter_code\']').attr('value');
  if (filter_code) {
    url += '&filter_code=' + encodeURIComponent(filter_code);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  if (filter_department && filter_department != '0') {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_division = $('select[name=\'filter_division\']').attr('value');
  if (filter_division && filter_division != '0') {
    url += '&filter_division=' + encodeURIComponent(filter_division);
  }

  var filter_region = $('select[name=\'filter_region\']').attr('value');

  if (filter_region && filter_region != '0') {
    url += '&filter_region=' + encodeURIComponent(filter_region);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit && filter_unit != '0') {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_company = $('select[name=\'filter_company\']').attr('value');
  if (filter_company && filter_company != '0') {
    url += '&filter_company=' + encodeURIComponent(filter_company);
  }
  
  location = url;
  return false;
}

//--></script>
<script type="text/javascript"><!--
$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

$('#filter_region').on('change', function() {
  region = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
    dataType: 'json',
    success: function(json) {   
      $('#filter_division').find('option').remove();
      if(json['division_datas']){
        $.each(json['division_datas'], function (i, item) {
          $('#filter_division').append($('<option>', { 
              value: item.division_id,
              text : item.division 
          }));
        });
      }
      $('#filter_unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#filter_unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});

$('#filter_division').on('change', function() {
  division = $(this).val();
  $.ajax({
    url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
    dataType: 'json',
    success: function(json) {   
      $('#filter_unit').find('option').remove();
      if(json['unit_datas']){
        $.each(json['unit_datas'], function (i, item) {
          $('#filter_unit').append($('<option>', { 
              value: item.unit_id,
              text : item.unit 
          }));
        });
      }
    }
  });
});

//--></script>
<?php echo $footer; ?>