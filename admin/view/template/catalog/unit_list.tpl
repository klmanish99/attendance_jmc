<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('add', $route)){ ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo 'Insert'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('delete', $route)){ ?>
        <a onclick="$('form').submit();" class="button"><?php echo 'Delete'; ?></a>
        <?php } ?>
        <?php if($this->user->hasPermission('modify', $route)){ ?>
        <a href="<?php echo $export; ?>" class="button"><?php echo 'Export'; ?></a>
        <?php } ?>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 'unit') { ?>
                <a href="<?php echo $sort_unit; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_unit; ?>"><?php echo 'Name'; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'unit_code') { ?>
                <a href="<?php echo $sort_unit_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Code'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_unit_code; ?>"><?php echo 'Code'; ?></a>
                <?php } ?></td>  
              <td class="left"><?php if ($sort == 'division_name') { ?>
                <a href="<?php echo $sort_division_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Division'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_division_name; ?>"><?php echo 'Division'; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'state_name') { ?>
                <a href="<?php echo $sort_state_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'State'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_state_name; ?>"><?php echo 'State'; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>" style="width:210px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_division_id" id="filter_division_id">
                  <?php foreach($division_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $filter_division_id) { ?>
                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_state_id" id="filter_state_id">
                  <?php foreach($state_data as $gkey => $gvalue) { ?>
                    <?php if ($gkey == $filter_state_id) { ?>
                      <option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right"><a onclick="filter();" class="button"><?php echo 'Filter'; ?></a></td>
            </tr>
            <?php if ($units) { ?>
            <?php foreach ($units as $unit) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($unit['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $unit['unit_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $unit['unit_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $unit['unit']; ?></td>
              <td class="left"><?php echo $unit['unit_code']; ?></td>
              <td class="left"><?php echo $unit['division_name']; ?></td>
              <td class="left"><?php echo $unit['state_name']; ?></td>
              <td class="right">
                <?php //if($this->user->hasPermission('modify', $route)){ ?>
                  <?php foreach ($unit['action'] as $action) { ?>
                  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                <?php //} ?>
              </td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/unit&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
      
    }
  }

  var filter_division_id = $('select[name=\'filter_division_id\']').attr('value');
  if (filter_division_id) {
    url += '&filter_division_id=' + encodeURIComponent(filter_division_id);
  }

  var filter_state_id = $('select[name=\'filter_state_id\']').attr('value');
  if (filter_state_id) {
    url += '&filter_state_id=' + encodeURIComponent(filter_state_id);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/unit/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.unit,
            value: item.unit_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
$('input[name=\'filter_division\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/division/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.division,
            value: item.division_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_division\']').val(ui.item.label);
    $('input[name=\'filter_division_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>