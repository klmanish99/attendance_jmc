<?php
// Heading
$_['heading_title']     = 'Paid Bills Report';

// Text

// Column
$_['column_bill_id'] 	 = 'Bill Id';
$_['column_horse_name']   = 'Horse Name';
$_['column_trainer']      = 'Trainer';
$_['column_owner']      = 'Owner';
$_['column_total']        = 'Total Amount';
$_['column_pending']     = 'Pending Amount';
$_['column_received'] = 'Received Amount';

$_['button_export'] = 'Export';

// Entry
$_['entry_name']       = 'Owner Name:';
$_['entry_month']   = 'Month';
$_['entry_year']     = 'Year';
$_['entry_doctor']     = 'Doctor';
$_['entry_transaction_type'] = 'Clinic';
?>