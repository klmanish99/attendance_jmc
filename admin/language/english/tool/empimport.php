<?php
// Heading
$_['heading_title']    = 'Employee Import';

// Text
$_['text_backup']      = 'Employee Import';
$_['text_success']     = 'Success: You have successfully imported your Employee Data!';

// Entry
$_['entry_restore']    = 'Restore Backup:';
$_['entry_backup']     = 'Backup:';
$_['button_revert']    = 'Revert';



// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Employee Import!';
$_['error_backup']     = 'Warning: You must select at least one table to backup!';
$_['error_empty']      = 'Warning: The file you uploaded was empty!';
?>