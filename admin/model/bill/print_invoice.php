<?php
class ModelBillPrintInvoice extends Model {
	public function getall_transaction_group($data = array()) {
		$sql = "SELECT `transaction_id`, `horse_id` FROM `" . DB_PREFIX . "transaction` WHERE 1=1 "; 

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND `medicine_doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (!empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		$sql .= " AND `bill_status` = '0' ";

		$sql .= " GROUP BY `horse_id` ASC";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}	
	
	public function getall_transaction($data = array()) {
		$sql = "SELECT `transaction_id`, `month`, `year`, `horse_id`, `medicine_doctor_id`, `trainer_id`, `dot` FROM `" . DB_PREFIX . "transaction` WHERE 1=1 "; 

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND `medicine_doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (!empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		$sql .= " AND `bill_status` = '0' ";

		$sql .= " ORDER BY `transaction_id` ASC";
		//echo $sql;
		//echo '<br />';
		//exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getexist_status($transaction_id){
		$sql = "SELECT `transaction_id`, `bill_id` FROM `" . DB_PREFIX . "bill` WHERE `transaction_id`= '".(int)$transaction_id."' ";		
		//echo $sql;
		//echo '<br />';
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			$is_exist = $query->row['bill_id'];
		} else {
			$is_exist = 0;
		}
		return $is_exist;
	}

	public function getbill_id(){
		$sql = "SELECT `bill_id` FROM `" . DB_PREFIX . "bill` ORDER BY `bill_id` DESC LIMIT 1 ";		
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			$p_bill_id = $query->row['bill_id'];
			$bill_id = $p_bill_id + 1;
		} else {
			$bill_id = 1;
		}
		return $bill_id;	
	}

	public function insert_bill($datas){
		foreach ($datas as $key => $value) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "bill` SET `bill_id` = '" . (int)$value['bill_id'] . "', `transaction_id` = '" . (int)$value['transaction_id'] . "', `month` = '" . $this->db->escape($value['month']) . "', `year` = '" . $this->db->escape($value['year']) . "', `horse_id` = '" . (int)$value['horse_id'] . "', `payment_status` = '0', `doctor_id` = '" . (int)$value['medicine_doctor_id'] . "', `trainer_id` = '" . (int)$value['trainer_id'] . "', `invoice_date` = '" . $this->db->escape(date('Y-m-d')) . "', `dot` = '" . $this->db->escape($value['dot']) . "' ");
			$this->db->query("UPDATE `" . DB_PREFIX . "transaction` SET `bill_status` = '1' WHERE `transaction_id` = '" . (int)$value['transaction_id'] . "' ");
		}
	}

	public function insert_bill_owner($datas){
		foreach ($datas as $key => $value) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "bill_owner` SET `bill_id` = '" . (int)$value['bill_id'] . "', `owner_id` = '" . (int)$value['owner_id'] . "', `owner_share` = '" . (int)$value['owner_share'] . "', `owner_amt` = '" . (float)$value['owner_amt'] . "', `owner_amt_rec` = '" . (float)$value['owner_amt_rec'] . "', `month` = '" . $this->db->escape($value['month']) . "', `year` = '" . $this->db->escape($value['year']) . "', `payment_status` = '0', `doctor_id` = '" . (int)$value['doctor_id'] . "', `transaction_type` = '" . (int)$value['transaction_type'] . "', `horse_id` = '" . (int)$value['horse_id'] . "', `trainer_id` = '" . (int)$value['trainer_id'] . "', `invoice_date` = '" . $this->db->escape(date('Y-m-d')) . "' ");
			//$sql = "SELECT `id`, owner_amt_rec, payment_status FROM `" . DB_PREFIX . "bill_owner` WHERE `bill_id` = '" . (int)$value['bill_id'] . "' AND `owner_id` = '" . (int)$value['owner_id'] . "' ";		
			//$id_query = $this->db->query($sql);
			//if($id_query->num_rows > 0){
				//$id = $id_query->row['id'];
				//$owner_amt_rec = $id_query->row['owner_amt_rec'];				
				//$payment_status = $id_query->row['payment_status'];
				//$invoice_date = $id_query->row['invoice_date'];				
				//$sql_del = "DELETE FROM `" . DB_PREFIX . "bill_owner` WHERE `id` = '" . (int)$id . "' ";		
				//$del_query = $this->db->query($sql_del);
				//$this->db->query("INSERT INTO `" . DB_PREFIX . "bill_owner` SET `id` = '" . (int)$id . "', `bill_id` = '" . (int)$value['bill_id'] . "', `owner_id` = '" . (int)$value['owner_id'] . "', `owner_share` = '" . (int)$value['owner_share'] . "', `owner_amt` = '" . (float)$value['owner_amt'] . "', `owner_amt_rec` = '" . (float)$owner_amt_rec . "', `month` = '" . $this->db->escape($value['month']) . "', `year` = '" . $this->db->escape($value['year']) . "', `payment_status` = '" . (int)$payment_status . "', `doctor_id` = '" . (int)$value['doctor_id'] . "', `transaction_type` = '" . (int)$value['transaction_type'] . "', `horse_id` = '" . (int)$value['horse_id'] . "', `trainer_id` = '" . (int)$value['trainer_id'] . "', `dot` = '" . $this->db->escape($value['dot']) . "', `invoice_date` = '" . $this->db->escape($invoice_date) . "' ");	
			//} else {
				//$this->db->query("INSERT INTO `" . DB_PREFIX . "bill_owner` SET `bill_id` = '" . (int)$value['bill_id'] . "', `owner_id` = '" . (int)$value['owner_id'] . "', `owner_share` = '" . (int)$value['owner_share'] . "', `owner_amt` = '" . (float)$value['owner_amt'] . "', `owner_amt_rec` = '" . (float)$value['owner_amt_rec'] . "', `month` = '" . $this->db->escape($value['month']) . "', `year` = '" . $this->db->escape($value['year']) . "', `payment_status` = '0', `doctor_id` = '" . (int)$value['doctor_id'] . "', `transaction_type` = '" . (int)$value['transaction_type'] . "', `horse_id` = '" . (int)$value['horse_id'] . "', `trainer_id` = '" . (int)$value['trainer_id'] . "', `dot` = '" . $this->db->escape($value['dot']) . "', `invoice_date` = '" . $this->db->escape(date('Y-m-d')) . "' ");
			//}
		}
	}

	public function getbill_groups($data = array()) {
		$sql = "SELECT `bill_id`, `horse_id`, `doctor_id`, `trainer_id` FROM `" . DB_PREFIX . "bill` WHERE `cancel_status` = '0' "; 

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (!empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		$sql .= " GROUP BY `bill_id` ASC";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function get_transaction_ids($bill_id) {
		$sql = "SELECT `transaction_id`, `month`, `year` FROM `" . DB_PREFIX . "bill` WHERE `bill_id` = '".(int)$bill_id."' AND `cancel_status` = '0' "; 
		$sql .= " ORDER BY `transaction_id` ASC";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function get_transaction_data($transaction_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "transaction` WHERE `transaction_id` = '".(int)$transaction_id."' AND `cancel_status` = '0' "; 
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function get_bill_data($bill_id) {
		$sql = "SELECT SUM(owner_amt) AS total FROM `" . DB_PREFIX . "bill_owner` WHERE `bill_id` = '".(int)$bill_id."' "; 
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function get_horse_data($horse_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "horse` WHERE `horse_id` = '".(int)$horse_id."' "; 
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function get_trainer_data($trainer_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "trainer` WHERE `trainer_id` = '".(int)$trainer_id."' "; 
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function get_owner_data($horse_id, $owner_id = 0) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "horse_owner` WHERE `horse_id` = '".(int)$horse_id."' AND `share` <> '0' "; 
		if($owner_id != 0) {
			$sql .= " AND `owner` = '".(int)$owner_id."' ";
		}
		$sql .= " GROUP BY `owner`";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function get_owner_name($owner_id) {
		$sql = "SELECT `name` FROM `" . DB_PREFIX . "owner` WHERE `owner_id` = '".(int)$owner_id."' "; 
		$query = $this->db->query($sql);
		$owner_name = '';
		if($query->num_rows > 0){
			$owner_name = $query->row['name'];
		}
		return $owner_name;
	}

	public function get_owner_transactiontype($owner_id) {
		$sql = "SELECT `transaction_type` FROM `" . DB_PREFIX . "owner` WHERE `owner_id` = '".(int)$owner_id."' "; 
		$query = $this->db->query($sql);
		$transaction_type = '';
		if($query->num_rows > 0){
			$transaction_type = $query->row['transaction_type'];
		}
		return $transaction_type;
	}

	public function get_doctor_name($doctor_id) {
		$sql = "SELECT `name` FROM `" . DB_PREFIX . "doctor` WHERE `doctor_id` = '".(int)$doctor_id."' "; 
		$query = $this->db->query($sql);
		$doctor_name = '';
		if($query->num_rows > 0){
			$octor_name = $query->row['name'];
		}
		return $octor_name;
	}
	
	public function getdoctors(){
		$sql = "SELECT * FROM `" . DB_PREFIX . "doctor` "; 
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getbillowner_groups($data = array()) {
		$sql = "SELECT `bill_id`, `horse_id`, `owner_id`, `owner_amt`, `trainer_id`, `owner_share`, `owner_amt`, `doctor_id`, `month`, `year` FROM `" . DB_PREFIX . "bill_owner` WHERE `cancel_status` = '0' "; 

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (!empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		if (!empty($data['filter_owner'])) {
			$sql .= " AND `owner_id` = '" . (int)$data['filter_owner'] . "'";
		}

		if (!empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}

		$sql .= " ORDER BY `bill_id` ASC";

		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}			

		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}	
			
		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// }
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getbillowner_groups_total($data = array()) {
		$sql = "SELECT count(*) AS total FROM `" . DB_PREFIX . "bill_owner` WHERE `cancel_status` = '0' "; 

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (!empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		$sql .= " ORDER BY `bill_id` ASC";
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function get_horse_name($horse_id) {
		$sql = "SELECT `name` FROM `" . DB_PREFIX . "horse` WHERE `horse_id` = '".(int)$horse_id."' "; 
		$query = $this->db->query($sql);
		$horse_name = '';
		if($query->num_rows > 0){
			$horse_name = $query->row['name'];
		}
		return $horse_name;
	}

	public function get_trainer_name($trainer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "trainer WHERE trainer_id = '".$trainer_id."'");
		$trainer_name = '';
		if($query->num_rows > 0){
			$trainer_name = $query->row['name'];
		}
		return $trainer_name;
	}

	public function getbillowner_once($data = array()) {
		$sql = "SELECT `bill_id`, `horse_id`, `owner_id`, `owner_amt`, `trainer_id`, owner_share, owner_amt, doctor_id FROM `" . DB_PREFIX . "bill_owner` WHERE `cancel_status` = '0' "; 

		if (!empty($data['filter_month'])) {
			$sql .= " AND `month` = '" . $this->db->escape($data['filter_month']) . "'";
		}

		if (!empty($data['filter_year'])) {
			$sql .= " AND `year` = '" . $this->db->escape($data['filter_year']) . "'";
		}

		if (!empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}

		if (!empty($data['filter_horse_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_horse_id'] . "'";
		}

		if (!empty($data['filter_owner'])) {
			$sql .= " AND `owner_id` = '" . (int)$data['filter_owner'] . "'";
		}

		$sql .= " ORDER BY `bill_id` ASC";
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getbillowners($data = array()) {
		$sql = "SELECT `id`, `bill_id`, `horse_id`, `owner_id`, `owner_amt`, `trainer_id`, `owner_share`, `owner_amt`, `owner_amt_rec`, `doctor_id`, `month`, `year`, `dop` FROM `" . DB_PREFIX . "bill_owner` WHERE `cancel_status` = '0' "; 
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (isset($data['filter_doctor']) && !empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (isset($data['filter_trainer_id']) && !empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		if (isset($data['filter_bill_id']) && !empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}

		if (isset($data['filter_owner']) && !empty($data['filter_owner'])) {
			$sql .= " AND `owner_id` = '" . (int)$data['filter_owner'] . "'";
		}

		if (isset($data['filter_unpaid']) && $data['filter_unpaid'] == '1') {
			$sql .= " AND `payment_status` = '0' ";
		}

		if (!empty($data['filter_transaction_type'])) {
			$sql .= " AND `transaction_type` = '" . (int)$data['filter_transaction_type'] . "'";
		}

		$sql .= " ORDER BY `payment_status`, `bill_id` ASC";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getbillowners_payment($data = array()) {
		$sql = "SELECT `id`, `bill_id`, `horse_id`, `owner_id`, `owner_amt`, `trainer_id`, `owner_share`, `owner_amt`, `owner_amt_rec`, `doctor_id`, `month`, `year`, `dop` FROM `" . DB_PREFIX . "bill_owner` WHERE `cancel_status` = '0' "; 
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (isset($data['filter_doctor']) && !empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (isset($data['filter_trainer_id']) && !empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		if (isset($data['filter_bill_id']) && !empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}

		if (isset($data['filter_owner']) && !empty($data['filter_owner'])) {
			$sql .= " AND `owner_id` = '" . (int)$data['filter_owner'] . "'";
		}

		if (isset($data['filter_unpaid']) && $data['filter_unpaid'] == '1') {
			$sql .= " AND `payment_status` = '0' ";
		}

		$sql .= " ORDER BY `owner_amt` DESC";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getbillmonyear($bill_id){
		$sql = "SELECT `month`, `year` FROM `" . DB_PREFIX . "bill` WHERE `bill_id` = '".(int)$bill_id."' AND `cancel_status` = '0' "; 
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getbillids_groups($data = array()) {
		$sql = "SELECT `bill_id`, `horse_id`, `doctor_id`, `trainer_id`, `month`, `year` FROM `" . DB_PREFIX . "bill` WHERE `cancel_status` = '0' "; 

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND date(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND date(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND `horse_id` = '" . (int)$data['filter_name_id'] . "'";
		} 
		
		if (isset($data['filter_trainer_id']) && !empty($data['filter_trainer_id'])) {
			$sql .= " AND `trainer_id` = '" . (int)$data['filter_trainer_id'] . "'";
		}

		if (isset($data['filter_doctor']) && !empty($data['filter_doctor'])) {
			$sql .= " AND `doctor_id` = '" . (int)$data['filter_doctor'] . "'";
		}

		if (isset($data['filter_bill_id']) && !empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}

		$sql .= " GROUP BY `bill_id` ASC";

		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getbillids_bill($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "bill` WHERE 1=1 "; 
		if (isset($data['filter_bill_id']) && !empty($data['filter_bill_id'])) {
			$sql .= " AND `bill_id` = '" . (int)$data['filter_bill_id'] . "'";
		}
		$sql .= " ORDER BY `bill_id` ASC";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}	

	public function updatepaymentstatus($datas = array()){
		$owner_amt_rec = $datas['owner_amt_paying'] + $datas['owner_amt_rec'];
		$dop = date('Y-m-d', strtotime($datas['dop']));
		$sql = "UPDATE `" . DB_PREFIX . "bill_owner` SET `owner_amt_rec` = '".(float)$owner_amt_rec."', `dop` = '".$dop."' WHERE `id` = '".(int)$datas['bill_owner_id']."'";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		if($owner_amt_rec == $datas['owner_amt']){
			$sql = "UPDATE `" . DB_PREFIX . "bill_owner` SET `payment_status` = '1' WHERE `id` = '".(int)$datas['bill_owner_id']."'";
			//$this->log->write($sql);
			$query = $this->db->query($sql);
		}
		
	}

	public function updatebillstatus($bill_id, $bill_status){
		$sql = "UPDATE `" . DB_PREFIX . "bill` SET `cancel_status` = '".(int)$bill_status."' WHERE `bill_id` = '".(int)$bill_id."' ";
		$query = $this->db->query($sql);
		
		$sql = "UPDATE `" . DB_PREFIX . "bill_owner` SET `cancel_status` = '".(int)$bill_status."' WHERE `bill_id` = '".(int)$bill_id."' ";
		$query = $this->db->query($sql);
		
		$sql = "SELECT `transaction_id` FROM `" . DB_PREFIX . "bill` WHERE `bill_id` = '".(int)$bill_id."' ";
		$query = $this->db->query($sql)->rows;
		foreach ($query as $qkey => $qvalue) {
			$sql = "UPDATE `" . DB_PREFIX . "transaction` SET `cancel_status` = '".(int)$bill_status."' WHERE `transaction_id` = '".(int)$qvalue['transaction_id']."' ";
			$query = $this->db->query($sql);	
		}
	}

	public function getbilldoctorid($bill_id){
		$sql = "SELECT `doctor_id` FROM `" . DB_PREFIX . "bill` WHERE `bill_id` = '".(int)$bill_id."' "; 
		$query = $this->db->query($sql);
		return $query->row;
	}		
}
?>
