<?php
class ModelReportCommonReport extends Model {
	public function gettransaction_data($emp_code, $data) {
		// echo'<pre>';
		// print_r($data);
		// exit;
		
		if (isset($data['filter_date_start']) && !empty($data['filter_date_start']) && (isset($data['filter_date_end']) && !empty($data['filter_date_end']))) {
			$start_date = date('Y-m-d', strtotime($data['filter_date_start']));
			$end_date = date('Y-m-d', strtotime($data['filter_date_end']));

			$start_month = date('n',strtotime($start_date));
			$end_month = date('n',strtotime($end_date));

			$start_year = date('Y',strtotime($start_date));
			$end_year = date('Y',strtotime($end_date));
			$date_double = 0;
			if($start_month != $end_month){
				$date_double = 1;
				$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;
				$trans_tblname_nxt = 'oc_transaction_'.$end_year.'_'.$end_month;
			}else {
				$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;

			}

			if($date_double == 0){
				$sql = "SELECT * FROM `".$trans_tblname."` WHERE 1=1";
		
				if (isset($data['filter_month']) && !empty($data['filter_month']) && isset($data['filter_year']) && !empty($data['filter_year'])) {
					$year = $data['filter_year'];
					$month = $data['filter_month'];
					if($month == 1){
						$prev_month = 12;
						$prev_year = $year - 1;
					} else {
						$prev_month = $month - 1;
						$prev_year = $year;
					}
					$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
					$filter_date_end = sprintf("%04d-%02d-%02d", $year, $month, '25');
					$sql .= " AND `date` >= '" . $this->db->escape($filter_date_start) . "' AND `date` <= '".$this->db->escape($filter_date_end)."' ";
				}
				

				if(!isset($data['filter_month']) && !isset($data['filter_year'])){
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
				}

				if(!isset($data['filter_month']) && !isset($data['filter_year'])){
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
				}

				if (isset($data['filter_date']) && !empty($data['filter_date'])) {
					$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date']) . "'";
				}

				if (isset($data['manual_punch']) && !empty($data['manual_punch'])) {
					$sql .= " AND `manual_status` = '1' ";
				}

				if (isset($data['status']) && $data['status'] == 1) {
					$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR `weekly_off` <> '0' OR `holiday_id` <> '0' OR `leave_status` <> '0')  ";
				} elseif(isset($data['status']) && $data['status'] == 2) {
					$sql .= " AND (`absent_status` = '1' AND (`weekly_off` = '0' AND `holiday_id` = '0') )";
				}

				if (isset($data['month_close']) && !empty($data['month_close'])) {
					$sql .= " AND `month_close_status` = '0' ";
				}

				if (isset($data['day_close']) && !empty($data['day_close'])) {
					//$sql .= " AND `day_close_status` = '1' ";
				}

				$sql .= " AND emp_id = '".$emp_code."' ";

				if(isset($data['filter_limit'])){
					$sql .= " LIMIT 0, 10";
				}
						
				$final_array = $this->db->query($sql)->rows;

			} else {
				$trans_tblname = 'oc_transaction_'.$start_year.'_'.$start_month;
				$trans_tblname_nxt = 'oc_transaction_'.$end_year.'_'.$end_month;
				
				$sql = "SELECT * FROM `".$trans_tblname."` WHERE 1=1";
		
				if (isset($data['filter_month']) && !empty($data['filter_month']) && isset($data['filter_year']) && !empty($data['filter_year'])) {
					$year = $data['filter_year'];
					$month = $data['filter_month'];
					if($month == 1){
						$prev_month = 12;
						$prev_year = $year - 1;
					} else {
						$prev_month = $month - 1;
						$prev_year = $year;
					}
					$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
					$filter_date_end = sprintf("%04d-%02d-%02d", $year, $month, '25');
					/*echo'<pre>';
					print_r($filter_date_start);
					echo'<pre>';
					print_r($filter_date_end);
					exit;*/
					$sql .= " AND `date` >= '" . $this->db->escape($filter_date_start) . "'  ";
				}
				
				if(!isset($data['filter_month']) && !isset($data['filter_year'])){
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape($start_date) . "' ";
				}


				if (isset($data['filter_date']) && !empty($data['filter_date'])) {
					$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date']) . "'";
				}

				if (isset($data['manual_punch']) && !empty($data['manual_punch'])) {
					$sql .= " AND `manual_status` = '1' ";
				}

				if (isset($data['status']) && $data['status'] == 1) {
					$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR `weekly_off` <> '0' OR `holiday_id` <> '0' OR `leave_status` <> '0')  ";
				} elseif(isset($data['status']) && $data['status'] == 2) {
					$sql .= " AND (`absent_status` = '1' AND (`weekly_off` = '0' AND `holiday_id` = '0') )";
				}

				if (isset($data['month_close']) && !empty($data['month_close'])) {
					$sql .= " AND `month_close_status` = '0' ";
				}

				if (isset($data['day_close']) && !empty($data['day_close'])) {
					//$sql .= " AND `day_close_status` = '1' ";
				}

				$sql .= " AND emp_id = '".$emp_code."' ";

				if(isset($data['filter_limit'])){
					$sql .= " LIMIT 0, 10";
				}



				$query = $this->db->query($sql)->rows;

				$sql1 = "SELECT * FROM `".$trans_tblname_nxt."` WHERE 1=1";
		
				if (isset($data['filter_month']) && !empty($data['filter_month']) && isset($data['filter_year']) && !empty($data['filter_year'])) {
					$year = $data['filter_year'];
					$month = $data['filter_month'];
					if($month == 1){
						$prev_month = 12;
						$prev_year = $year - 1;
					} else {
						$prev_month = $month - 1;
						$prev_year = $year;
					}
					$filter_date_start = sprintf("%04d-%02d-%02d", $prev_year, $prev_month, '26');
					$filter_date_end = sprintf("%04d-%02d-%02d", $year, $month, '25');
					$sql1 .= " AND  `date` <= '".$this->db->escape($filter_date_end)."' ";
				}
				
				if(!isset($data['filter_month']) && !isset($data['filter_year'])){
					$sql1 .= " AND DATE(`date`) <= '" . $this->db->escape($end_date) . "' ";
				}

				if (isset($data['filter_date']) && !empty($data['filter_date'])) {
					$sql1 .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date']) . "'";
				}

				if (isset($data['manual_punch']) && !empty($data['manual_punch'])) {
					$sql1 .= " AND `manual_status` = '1' ";
				}

				if (isset($data['status']) && $data['status'] == 1) {
					$sql1 .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR `weekly_off` <> '0' OR `holiday_id` <> '0' OR `leave_status` <> '0')  ";
				} elseif(isset($data['status']) && $data['status'] == 2) {
					$sql1 .= " AND (`absent_status` = '1' AND (`weekly_off` = '0' AND `holiday_id` = '0') )";
				}

				if (isset($data['month_close']) && !empty($data['month_close'])) {
					$sql1 .= " AND `month_close_status` = '0' ";
				}

				if (isset($data['day_close']) && !empty($data['day_close'])) {
					//$sql .= " AND `day_close_status` = '1' ";
				}

				$sql1 .= " AND emp_id = '".$emp_code."' ";

				if(isset($data['filter_limit'])){
					$sql1 .= " LIMIT 0, 10";
				}

		// 		echo '<pre>';
		// print_r($sql1);exit;
				$query1 = $this->db->query($sql1)->rows;

				$final_array = array_merge($query, $query1);
			}
		}
		// echo '<pre>';
		// print_r($final_array);exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		//$query = $this->db->query($sql);
		return $final_array;
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND (`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND (`department_id`) = '" . $this->db->escape($data['department']) . "'";
		}

		if (!empty($data['division'])) {
			$sql .= " AND (`division_id`) = '" . $this->db->escape($data['division']) . "'";
		}

		if (!empty($data['region'])) {
			$sql .= " AND (`region_id`) = '" . $this->db->escape($data['region']) . "'";
		}

		if (!empty($data['grade'])) {
			$grade_string = "'" . str_replace(",", "','", html_entity_decode($data['grade'])) . "'";
			$sql .= " AND (`grade_id`) IN (" . strtolower($grade_string) . ") ";
		}

		if (!empty($data['company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['company'])) . "'";
			$sql .= " AND (`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		// if (isset($data['device_id']) && !empty($data['device_id'])) {
		// 	$sql .= " AND `device_id` = '" . $this->db->escape(strtolower($data['device_id'])) . "'";
		// }

		if (!empty($data['group'])) {
			//if($data['group'] == '1'){
				//$sql .= " AND LOWER(`group`) <> 'officials' ";
			//} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			//}
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['company']) && !empty($data['company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . ($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['division']) && !empty($data['division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . ($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['region']) && !empty($data['region'])) {
			} else {
				$sql .= " AND `region_id` IN (" . ($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['unit']) && !empty($data['unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . ($site_string) . ") ";
			}
		}

		//$sql .= " AND `status` = '1' AND `emp_code` = '22848' ";
		if(!isset($data['filter_all_stat'])){
			//$sql .= " AND `status` = '1' ";
		}

		$sql .= " AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$data['filter_date_start']."') ";
		
		$sql .= " ORDER BY `shift_type` ";		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getemployees_muster($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}

		if (!empty($data['division'])) {
			$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($data['division'])) . "'";
		}

		if (!empty($data['region'])) {
			$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($data['region'])) . "'";
		}

		if (!empty($data['company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['company'])) . "'";
			$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		if (!empty($data['group'])) {
			// if($data['group'] == '1'){
			// 	$sql .= " AND LOWER(`group`) <> 'officials' ";
			// } else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			//}
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['company']) && !empty($data['company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['division']) && !empty($data['division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['region']) && !empty($data['region'])) {
			} else {
				$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['unit']) && !empty($data['unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}

		$sql .= " AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$data['filter_date_start']."') ";

		$sql .= " AND (DATE(`doj`) = '0000-00-00' OR DATE(`doj`) <= '".$data['filter_date_end']."') ";

		//$sql .= " AND `status` = '1' AND `emp_code` = '11923' ";
		//$sql .= " AND `status` = '1'";

		$sql .= " ORDER BY `department`, `grade`, `emp_code` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getattendance_exist($date) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE `punch_date` = '".$date."' ");
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function gettransaction_leave_data($emp_code) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' ORDER BY id DESC";

		if(isset($data['filter_limit'])){
			$sql .= " LIMIT 0, 10";
		}
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction  GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date']));	
		} else {
			return date('Y-m-d', strtotime('2016-11-21'));
		}
		
	}

	public function getfilter_date_end($filter_date_start, $filter_name_id, $unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE DATE(`date`) >= '" . $this->db->escape($filter_date_start) . "' AND `day_close_status` = '1' ";
		$in = 0;
		if($filter_name_id){
			$in = 1;
			$units = "SELECT `unit` FROM " . DB_PREFIX . "employee WHERE `emp_code` = '" . $this->db->escape($filter_name_id) . "' ";
			$unit = $this->db->query($units)->row['unit'];
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($unit)) . "'";
		}
		if($in == 0){
			if($unit) {
				$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($unit)) . "'";		
			}
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date']));	
		} else {
			return date('Y-m-d', strtotime('2016-11-21'));
		}
	}

	public function getleave_transaction_data_group($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
			$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getencash_data($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		$encash_days = 0;
		foreach ($query->rows as $key => $value) {
			$encash_days = $encash_days + $value['encash'];
		}
		return $encash_days;
	}

	public function getencash_data_un($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
		$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";

		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		$encash_days = 0;
		foreach ($query->rows as $key => $value) {
			$sql1 = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
			$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
			$query1 = $this->db->query($sql1);
			$p_status = 0;
			foreach($query1->rows as $qkey => $qvalue){
				if($qvalue['p_status'] == 1){
					$p_status = 1;
					break;
				}
			}
			if($p_status == 0){
				$encash_days = $encash_days + $value['encash'];
			}
		}
		return $encash_days;
	}

	public function getleave_transaction_data($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND batch_id = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_data($emp_code, $from_year) {
		$sql = "SELECT * FROM `oc_leave` WHERE 1=1";
		//$sql .= " AND emp_id = '".$emp_code."' AND `close_status` = '0' ";
		$sql .= " AND emp_id = '".$emp_code."' AND `year` = '".$from_year."' ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getsum_data($emp_id, $month, $year, $key) {
		$sql = "SELECT COUNT(*) as total FROM `oc_transaction` WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_id` = '".$emp_id."' AND (`firsthalf_status` = '".$key."' OR `secondhalf_status` = '".$key."') ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getleav_data($emp_id, $month, $year, $key) {
		$sql = "SELECT * FROM `oc_transaction` WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_id` = '".$emp_id."' AND (`firsthalf_status` = '".$key."' OR `secondhalf_status` = '".$key."') ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_transaction_data_group_ess($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
			$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_transaction_data_ess($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
		$sql .= " AND batch_id = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function gettransaction_data_group($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		
		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}

		if (!empty($data['division'])) {
			$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($data['division'])) . "'";
		}

		if (!empty($data['region'])) {
			$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($data['region'])) . "'";
		}

		if (!empty($data['company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['company'])) . "'";
			$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		if (!empty($data['group'])) {
			//if($data['group'] == '1'){
				//$sql .= " AND LOWER(`group`) <> 'officials' ";
			//} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			//}
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['company']) && !empty($data['company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['division']) && !empty($data['division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['region']) && !empty($data['region'])) {
			} else {
				$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['unit']) && !empty($data['unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}

		$sql .= " GROUP BY `date` ASC ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function gettransaction_data_new($data, $date) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		
		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}

		if (!empty($data['division'])) {
			$sql .= " AND LOWER(`division_id`) = '" . $this->db->escape(strtolower($data['division'])) . "'";
		}

		if (!empty($data['region'])) {
			$sql .= " AND LOWER(`region_id`) = '" . $this->db->escape(strtolower($data['region'])) . "'";
		}

		if (!empty($data['company'])) {
			$company_string = "'" . str_replace(",", "','", html_entity_decode($data['company'])) . "'";
			$sql .= " AND LOWER(`company_id`) IN (" . strtolower($company_string) . ") ";
		}

		if (!empty($data['group'])) {
			//if($data['group'] == '1'){
				//$sql .= " AND LOWER(`group`) <> 'officials' ";
			//} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			//}
		}

		$division_string = $this->user->getdivision();
		$region_string = $this->user->getregion();
		$site_string = $this->user->getsite();
		$company_string = $this->user->getCompanyId();
			
		if($company_string != ''){
			$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
			if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			} else {
				$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
			}
		}
		
		if($division_string != ''){
			$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
			if (isset($data['division']) && !empty($data['division'])) {
			} else {
				$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
			}
		}

		if($region_string != ''){
			$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
			if (isset($data['region']) && !empty($data['region'])) {
			} else {
				$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
			}
		}

		if($site_string != ''){
			$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
			if (isset($data['unit']) && !empty($data['unit'])) {
			} else {
				$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
			}
		}

		$sql .= " AND `date` = '".$date."' ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function gethistory_transaction_data_group($emp_code, $data) {
		$sql = "SELECT * FROM `oc_employee_change_history` WHERE 1=1";

		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`to_date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`to_date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (isset($data['filter_fields']) && !empty($data['filter_fields'])) {
			$fields_string = implode(',', $data['filter_fields']);
			$fields_string = "'" . str_replace(",", "','", html_entity_decode($fields_string)) . "'";
			$sql .= " AND `type` IN (" . strtolower($fields_string) . ") ";
		}

		$sql .= " AND emp_code = '".$emp_code."' GROUP BY DATE(`to_date`) ORDER BY DATE(`to_date`) ASC";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function gethistory_transaction_data($emp_code, $data) {
		$sql = "SELECT * FROM `oc_employee_change_history` WHERE 1=1";

		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`to_date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`to_date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (isset($data['filter_to_date']) && !empty($data['filter_to_date'])) {
			$sql .= " AND DATE(`to_date`) = '" . $this->db->escape($data['filter_to_date']) . "'";
		}

		if (isset($data['filter_fields']) && !empty($data['filter_fields'])) {
			$fields_string = implode(',', $data['filter_fields']);
			$fields_string = "'" . str_replace(",", "','", html_entity_decode($fields_string)) . "'";
			$sql .= " AND `type` IN (" . strtolower($fields_string) . ") ";
		}

		$sql .= " AND emp_code = '".$emp_code."' ORDER BY DATE(`to_date`) ASC";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>
