<?php
class ModelCatalogEmployement extends Model {
	public function addEmployement($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "employement` SET 
							`employement` = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',
							`employement_code` = '" . $this->db->escape(html_entity_decode($data['employement_code'])) . "'
						");

		$employement_id = $this->db->getLastId(); 
	}

	public function editEmployement($employement_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "employement SET 
							`employement` = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',
							`employement_code` = '" . $this->db->escape(html_entity_decode($data['employement_code'])) . "'
							WHERE employement_id = '" . (int)$employement_id . "'");
	}

	public function deleteEmployement($employement_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "employement WHERE employement_id = '" . (int)$employement_id . "'");
	}	

	public function getEmployement($employement_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employement WHERE employement_id = '" . (int)$employement_id . "'");

		return $query->row;
	}

	public function getEmployements($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employement WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND employement_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(employement) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'employement',
			'employement_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY employement";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalEmployements() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "employement";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND employement_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(employement) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>