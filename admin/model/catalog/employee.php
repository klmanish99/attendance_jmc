<?php
class ModelCatalogEmployee extends Model {
	public function addemployee($data) {
		if($data['dob'] != '00-00-0000' && $data['dob'] != ''){
			$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		} else {
			$data['dob'] = '0000-00-00';
		}

		if($data['doj'] != '00-00-0000' && $data['doj'] != ''){
			$data['doj'] = date('Y-m-d', strtotime($data['doj']));
			$doj = date('Y-m-d', strtotime($data['doj']));
		} else {
			$data['doj'] = '0000-00-00';
			$doj = '0000-00-00';
		}

		if($data['doc'] != '00-00-0000' && $data['doc'] != ''){
			$data['doc'] = date('Y-m-d', strtotime($data['doc']));
		} else {
			$data['doc'] = '0000-00-00';
		}

		if($data['dol'] != '00-00-0000' && $data['dol'] != ''){
			$data['dol'] = date('Y-m-d', strtotime($data['dol']));
		} else {
			$data['dol'] = '0000-00-00';
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							gender = '" . $this->db->escape($data['gender']) . "', 
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							employement = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',  
							employement_id = '" . $this->db->escape($data['employement_id']) . "',  
							grade = '" . $this->db->escape(html_entity_decode($data['grade'])) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							dol = '" . $this->db->escape($data['dol']) . "',  
							designation = '" . $this->db->escape(html_entity_decode($data['designation'])) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape(html_entity_decode($data['department'])) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							unit = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',  
							unit_id = '" . $this->db->escape($data['unit_id']) . "',
							division = '" . $this->db->escape(html_entity_decode($data['division'])) . "',  
							division_id = '" . $this->db->escape($data['division_id']) . "',  
							region = '" . $this->db->escape(html_entity_decode($data['region'])) . "',  
							region_id = '" . $this->db->escape($data['region_id']) . "',
							state = '" . $this->db->escape(html_entity_decode($data['state'])) . "',  
							state_id = '" . $this->db->escape($data['state_id']) . "',
							status = '" . $this->db->escape($data['status']) . "',
							company = '" . $this->db->escape(html_entity_decode($data['company'])) . "',  
							company_id = '" . $this->db->escape($data['company_id']) . "',
							shift_id = '" . $this->db->escape($data['shift_id']) . "',
							sat_status = '" . $this->db->escape($data['sat_status']) . "',
							shift_type = 'F',
							is_new = '1' ");
		$employee_id = $this->db->getLastId();
		$emp_code = $data['emp_code'];
		$user_name = $emp_code;
		$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		$password = sha1($salt . sha1($salt . sha1($emp_code)));
		$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0' WHERE `employee_id` = '".$employee_id."' ";
		$this->db->query($sql);

		$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
		if($open_years->num_rows > 0){
			$open_year = $open_years->row['year'];
		} else {
			$open_year = date('Y');
		}

		$is_exist = $this->db->query("SELECT `emp_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '".$open_year."' ");
		if($is_exist->num_rows == 0){
			$insert2 = "INSERT INTO `oc_leave` SET 
						`emp_id` = '".$emp_code."', 
						`emp_name` = '".$this->db->escape($data['name'])."', 
						`emp_doj` = '".$data['doj']."', 
						`year` = '".$open_year."',
						`close_status` = '0' "; 
			$this->db->query($insert2);
			// echo $insert1;
			// echo '<br />';
			// exit;
		}
		

		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		// foreach ($months_array as $key => $value) {
		// 	$insert1 = "INSERT INTO `oc_shift_schedule` SET 
		// 				`emp_code` = '".$data['emp_code']."',
		// 				`early_mark` = '0',
		// 				`late_mark` = '0',
		// 				`month` = '".$key."',
		// 				`year` = '".date('Y')."',
		// 				";
		// 	$this->db->query($insert1);
		// }
		/*
		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `unit` = '".$data['unit']."' AND `emp_code` <> '".$data['emp_code']."' LIMIT 1");
		if($emp_codes->num_rows > 0){ 
			$emp_codes = $emp_codes->row;
			$emp_code = $emp_codes['emp_code'];
			$exist_schedules = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_code."' ");
			if($exist_schedules->num_rows > 0){
				$exist_schedule = $exist_schedules->rows;
				foreach($exist_schedule as $ekeys => $evalues){
					$sql = "INSERT INTO `oc_shift_schedule` SET `1` = '".$evalues['1']."', `2` = '".$evalues['2']."', `3` = '".$evalues['3']."', `4` = '".$evalues['4']."', `5` = '".$evalues['5']."', `6` = '".$evalues['6']."', `7` = '".$evalues['7']."', `8` = '".$evalues['8']."', `9` = '".$evalues['9']."', `10` = '".$evalues['10']."', `11` = '".$evalues['11']."', `12` = '".$evalues['12']."', `13` = '".$evalues['13']."', `14` = '".$evalues['14']."', `15` = '".$evalues['15']."', `16` = '".$evalues['16']."', `17` = '".$evalues['17']."', `18` = '".$evalues['18']."', `19` = '".$evalues['19']."', `20` = '".$evalues['20']."', `21` = '".$evalues['21']."', `22` = '".$evalues['22']."', `23` = '".$evalues['23']."', `24` = '".$evalues['24']."', `25` = '".$evalues['25']."', `26` = '".$evalues['26']."', `27` = '".$evalues['27']."', `28` = '".$evalues['28']."', `29` = '".$evalues['29']."', `30` = '".$evalues['30']."', `31` = '".$evalues['31']."', `emp_code` = '".$data['emp_code']."', `status` = '1', `month` = '".$evalues['month']."', `year` = '".$evalues['year']."', `unit` = '".$evalues['unit']."', `unit_id` = '".$evalues['unit_id']."' "; 
					$this->db->query($sql);
					//echo $sql;exit;
				}
			}
		} else {
		*/
			$shift_id_data = 'S_'.$data['shift_id'];
			foreach ($months_array as $key => $value) {
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
					`emp_code` = '".$data['emp_code']."',
					`1` = '".$shift_id_data."',
					`2` = '".$shift_id_data."',
					`3` = '".$shift_id_data."',
					`4` = '".$shift_id_data."',
					`5` = '".$shift_id_data."',
					`6` = '".$shift_id_data."',
					`7` = '".$shift_id_data."',
					`8` = '".$shift_id_data."',
					`9` = '".$shift_id_data."',
					`10` = '".$shift_id_data."',
					`11` = '".$shift_id_data."',
					`12` = '".$shift_id_data."', 
					`13` = '".$shift_id_data."', 
					`14` = '".$shift_id_data."', 
					`15` = '".$shift_id_data."', 
					`16` = '".$shift_id_data."', 
					`17` = '".$shift_id_data."', 
					`18` = '".$shift_id_data."', 
					`19` = '".$shift_id_data."', 
					`20` = '".$shift_id_data."', 
					`21` = '".$shift_id_data."', 
					`22` = '".$shift_id_data."', 
					`23` = '".$shift_id_data."', 
					`24` = '".$shift_id_data."', 
					`25` = '".$shift_id_data."', 
					`26` = '".$shift_id_data."', 
					`27` = '".$shift_id_data."', 
					`28` = '".$shift_id_data."', 
					`29` = '".$shift_id_data."', 
					`30` = '".$shift_id_data."', 
					`31` = '".$shift_id_data."', 
					`month` = '".$key."',
					`year` = '2018',
					`status` = '1' ,
					`unit` = '".$data['unit']."',
					`unit_id` = '".$data['unit_id']."' "; 
					// echo "<pre>";
					// echo $insert1;
				$this->db->query($insert1);
			
				$year = 2018;
				if($key == 12){
					$mod_key = 1;
					$mod_year = $year + 1;
				} else {
					$mod_key = $key + 1;
					$mod_year = $year;
				}
				//echo $mod_key;exit;
				$start_date_string = '2018-'.$key.'-01';
				$start = new DateTime('2018-'.$key.'-01');
				//if(strtotime($start_date_string) > strtotime($doj)){
					$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($start, $interval, $end);
					$week_array = array();
					foreach ($period as $dt)
					{
					    $current_date = $dt->format('Y-m-d');
						if(strtotime($current_date) > strtotime($doj)){
							if ($dt->format('N') == 3)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
						    }
						    if ($dt->format('N') == 7 || $dt->format('N') == 6)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
					    		$current_day = date('j', strtotime($sunday));
					    		if($first_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
					    		}

					    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
					    		if($second_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
					    		}

					    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
					    		if($third_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
					    		}

					    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
					    		if($fourth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
					    		}

					    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
					    		if($fifth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
					    		}
						        //$no++;
						    }
						}
					}
					$this->log->write(print_r($week_array, true));
					$this->log->write(print_r($start_date_string, true));
					$this->log->write(print_r($end, true));
				    // echo '<pre>';
				    // print_r($week_array);
				    // exit;
				    foreach ($week_array as $wkey => $wvalue) {
						$week_string = 'W_1_'.$data['shift_id'];
						$halfday_string = 'HD_1_'.$data['shift_id'];
						$shift_string = 'S_'.$data['shift_id'];
						if($data['shift_id'] == '1'){
							if($data['sat_status'] == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		//$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						/*
						if($data['unit'] == 'VITA' && $data['shift_id'] == '2'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						} else {
						*/	
						if($data['shift_id'] == '2' || $data['shift_id'] == '4'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						//}
					}
				//}
			}
			foreach ($months_array as $key => $value) {
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
					`emp_code` = '".$data['emp_code']."',
					`1` = '".$shift_id_data."',
					`2` = '".$shift_id_data."',
					`3` = '".$shift_id_data."',
					`4` = '".$shift_id_data."',
					`5` = '".$shift_id_data."',
					`6` = '".$shift_id_data."',
					`7` = '".$shift_id_data."',
					`8` = '".$shift_id_data."',
					`9` = '".$shift_id_data."',
					`10` = '".$shift_id_data."',
					`11` = '".$shift_id_data."',
					`12` = '".$shift_id_data."', 
					`13` = '".$shift_id_data."', 
					`14` = '".$shift_id_data."', 
					`15` = '".$shift_id_data."', 
					`16` = '".$shift_id_data."', 
					`17` = '".$shift_id_data."', 
					`18` = '".$shift_id_data."', 
					`19` = '".$shift_id_data."', 
					`20` = '".$shift_id_data."', 
					`21` = '".$shift_id_data."', 
					`22` = '".$shift_id_data."', 
					`23` = '".$shift_id_data."', 
					`24` = '".$shift_id_data."', 
					`25` = '".$shift_id_data."', 
					`26` = '".$shift_id_data."', 
					`27` = '".$shift_id_data."', 
					`28` = '".$shift_id_data."', 
					`29` = '".$shift_id_data."', 
					`30` = '".$shift_id_data."', 
					`31` = '".$shift_id_data."', 
					`month` = '".$key."',
					`year` = '".date('Y')."',
					`status` = '1' ,
					`unit` = '".$data['unit']."',
					`unit_id` = '".$data['unit_id']."' "; 
					// echo "<pre>";
					// echo $insert1;
				$this->db->query($insert1);
			
				$year = date('Y');
				if($key == 12){
					$mod_key = 1;
					$mod_year = $year + 1;
				} else {
					$mod_key = $key + 1;
					$mod_year = $year;
				}
				//echo $mod_key;exit;
				$start_date_string = date('Y').'-'.$key.'-01';
				$start = new DateTime(date('Y').'-'.$key.'-01');
				//if(strtotime($start_date_string) > strtotime($doj)){
					$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($start, $interval, $end);
					$week_array = array();
					foreach ($period as $dt)
					{
					    $current_date = $dt->format('Y-m-d');
						if(strtotime($current_date) > strtotime($doj)){
							if ($dt->format('N') == 3)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
						    }
						    if ($dt->format('N') == 7 || $dt->format('N') == 6)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
					    		$current_day = date('j', strtotime($sunday));
					    		if($first_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
					    		}

					    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
					    		if($second_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
					    		}

					    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
					    		if($third_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
					    		}

					    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
					    		if($fourth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
					    		}

					    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
					    		if($fifth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
					    		}
						        //$no++;
						    }
						}
					}
					$this->log->write(print_r($week_array, true));
					$this->log->write(print_r($start_date_string, true));
					$this->log->write(print_r($end, true));
				    // echo '<pre>';
				    // print_r($week_array);
				    // exit;
				    foreach ($week_array as $wkey => $wvalue) {
						$week_string = 'W_1_'.$data['shift_id'];
						$halfday_string = 'HD_1_'.$data['shift_id'];
						$shift_string = 'S_'.$data['shift_id'];
						if($data['shift_id'] == '1'){
							if($data['sat_status'] == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		//$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						/*
						if($data['unit'] == 'VITA' && $data['shift_id'] == '2'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						} else {
						*/
						if($data['shift_id'] == '2' || $data['shift_id'] == '4'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						//}
					}
				//}
			}
		//}

		$holidayloc_sql = "SELECT `holiday_id`, `location`, `location_id` FROM `oc_holiday_loc` WHERE `location_id` = '".$data['unit_id']."' ";
		$holidayloc_datas = $this->db->query($holidayloc_sql);
		if($holidayloc_datas->num_rows > 0){
			$holidayloc_data = $holidayloc_datas->rows;
			foreach($holidayloc_data as $hkey => $hvalue){
				$holiday_sql = "SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$hvalue['holiday_id']."' AND `date` >= '".$doj."' ";
				$holiday_datas = $this->db->query($holiday_sql);
				if($holiday_datas->num_rows > 0){
					$holiday_data = $holiday_datas->row;
					$date = $holiday_data['date'];
					$month = date('n', strtotime($date));
					$year = date('Y', strtotime($date));
					$day = date('j', strtotime($date));
					$holiday_string = 'H_'.$hvalue['holiday_id'].'_'.$data['shift_id'];
					$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$data['emp_code']."' ";
					// echo $update_shift_schedule;
					// echo '<br />';
					// //exit;
					$this->db->query($update_shift_schedule);
				}
			}
			//echo 'out';exit;
		}

		$current_date = date('Y-m-d');
		$current_month = date('n');
		if($current_month == 1 || $current_month == 2){
			$start_date = date('Y-02-01');
		} else {
			$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2019-06-26';
		}
		$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2019-06-30'
		$day = array();
		$days = $this->GetDays($start_date, $end_date);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		// echo '<pre>';
		// print_r($day);
		// exit;
		$batch_id = '0';
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			$results = $this->getemployee_code($emp_code);
			// echo '<pre>';
			// print_r($results);
			// exit;
			foreach ($results as $rkey => $rvalue) {
				if(isset($rvalue['name']) && $rvalue['name'] != ''){
					$emp_name = $rvalue['name'];
					$department = $rvalue['department'];
					$unit = $rvalue['unit'];
					$group = '';
					$day_date = date('j', strtotime($filter_date_start));
					$month = date('n', strtotime($filter_date_start));
					$year = date('Y', strtotime($filter_date_start));
					$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
					//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
					$shift_schedule = $this->db->query($update3)->row;
					$schedule_raw = explode('_', $shift_schedule[$day_date]);
					if(!isset($schedule_raw[2])){
						$schedule_raw[2]= 1;
					}
					// echo '<pre>';
					// print_r($schedule_raw);
					// exit;
					if($schedule_raw[0] == 'S'){
						$shift_data = $this->getshiftdata_data($schedule_raw[1]);
						if(isset($shift_data['shift_id'])){
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
								//echo $sql;exit;
								$this->db->query($sql);
							}
						} else {
							$shift_data = $this->getshiftdata_data('1');
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
								$this->db->query($sql);
							}
						}
					} elseif ($schedule_raw[0] == 'W') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
							$this->db->query($sql);
						}
					} elseif ($schedule_raw[0] == 'H') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
							$this->db->query($sql);
						}
					} elseif ($schedule_raw[0] == 'HD') {
						$shift_data = $this->getshiftdata_data($schedule_raw[2]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->getshiftdata_data('1');
						}
						$shift_intime = $shift_data['in_time'];
						if($shift_data['shift_id'] == '1'){
							if($rvalue['sat_status'] == '1'){
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
							} else {
								$shift_outtime = $shift_data['out_time'];
							}
						} else {
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
						}
						$act_intime = '00:00:00';
						$act_outtime = '00:00:00';
						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
							$this->db->query($sql);
						}
					}
				}	
			}
		}
	}

	public function editemployee($employee_id, $data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['dob'] != '00-00-0000' && $data['dob'] != ''){
			$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		} else {
			$data['dob'] = '0000-00-00';
		}

		if($data['doj'] != '00-00-0000' && $data['doj'] != ''){
			$data['doj'] = date('Y-m-d', strtotime($data['doj']));
			$doj = date('Y-m-d', strtotime($data['doj']));
		} else {
			$data['doj'] = '0000-00-00';
			$doj = '0000-00-00';
		}

		if($data['hidden_doj'] != '00-00-0000' && $data['hidden_doj'] != ''){
			$data['hidden_doj'] = date('Y-m-d', strtotime($data['hidden_doj']));
			$hidden_doj = date('Y-m-d', strtotime($data['hidden_doj']));
		} else {
			$data['hidden_doj'] = '0000-00-00';
			$hidden_doj = '0000-00-00';
		}

		if($data['doc'] != '00-00-0000' && $data['doc'] != ''){
			$data['doc'] = date('Y-m-d', strtotime($data['doc']));
		} else {
			$data['doc'] = '0000-00-00';
		}

		if($data['dol'] != '00-00-0000' && $data['dol'] != ''){
			$data['dol'] = date('Y-m-d', strtotime($data['dol']));
		} else {
			$data['dol'] = '0000-00-00';
		}

		if($data['hidden_dol'] != '00-00-0000' && $data['hidden_dol'] != ''){
			$data['hidden_dol'] = date('Y-m-d', strtotime($data['hidden_dol']));
		} else {
			$data['hidden_dol'] = '0000-00-00';
		}

		if($data['hidden_status'] == '1' && $data['status'] == '0'){
			$from_date = '0000-00-00';
			$to_date = date('Y-m-d', strtotime($data['dol']));
			$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
			$this->db->query($sql);
		}
		
		if($data['change_date'] != '00-00-0000' && $data['change_date'] != ''){
			if($data['region_id'] != $data['hidden_region_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Region' AND `current_value_id` = '".$data['hidden_region_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Region', `value` = '".$this->db->escape(html_entity_decode($data['hidden_region']))."', `value_id` = '".$this->db->escape($data['hidden_region_id'])."', `current_value_id` = '".$this->db->escape($data['region_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['division_id'] != $data['hidden_division_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Division' AND `current_value_id` = '".$data['hidden_division_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Division', `value` = '".$this->db->escape(html_entity_decode($data['hidden_division']))."', `value_id` = '".$this->db->escape($data['hidden_division_id'])."', `current_value_id` = '".$this->db->escape($data['division_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['unit_id'] != $data['hidden_unit_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Unit' AND `current_value_id` = '".$data['hidden_unit_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Unit', `value` = '".$this->db->escape(html_entity_decode($data['hidden_unit']))."', `value_id` = '".$this->db->escape($data['hidden_unit_id'])."', `current_value_id` = '".$this->db->escape($data['unit_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['designation_id'] != $data['hidden_designation_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Designation' AND `current_value_id` = '".$data['hidden_designation_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Designation', `value` = '".$this->db->escape(html_entity_decode($data['hidden_designation']))."', `value_id` = '".$this->db->escape($data['hidden_designation_id'])."', `current_value_id` = '".$this->db->escape($data['designation_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['grade_id'] != $data['hidden_grade_id']){
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Grade' AND `current_value_id` = '".$data['hidden_grade_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Grade', `value` = '".$this->db->escape(html_entity_decode($data['hidden_grade']))."', `value_id` = '".$this->db->escape($data['hidden_grade_id'])."', `current_value_id` = '".$this->db->escape($data['grade_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}		
			if($data['department_id'] != $data['hidden_department_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Department' AND `current_value_id` = '".$data['hidden_department_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Department', `value` = '".$this->db->escape(html_entity_decode($data['hidden_department']))."', `value_id` = '".$this->db->escape($data['hidden_department_id'])."', `current_value_id` = '".$this->db->escape($data['department_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['employement_id'] != $data['hidden_employement_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Employment' AND `current_value_id` = '".$data['hidden_employement_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Employment', `value` = '".$this->db->escape(html_entity_decode($data['hidden_employement']))."', `value_id` = '".$this->db->escape($data['hidden_employement_id'])."', `current_value_id` = '".$this->db->escape($data['employement_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
			if($data['company_id'] != $data['hidden_company_id']){
				//$to_date = date('Y-m-d', strtotime($data['change_date'].' -1 day'));
				$to_date = date('Y-m-d', strtotime($data['change_date']));
				$past_datas = $this->db->query("SELECT `to_date` FROM `oc_employee_change_history` WHERE `type` = 'Company' AND `current_value_id` = '".$data['hidden_company_id']."' AND `emp_code` = '".$data['emp_code']."' ");
				if($past_datas->num_rows > 0){
					$past_data = $past_datas->row;
					$date_tos = $past_data['to_date'];
					$from_date = date('Y-m-d', strtotime($date_tos.' +1 day'));
				} else {
					$from_date = $data['doj'];
				}
				$sql = "INSERT INTO `oc_employee_change_history` SET `emp_code` = '".$data['emp_code']."', `emp_name` = '".$this->db->escape($data['name'])."', `from_date` = '".$this->db->escape($from_date)."', `to_date` = '".$this->db->escape($to_date)."', `type` = 'Company', `value` = '".$this->db->escape(html_entity_decode($data['hidden_company']))."', `value_id` = '".$this->db->escape($data['hidden_company_id'])."', `current_value_id` = '".$this->db->escape($data['company_id'])."', `department` = '".$this->db->escape(html_entity_decode($data['department']))."', `department_id` = '".$this->db->escape($data['department_id'])."', `unit` = '".$this->db->escape(html_entity_decode($data['unit']))."', `unit_id` = '".$this->db->escape($data['unit_id'])."', `division` = '".$this->db->escape(html_entity_decode($data['division']))."', `division_id` = '".$this->db->escape($data['division_id'])."', `designation` = '".$this->db->escape(html_entity_decode($data['designation']))."', `designation_id` = '".$this->db->escape($data['designation_id'])."', `grade` = '".$this->db->escape(html_entity_decode($data['grade']))."', `grade_id` = '".$this->db->escape($data['grade_id'])."', `employement` = '".$this->db->escape(html_entity_decode($data['employement']))."', `employement_id` = '".$this->db->escape($data['employement_id'])."', `company` = '".$this->db->escape(html_entity_decode($data['company']))."', `company_id` = '".$this->db->escape($data['company_id'])."', `region` = '".$this->db->escape(html_entity_decode($data['region']))."', `region_id` = '".$this->db->escape($data['region_id'])."' ";
				$this->db->query($sql);
			}
		}	
		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							gender = '" . $this->db->escape($data['gender']) . "', 
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							employement = '" . $this->db->escape(html_entity_decode($data['employement'])) . "',  
							employement_id = '" . $this->db->escape($data['employement_id']) . "',  
							grade = '" . $this->db->escape(html_entity_decode($data['grade'])) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							dol = '" . $this->db->escape($data['dol']) . "',  
							designation = '" . $this->db->escape(html_entity_decode($data['designation'])) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape(html_entity_decode($data['department'])) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							unit = '" . $this->db->escape(html_entity_decode($data['unit'])) . "',  
							unit_id = '" . $this->db->escape($data['unit_id']) . "',
							division = '" . $this->db->escape(html_entity_decode($data['division'])) . "',  
							division_id = '" . $this->db->escape($data['division_id']) . "',  
							region = '" . $this->db->escape(html_entity_decode($data['region'])) . "',  
							region_id = '" . $this->db->escape($data['region_id']) . "',
							state = '" . $this->db->escape(html_entity_decode($data['state'])) . "',  
							state_id = '" . $this->db->escape($data['state_id']) . "',
							status = '" . $this->db->escape($data['status']) . "',
							company = '" . $this->db->escape(html_entity_decode($data['company'])) . "',  
							company_id = '" . $this->db->escape($data['company_id']) . "',
							shift_id = '" . $this->db->escape($data['shift_id']) . "',
							sat_status = '" . $this->db->escape($data['sat_status']) . "'
							WHERE employee_id = '" . (int)$employee_id . "'");
		
		if($data['emp_code'] != $data['hidden_emp_code']){
			$shift_schedule_sql = $this->db->query("UPDATE `oc_shift_schedule` SET `emp_code` = '".$data['emp_code']."' WHERE `emp_code` = '".$data['hidden_emp_code']."' ");
			$leave_sql = $this->db->query("UPDATE `oc_leave` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_emp_sql = $this->db->query("UPDATE `oc_leave_employee` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_transaction_sql = $this->db->query("UPDATE `oc_leave_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$transaction_sql = $this->db->query("UPDATE `oc_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
		}

		$emp_code = $data['emp_code'];
		if($data['password_reset'] == '1'){
			$user_name = $emp_code;
			$salt = substr(md5(uniqid(rand(), true)), 0, 9);
			$password = sha1($salt . sha1($salt . sha1($emp_code)));

			$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0' WHERE `employee_id` = '".$employee_id."' ";
			$this->db->query($sql);
		}

		$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
		if($open_years->num_rows > 0){
			$open_year = $open_years->row['year'];
		} else {
			$open_year = date('Y');
		}

		$is_exist = $this->db->query("SELECT `emp_id` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `year` = '".$open_year."' ");
		// echo '<pre>';
		// print_r($is_exist);
		// exit;
		if($is_exist->num_rows == 0){
			$insert2 = "INSERT INTO `oc_leave` SET 
						`emp_id` = '".$emp_code."', 
						`emp_name` = '".$this->db->escape($data['name'])."', 
						`emp_doj` = '".$data['doj']."', 
						`year` = '".$open_year."',
						`close_status` = '0' "; 
			$this->db->query($insert2);
			// echo $insert1;
			// echo '<br />';
			// exit;
		}

		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);

		$shift_id_data = 'S_'.$data['shift_id'];
		foreach ($months_array as $key => $value) {
			$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['emp_code']."' AND `month` = '".$key."' AND `year` = '2018' ");
			if($is_exist->num_rows == 0){
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$data['emp_code']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',
							`month` = '".$key."',
							`year` = '2018',
							`status` = '1' ,
							`unit` = '".$data['unit']."',
							`unit_id` = '".$data['unit_id']."' ";
				$this->db->query($insert1);

				$year = '2018';
				if($key == 12){
					$mod_key = 1;
					$mod_year = $year + 1;
				} else {
					$mod_key = $key + 1;
					$mod_year = $year;
				}
				//echo $mod_key;exit;
				$start_date_string = '2018-'.$key.'-01';
				$start = new DateTime('2018-'.$key.'-01');
				//if(strtotime($start_date_string) > strtotime($doj)){
					$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($start, $interval, $end);
					$week_array = array();
					foreach ($period as $dt)
					{
						$current_date = $dt->format('Y-m-d');
						if(strtotime($current_date) > strtotime($doj)){
						    if ($dt->format('N') == 3)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
						    }
						    if ($dt->format('N') == 7 || $dt->format('N') == 6)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
					    		$current_day = date('j', strtotime($sunday));
					    		if($first_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
					    		}

					    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
					    		if($second_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
					    		}

					    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
					    		if($third_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
					    		}

					    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
					    		if($fourth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
					    		}

					    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
					    		if($fifth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
					    		}
						        //$no++;
						    }
						}
					}
					foreach ($week_array as $wkey => $wvalue) {
						$week_string = 'W_1_'.$data['shift_id'];
						$halfday_string = 'HD_1_'.$data['shift_id'];
						$shift_string = 'S_'.$data['shift_id'];
						if($data['shift_id'] == '1'){
							if($data['sat_status'] == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		//$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						/*
						if($data['unit'] == 'VITA' && $data['shift_id'] == '2'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						} else {
						*/
						if($data['shift_id'] == '2' || $data['shift_id'] == '4'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						//}
					}
				//}
			}
			$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$data['emp_code']."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
			if($is_exist->num_rows == 0){
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$data['emp_code']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',
							`month` = '".$key."',
							`year` = '".date('Y')."',
							`status` = '1' ,
							`unit` = '".$data['unit']."',
							`unit_id` = '".$data['unit_id']."' ";
				$this->db->query($insert1);

				$year = date('Y');
				if($key == 12){
					$mod_key = 1;
					$mod_year = $year + 1;
				} else {
					$mod_key = $key + 1;
					$mod_year = $year;
				}
				//echo $mod_key;exit;
				$start_date_string = date('Y').'-'.$key.'-01';
				$start = new DateTime(date('Y').'-'.$key.'-01');
				//if(strtotime($start_date_string) > strtotime($doj)){
					$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
					$interval = DateInterval::createFromDateString('1 day');
					$period = new DatePeriod($start, $interval, $end);
					$week_array = array();
					foreach ($period as $dt)
					{
						$current_date = $dt->format('Y-m-d');
						if(strtotime($current_date) > strtotime($doj)){
						    if ($dt->format('N') == 3)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
						    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
						    }
						    if ($dt->format('N') == 7 || $dt->format('N') == 6)
						    {
						    	$sunday = $dt->format('Y-m-d');
						    	$month_name = date('M', strtotime($sunday));
					    		$year = date('Y', strtotime($sunday));
						    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
						    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
						    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
						    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
						    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
						    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
					    		$current_day = date('j', strtotime($sunday));
					    		if($first_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
					    		}

					    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
					    		if($second_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
					    		}

					    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
					    		if($third_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
					    		}

					    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
					    		if($fourth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
					    		}

					    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
					    		if($fifth_saturday == $current_day){
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
					    		} else {
					    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
					    		}
						        //$no++;
						    }
						}
					}
					foreach ($week_array as $wkey => $wvalue) {
						$week_string = 'W_1_'.$data['shift_id'];
						$halfday_string = 'HD_1_'.$data['shift_id'];
						$shift_string = 'S_'.$data['shift_id'];
						if($data['shift_id'] == '1'){
							if($data['sat_status'] == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		//$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						/*
						if($data['unit'] == 'VITA' && $data['shift_id'] == '2'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						} else {
						*/
						if($data['shift_id'] == '2' || $data['shift_id'] == '4'){
							if($wvalue['day_name'] == 'Sun'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
							if($wvalue['day_name'] == 'Wed'){
								$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
				        		//echo $sql2;
				        		//echo '<br />';
				        		$this->db->query($sql2);
							}
						}
						//}
					}
				//}
			} else {
				$shift_schedule_data = $is_exist->row;
				$shift = $shift_id_data;
				// echo '<pre>';
				// print_r($shift_schedule_data);
				// exit;
				if($data['shift_id'] != $data['hidden_shift_id'] || $data['sat_status'] != $data['hidden_sat_status'] || $data['unit_id'] != $data['hidden_unit_id'] || strtotime($data['doj']) != strtotime($data['hidden_doj'])){
					$insert1 = "UPDATE `oc_shift_schedule` SET 
							`emp_code` = '".$data['emp_code']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',
							`month` = '".$key."',
							`year` = '".date('Y')."',
							`status` = '1' ,
							`unit` = '".$data['unit']."',
							`unit_id` = '".$data['unit_id']."' 
							WHERE id = '".$shift_schedule_data['id']."' ";
					$this->db->query($insert1);

					$year = date('Y');
					if($key == 12){
						$mod_key = 1;
						$mod_year = $year + 1;
					} else {
						$mod_key = $key + 1;
						$mod_year = $year;
					}
					//echo $mod_key;exit;
					$start_date_string = date('Y').'-'.$key.'-01';
					$start = new DateTime(date('Y').'-'.$key.'-01');
					//if(strtotime($start_date_string) > strtotime($doj)){
						$end   = new DateTime($mod_year.'-'.$mod_key.'-01');
						$interval = DateInterval::createFromDateString('1 day');
						$period = new DatePeriod($start, $interval, $end);
						$week_array = array();
						foreach ($period as $dt)
						{
						   	$current_date = $dt->format('Y-m-d');
							if(strtotime($current_date) > strtotime($doj)){
							    if ($dt->format('N') == 3)
							    {
							    	$sunday = $dt->format('Y-m-d');
							    	$month_name = date('M', strtotime($sunday));
						    		$year = date('Y', strtotime($sunday));
							    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
							    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
							    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
							    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
							    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
							    	$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
							    	$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
							    	$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
							    	$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
							    	$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
							    }
							    if ($dt->format('N') == 7 || $dt->format('N') == 6)
							    {
							    	$sunday = $dt->format('Y-m-d');
							    	$month_name = date('M', strtotime($sunday));
						    		$year = date('Y', strtotime($sunday));
							    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
							    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
							    	$week_array[$dt->format('Y-m-d')]['day_name'] = $dt->format('D');
							    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
							    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
							    	$first_saturday = date('j', strtotime('first sat of '.$month_name.' '.$year));
						    		$current_day = date('j', strtotime($sunday));
						    		if($first_saturday == $current_day){
						    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 1;
						    		} else {
						    			$week_array[$dt->format('Y-m-d')]['first_saturday'] = 0;
						    		}

						    		$second_saturday = date('j', strtotime('second sat of '.$month_name.' '.$year));
						    		if($second_saturday == $current_day){
						    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 1;
						    		} else {
						    			$week_array[$dt->format('Y-m-d')]['second_saturday'] = 0;
						    		}

						    		$third_saturday = date('j', strtotime('third sat of '.$month_name.' '.$year));
						    		if($third_saturday == $current_day){
						    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 1;
						    		} else {
						    			$week_array[$dt->format('Y-m-d')]['third_saturday'] = 0;
						    		}

						    		$fourth_saturday = date('j', strtotime('fourth sat of '.$month_name.' '.$year));
						    		if($fourth_saturday == $current_day){
						    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 1;
						    		} else {
						    			$week_array[$dt->format('Y-m-d')]['fourth_saturday'] = 0;
						    		}

						    		$fifth_saturday = date('j', strtotime('fifth sat of '.$month_name.' '.$year));
						    		if($fifth_saturday == $current_day){
						    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 1;
						    		} else {
						    			$week_array[$dt->format('Y-m-d')]['fifth_saturday'] = 0;
						    		}
							        //$no++;
							    }
							}
						}
						foreach ($week_array as $wkey => $wvalue) {
							$week_string = 'W_1_'.$data['shift_id'];
							$halfday_string = 'HD_1_'.$data['shift_id'];
							$shift_string = 'S_'.$data['shift_id'];
							if($data['shift_id'] == '1'){
								if($data['sat_status'] == '0' && ($wvalue['second_saturday'] == '1' || $wvalue['fourth_saturday'] == '1') ){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
								if($data['sat_status'] == '0' && ($wvalue['first_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
									//$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		//$this->db->query($sql2);
								}
								if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
								if($wvalue['day_name'] == 'Sun'){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
							}
							/*
							if($data['unit'] == 'VITA' && $data['shift_id'] == '2'){
								if($wvalue['day_name'] == 'Sun'){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
								if($wvalue['day_name'] == 'Wed'){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
							} else {
							*/
							if($data['shift_id'] == '2' || $data['shift_id'] == '4'){
								if($wvalue['day_name'] == 'Sun'){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$week_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
								if($data['sat_status'] == '1' && ($wvalue['first_saturday'] == '1' || $wvalue['second_saturday'] == '1' || $wvalue['third_saturday'] == '1' || $wvalue['fourth_saturday'] == '1' || $wvalue['fifth_saturday'] == '1') ){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$halfday_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
								if($wvalue['day_name'] == 'Wed'){
									$sql2 = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$shift_string."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$data['emp_code']."' ";	
					        		//echo $sql2;
					        		//echo '<br />';
					        		$this->db->query($sql2);
								}
							}
							//}
						}
					//}

					/*
					foreach($shift_schedule_data as $skey => $svalue){
						if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
							$s_data_exp = explode("_", $svalue);
							if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
								foreach($s_data_exp as $sdxkey => $sdxvalue){
									if($sdxkey == 2){
										$s_data_exp[$sdxkey] = $data['shift_id'];
									}
								}
								$svalue = implode('_', $s_data_exp);
								$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "', `unit` = '".$data['unit']."', `unit_id` = '".$data['unit_id']."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
							} else {
								$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift) . "', `unit` = '".$data['unit']."', `unit_id` = '".$data['unit_id']."' WHERE `id` = '" . (int)$shift_schedule_data['id'] . "' ";
							}
							$this->db->query($sql);	
							//$this->log->write($sql);	
						}
					}
					*/
				}

				if($data['unit_id'] != $data['hidden_unit_id']){
					$holidayloc_sql = "SELECT `holiday_id`, `location`, `location_id` FROM `oc_holiday_loc` WHERE `location_id` = '".$data['unit_id']."' ";
					$holidayloc_datas = $this->db->query($holidayloc_sql);
					if($holidayloc_datas->num_rows > 0){
						$holidayloc_data = $holidayloc_datas->rows;
						foreach($holidayloc_data as $hkey => $hvalue){
							$holiday_sql = "SELECT `date` FROM `oc_holiday` WHERE `holiday_id` = '".$hvalue['holiday_id']."' AND `date` >= '".$doj."' ";
							$holiday_datas = $this->db->query($holiday_sql);
							if($holiday_datas->num_rows > 0){
								$holiday_data = $holiday_datas->row;
								$date = $holiday_data['date'];
								$month = date('n', strtotime($date));
								$year = date('Y', strtotime($date));
								$day = date('j', strtotime($date));
								$holiday_string = 'H_'.$hvalue['holiday_id'].'_'.$data['shift_id'];
								$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$data['emp_code']."' ";
								// echo $update_shift_schedule;
								// echo '<br />';
								// //exit;
								$this->db->query($update_shift_schedule);
							}
						}
						//echo 'out';exit;
					}
				}

				/*
				if($data['unit_id'] != $data['hidden_unit_id']){
					$sql = "UPDATE " . DB_PREFIX . "shift_schedule SET `unit` = '".$data['unit']."', `unit_id` = '".$data['unit_id']."' WHERE `emp_code` = '" . (int)$data['emp_code'] . "' AND `year` = '".date('Y')."' ";
					$this->db->query($sql);	
					//$this->log->write($sql);	
				}
				*/
			}
		}

		$emp_code = $data['emp_code'];

		// echo '<pre>';
		// print_r($data['dol']);
		// echo '<pre>';
		// print_r($data['hidden_dol']);
		// exit;

		if($data['dol'] = '0000-00-00' && strtotime($data['hidden_dol']) != strtotime($data['dol'])){
			$current_date = date('Y-m-d');
			$current_month = date('n');
			if($current_month == 1 || $current_month == 2){
				$start_date = date('Y-02-01');
			} else {
				$start_date = date('Y-m-d', strtotime($current_date . ' -60 day'));//'2019-06-26';
			}
			$end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));//'2019-06-30'
			$day = array();
			$days = $this->GetDays($start_date, $end_date);
			foreach ($days as $dkey => $dvalue) {
				$dates = explode('-', $dvalue);
				$day[$dkey]['day'] = $dates[2];
				$day[$dkey]['date'] = $dvalue;
			}
			// echo '<pre>';
			// print_r($day);
			// exit;
			$batch_id = '0';
			foreach($day as $dkeys => $dvalues){
				$filter_date_start = $dvalues['date'];
				$results = $this->getemployee_code($emp_code);
				// echo '<pre>';
				// print_r($results);
				// exit;
				foreach ($results as $rkey => $rvalue) {
					if(isset($rvalue['name']) && $rvalue['name'] != ''){
						$emp_name = $rvalue['name'];
						$department = $rvalue['department'];
						$unit = $rvalue['unit'];
						$group = '';
						$day_date = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));
						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2]= 1;
						}
						// echo '<pre>';
						// print_r($schedule_raw);
						// exit;
						if($schedule_raw[0] == 'S'){
							$shift_data = $this->getshiftdata_data($schedule_raw[1]);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
									//echo $sql;exit;
									$this->db->query($sql);
								}
							} else {
								$shift_data = $this->getshiftdata_data('1');
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
									$this->db->query($sql);
								}
							}
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
								$this->db->query($sql);
							}
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
								$this->db->query($sql);
							}
						} elseif ($schedule_raw[0] == 'HD') {
							$shift_data = $this->getshiftdata_data($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->getshiftdata_data('1');
							}
							$shift_intime = $shift_data['in_time'];
							if($shift_data['shift_id'] == '1'){
								if($rvalue['sat_status'] == '1'){
									$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
									$shift_outtime = Date('H:i:s', strtotime($shift_outtime .' +30 minutes'));
								} else {
									$shift_outtime = $shift_data['out_time'];
								}
							} else {
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
							}
							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', division = '".$rvalue['division']."', region = '".$rvalue['region']."', region_id = '".$rvalue['region_id']."', division_id = '".$rvalue['division_id']."', unit_id = '".$rvalue['unit_id']."', department_id = '".$rvalue['department_id']."', company = '".$rvalue['company']."', company_id = '".$rvalue['company_id']."' ";
								$this->db->query($sql);
							}
						}
					}	
				}
			}
		}
		//$this->db->query("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', grade = '" . $this->db->escape($data['grade']) . "', branch = '" . $this->db->escape($data['branch']) . "', department = '" . $this->db->escape($data['department']) . "', division = '" . $this->db->escape($data['division']) . "', group = '" . $this->db->escape($data['group']) . "', category = '" . $this->db->escape($data['category']) . "', unit = '" . $this->db->escape($data['unit']) . "', designation = '" . $this->db->escape($data['designation']) . "', dob = '" . $this->db->escape($data['dob']) . "', doj = '" . $this->db->escape($data['doj']) . "', doc = '" . $this->db->escape($data['doc']) . "', dol = '" . $this->db->escape($data['dol']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//echo "UPDATE " . DB_PREFIX . "employee SET card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "' WHERE employee_id = '" . (int)$employee_id . "'";
		//exit;
		//$this->log->write("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', department = '" . $this->db->escape($data['department_id']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', 
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function getshiftdata_data($shift_id) {
		$query = $this->db->query("SELECT * FROM oc_shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function deleteemployee($employee_id) {
		$emp_data = $this->getemployee($employee_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave_employee WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "transaction WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave_transaction WHERE emp_id = '" . (int)$emp_data['emp_code'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
	}	

	public function getemployee($employee_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
		return $query->row;
	}

	public function getemployee_code($emp_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "'");
		return $query->rows;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getdeptname($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshiftname($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		if($query->num_rows > 0){
			$shift_name = $query->row['in_time'].'-'.$query->row['out_time'];
			return $shift_name;
		} else {
			return '';
		}
	}

	public function getweekname($week_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "week WHERE week_id = '" . (int)$week_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getholidayname($holiday_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshift_id($emp_code, $day_date, $month, $year) {
		$query = $this->db->query("SELECT `".$day_date."` FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_code . "' ");
		if($query->num_rows > 0){
			return $query->row[$day_date];
		} else {
			return '';
		}
	}

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employee WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		if(isset($data['filter_jmc'])){
			$sql .= " AND `company_id` = '1' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != '' && !isset($data['filter_transfer'])){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}

			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$sort_data = array(
			'name',
			'emp_code',
			'department',
			'division',
			'region',
			'company',
			'unit'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalemployees($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		if(isset($data['filter_jmc'])){
			$sql .= " AND `company_id` = '1' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != '' && !isset($data['filter_transfer'])){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}
			
			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveEmployee() {
		$sql = "SELECT `emp_code`, `name`, `doj`, `grade`, `group` FROM ".DB_PREFIX."employee WHERE `status` = '1'";	
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getemployees_export($data = array()) {
		$sql = "SELECT `emp_code`, `name`, `gender`, `dob`, `doj`, `doc`, `dol`, `employement`, `grade`, `designation`, `department`, `unit`, `division`, `region`, `state`, `company`, `sat_status`, `status` FROM " . DB_PREFIX . "employee WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			if(ctype_digit($data['filter_name'])){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department_id) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_region']) && !empty($data['filter_region'])) {
			$sql .= " AND LOWER(region_id) = '" . $this->db->escape(strtolower($data['filter_region'])) . "' ";
		}

		if (isset($data['filter_division']) && !empty($data['filter_division'])) {
			$sql .= " AND LOWER(division_id) = '" . $this->db->escape(strtolower($data['filter_division'])) . "' ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit_id) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_company']) && !empty($data['filter_company'])) {
			$sql .= " AND LOWER(company_id) = '" . $this->db->escape(strtolower($data['filter_company'])) . "' ";
		}

		if(isset($data['filter_local'])){
			$sql .= " AND `company_id` <> '1' ";
		}

		if(isset($data['filter_jmc'])){
			$sql .= " AND `company_id` = '1' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		//if($this->user->getId() != '1'){
			$division_string = $this->user->getdivision();
			$region_string = $this->user->getregion();
			$site_string = $this->user->getsite();
			$company_string = $this->user->getCompanyId();
			
			if($company_string != ''){
				$company_string = "'" . str_replace(",", "','", html_entity_decode($company_string)) . "'";
				if (isset($data['filter_company']) && !empty($data['filter_company'])) {
				} else {
					$sql .= " AND `company_id` IN (" . strtolower($company_string) . ") ";
				}
			}

			if($division_string != ''){
				$division_string = "'" . str_replace(",", "','", html_entity_decode($division_string)) . "'";
				if (isset($data['filter_division']) && !empty($data['filter_division'])) {
				} else {
					$sql .= " AND `division_id` IN (" . strtolower($division_string) . ") ";
				}
			}

			if($region_string != ''){
				$region_string = "'" . str_replace(",", "','", html_entity_decode($region_string)) . "'";
				if (isset($data['filter_region']) && !empty($data['filter_region'])) {
				} else {
					$sql .= " AND `region_id` IN (" . strtolower($region_string) . ") ";
				}
			}

			if($site_string != ''){
				$site_string = "'" . str_replace(",", "','", html_entity_decode($site_string)) . "'";
				if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
				} else {
					$sql .= " AND `unit_id` IN (" . strtolower($site_string) . ") ";
				}
			}
		//}

		$sort_data = array(
			'name',
			'emp_code',
			'department',
			'division',
			'region',
			'company',
			'unit'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
?>