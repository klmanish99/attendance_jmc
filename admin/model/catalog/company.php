<?php
class ModelCatalogCompany extends Model {
	public function addCompany($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "company` SET 
							`company` = '" . $this->db->escape(html_entity_decode($data['company'])) . "',
							`company_code` = '" . $this->db->escape(html_entity_decode($data['company_code'])) . "'
						");

		$company_id = $this->db->getLastId(); 
	}

	public function editCompany($company_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "company SET 
							`company` = '" . $this->db->escape(html_entity_decode($data['company'])) . "',
							`company_code` = '" . $this->db->escape(html_entity_decode($data['company_code'])) . "'
							WHERE company_id = '" . (int)$company_id . "'");
	}

	public function deleteCompany($company_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "company WHERE company_id = '" . (int)$company_id . "'");
	}	

	public function getCompany($company_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company WHERE company_id = '" . (int)$company_id . "'");

		return $query->row;
	}

	public function getCompanys($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "company WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND company_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(company) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'company',
			'company_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY company";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalCompanys() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "company";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND company_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(company) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>