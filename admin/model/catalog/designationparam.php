<?php
class ModelCatalogDesignationparam extends Model {
	public function addDesignationparam($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "designationparam` SET 
							`name` = '" . $this->db->escape($data['name']) . "', 
							`type` = '" . $this->db->escape($data['type']) . "', 
							`percent` = '" . $this->db->escape($data['percent']) . "', 
							`status` = '" . $data['status'] . "'
						");

		$designationparam_id = $this->db->getLastId(); 
	}

	public function editDesignationparam($designationparam_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "designationparam SET 
							`name` = '" . $this->db->escape($data['name']) . "', 
							`type` = '" . $this->db->escape($data['type']) . "', 
							`percent` = '" . $this->db->escape($data['percent']) . "',
							`status` = '" . $data['status'] . "' 
							WHERE designationparam_id = '" . (int)$designationparam_id . "'");
	}

	public function deleteDesignationparam($designationparam_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "designationparam WHERE designationparam_id = '" . (int)$designationparam_id . "'");
	}	

	public function getDesignationparam($designationparam_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "designationparam WHERE designationparam_id = '" . (int)$designationparam_id . "'");

		return $query->row;
	}

	public function getDesignationparams($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "designationparam WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND designationparam_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'name',
			'type'
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDesignationparams() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "designationparam";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND designationparam_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>