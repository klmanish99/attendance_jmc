<?php
class ModelCatalogRegion extends Model {
	public function addRegion($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "region` SET 
							`region` = '" . $this->db->escape(html_entity_decode($data['region'])) . "',
							`region_code` = '" . $this->db->escape(html_entity_decode($data['region_code'])) . "'
						");

		$region_id = $this->db->getLastId(); 
	}

	public function editRegion($region_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "region SET 
							`region` = '" . $this->db->escape(html_entity_decode($data['region'])) . "',
							`region_code` = '" . $this->db->escape(html_entity_decode($data['region_code'])) . "'
							WHERE region_id = '" . (int)$region_id . "'");
	}

	public function deleteRegion($region_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "region WHERE region_id = '" . (int)$region_id . "'");
	}	

	public function getRegion($region_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "region WHERE region_id = '" . (int)$region_id . "'");

		return $query->row;
	}

	public function getRegions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "region WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND region_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(region) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'region',
			'region_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY region";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalRegions() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "region";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND region_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(region) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>